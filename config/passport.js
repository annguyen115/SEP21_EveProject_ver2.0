var LocalStrategy   = require('passport-local').Strategy;
var Lecturer = require('../app/models/lecturer');
var https = require('https');


module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);    
    });

    passport.deserializeUser(function(id, done) {
        Lecturer.findOne({'lecturer_id':id}, function(err, user) {
            done(err,user);
        });
    });
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, password, done){
        process.nextTick(function(){
            var url = 'https://entool.azurewebsites.net/SEP21/Login?Username=' + username + '&Password=' + password;
            var request = https.get(url,function(response){
                var json='';
                response.on('data',d=>{
                   json+=d
                });
                response.on('end',()=>{
                    var data_response = JSON.parse(json);
                    var lecturer_infor = data_response.data;
                    if(data_response.code == 0){
                        Lecturer.findOne({'lecturer_id':lecturer_infor.id},function(err,user){
                            if(!user){
                                // var lecturer = new Lecturer(data_response);
                                var lecturer = new Lecturer();
                                let hashPassword = lecturer.generateHash(password);
                                var local = {
                                    username : username,
                                    password : hashPassword,
                                }
                                lecturer.lecturer_id = data_response.data.id;
                                lecturer.secrect= data_response.data.secret;
                                lecturer.local=local;
                                // lecturer.local.username = username;
                                // lecturer.local.password = lecturer.generateHash(password);                               
                                lecturer.save(err=>{
                                    if(err) console.log('err:'+err);
                                    console.log('Insert '+username+' successfully');
                                   
                                });
                            }
                        });
                    }
                    
                    if(data_response.code == 1){
                        console.log('login failed');
                        return done(null, false, req.flash('loginMessage', 'Username or password is incorrect.'));
                    }
                    console.log('login success');
                    return done(null,data_response.data);
                });
            });     
        });
    }));
};
