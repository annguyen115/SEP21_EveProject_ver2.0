'use strict';
var mongoose = require('mongoose');

var Attendance = mongoose.model('Attendance');
var find = require('array-find');
var Session = mongoose.model('Session');
var Student = mongoose.model('Student');
var Course = mongoose.model('Course');

exports.getStudent = function (studentId) {
    return new Promise((resolve,reject)=>{
        Student.findOne({student_id : studentId}).lean().exec((err, data) => {
            err ? reject(err) : resolve (data);
        });
    })
}

exports.getCourse = function (courseId) {
    return new Promise((resolve,reject)=>{
        Course.findOne({course_id : courseId}).lean().exec((err, data) => {
            err ? reject(err) : resolve (data);
        });
    })
}

exports.countSession = function (courseId) {
    return new Promise((resolve,reject)=>{
        Session.count({course_id : courseId}).lean().exec((err, data) => {
            err ? reject(err) : resolve (data);
        });
    })
}

exports.countAttendance = function (StudentId, CourseId) {
    return new Promise((resolve,reject)=>{
        Attendance.count({student_id : StudentId, course_id : CourseId, status: 1}).lean().exec((err, data) => {
            err ? reject(err) : resolve (data);
        });
    })
}