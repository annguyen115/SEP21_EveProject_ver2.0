'use strict';
var mongoose = require('mongoose');
// var lecturer = mongoose.model('Lecturer');
var Attendance = mongoose.model('Attendance');
var Session = mongoose.model('Session');
var Student = mongoose.model('Student');
var async = require("async");
var FormData = require('form-data');
var unirest = require('unirest');
/* Insert session list from table */
/* Params: id as Course_id,list as StudentList */
exports.createAttendance = function(id,list,newSessionID,lastAtId){
    return new Promise((resolve,reject)=>{
        let course_id = id;
        list.forEach(s => {
            let newAttendance = new Attendance({
                attendance_id:lastAtId+1,
                session_id:newSessionID,
                student_id:s.student_id,
                course_id:course_id,
                note:"",
                status:0
            });
            newAttendance.save(err=>{
                if(err) return reject(err);
                console.log('insert '+s.fullname+"'s attandance successfully");
            })
            lastAtId++;
        });
        return resolve('done');  
    })
}
/* Params: id as Course_id,student_id as id */
exports.createStudentAttendance = function(id,student_id,aCount,lastAtId){
    return new Promise((resolve,reject)=>{
        let course_id = id;
        let i =0;
        let c = '';
        Session.find({course_id:id}).lean().exec((err,session)=>{
            if(err) return reject(err);
            else{
                let dif = session.length-aCount;
                // console.log('dif:'+dif);
                if(dif>0){//khúc này dùng để check xem coi có attendance của sinh viên này có cái nào trong db ko,chỉ tạo thêm attendance = hiện của tổng session - tổng số attendance của sinh viên đó
                    for(i=0;i<dif;i++){
                        let newAttendance = new Attendance({
                            attendance_id : lastAtId+1,
                            session_id:session[aCount+i].session_id,
                            student_id:student_id,
                            course_id:course_id,
                            note:"",
                            status:0
                        })
                        newAttendance.save(err=>{
                            if(err) return reject(err);
                            else{
                                console.log('insert '+student_id+' '+(newAttendance.attendance_id)+' attandance successfully');
                            }
                        })
                        lastAtId++;
                    }
                }
                return resolve('done');   
            }
        });
        
           
    })
}

exports.getAttendanceList = function (id,session_id) {
    return new Promise((resolve,reject)=>{
        let course_id = id;
        Attendance.find({course_id:course_id}).sort({ attendance_id: 1 }).populate({path:'session'}).exec((err,attend)=>{
            if(err) return reject(err);
            return resolve(attend);
        });
    });
}
/* Update One Attendance Status of student*/
/* Params: id as attendance_id,status as status of attendance */
exports.updateAttendance = function(atid,stid,status){
    return new Promise((resolve,reject)=>{
        Attendance.update({ attendance_id: atid,student_id:stid}, { $set: { status: status }},function(err,raw){
            if(err) return reject(err);
            return resolve('success')
        });
    });
}
/* Update Many Attendance Status of student*/
/* Params: id as course_id,attendanceArray as Array with it has full infor of student such as fullname,student_id,attendance... */
exports.updateManyAttendance = function(id,attendanceArray){
    return new Promise((resolve,reject)=>{
        let student_id = attendanceArray.Id;
        let course_id = id;
        let statusArray = attendanceArray.attendance;
        let i=0;
        let status=0;
        Attendance.find({$and:[{student_id:student_id,course_id:course_id}]}).sort({attendance_id:1}).lean().exec((err,attend)=>{
            if(err) return reject(err);
            // console.log('length:'+attend.length) ;
            console.log('test length:'+attend.length)
            if(attend.length>0){
                attend.forEach(e => {
                    // console.log('go');
                    // console.log('i:'+i);
                    let s = statusArray[i].status.toUpperCase().trim();
                    if(s=='V'){
                        status = 1;
                    }else if(s=='X'){
                        status = 2;
                    }else if(s!='X'&&s!='V'){
                        status=0;
                    }
                    // console.log(i,status);
                    Attendance.update({ attendance_id: e.attendance_id }, { $set: { status: status }},function(err,raw){
                        if(err) return reject(err);
                        else{
                            // console.log('update success')
                            return resolve('Update attendance status of '+student_id+' successfully');                        
                        }
                    });
                    i++;
                });
            }else{
                return resolve('done');                 
            }
        });
        
    });
}
/* Statistics for Attendance*/
/* Params: id as sesssion_id ,sid as studentid*/
exports.attendanceStatistics = function(id,slist){
    return new Promise((resolve,reject)=>{
        let count0=0,count1=0,count2=0;
        async.each(slist,function(s,cb){
            Attendance.findOne({$and:[{session_id:id},{student_id:s.student_id}]}).lean().exec((err,att)=>{
                if(err){
                    cb(err);
                    return;
                }else if(att){
                    console.log('go');
                    if(att.status==0){
                        count0++;
                    }else if(att.status==1){
                        count1++;
                    }else if(att.status==2){
                        count2++;
                    }
                    console.log(count0,count1,count2);
                    cb();
                } 
            });          
        },function(err){
            if(err){
                return reject(err);
            }else{
                let total = {
                    notattend:count0,
                    attend:count1,
                    absent:count2,
                }
                console.log(total);
                return resolve(total);        
            }
        });
    });
}
/*Count Student Attendance Exist in database*/
/*Param: id as course_id,sid as student_id */
exports.countStudentAttendance = function(id,sid){
    return new Promise((resolve,reject)=>{
        Attendance.count({$and:[{course_id:id},{student_id:sid}]}).exec((err,count)=>{
            if(err) return reject(err);
            return resolve (count);
        });
    });
}
exports.getAttendOfOneStudentInOneCourse =function(id,sid){
    return new Promise((resolve,reject)=>{
        Attendance.find({$and:[{course_id:id},{student_id:sid}]}).lean().exec((err,att)=>{
            if(err) return reject(err);
            if(att){
                return resolve(att);
            }else{
                return resolve('');            
            }
        });
    });
}
exports.attendanceStatisticsOneStudentInOneCourse = function(id,sid){
    return new Promise((resolve,reject)=>{
        let count0=0,count1=0,count2=0;
        Attendance.find({$and:[{student_id:sid},{course_id:id}]}).lean().exec((err,at)=>{
            if(err) return reject(err);
            if(at){
                at.forEach(e => {
                    if(e.status==0){
                        count0++;
                    }else if(e.status==1){
                        count1++;
                    }else if(e.status==2){
                        count2++;
                    }
                });
                let total = {
                    notattend:count0,
                    attend:count1,
                    absent:count2,
                }
                return resolve(total);    
            }else{
                return resolve('');
            }
        })
    });
}
/*Tim attendance của 1 sinh viên trong 1 session,tìm thấy thì thôi,không tìm thấy thì tạo mới */
/*Param id as course_id,sId as student_id,seId as session_id,lastAtId as Last attendance_id */
exports.findAndCreateAttendanceOfOneStudent = function(id,sId,seId,lastAtId){
    return new Promise((resolve,reject)=>{
        Attendance.findOne({$and:[{student_id:sId},{session_id:seId},{course_id:id}]}).lean().exec((err,at)=>{
            if(err) return reject(err);
            if(!at){
                let newAttendance = new Attendance({
                    attendance_id:lastAtId+1,
                    session_id:seId,
                    student_id:sId,
                    course_id:id,
                    note:'',
                    status:0
                })
                newAttendance.save(err=>{
                    if(err) {
                        return reject(err);
                    }else{
                        console.log('save');
                        console.log('insert '+sId+' '+(newAttendance.attendance_id)+' attandance successfully');
                        return resolve('success')
                    }
                })
            }else{
                return resolve('');            
            }
        });
    });
}
/*Tìm ID cuối cùng của Attendance */
exports.getLastAttendanceId = function(){
    return new Promise((resolve,reject)=>{
        Attendance.findOne({}).sort({attendance_id:-1}).lean().exec((err,at)=>{
            if(err) return reject(err);
            if(at){
                return resolve(at.attendance_id);                
            }else{
                return resolve(0);
            }
        })
    })
}
exports.removeAttendance = function(seId){
    return new Promise((resolve,reject)=>{
        Attendance.find({session_id:seId}).remove().exec((err,at)=>{
            if(err) return reject(err);
            return resolve('done');
        })
    });
}
exports.getAttendanceListFormatForSync = function(id,stID,seIDList){
    return new Promise((resolve,reject)=>{
        let checklist = [];        
        let result = [];
        Attendance.find({$and:[{student_id:stID},{course_id:id},{session_id:{$in:seIDList}}]}).exec((err,at)=>{
            if(err) return reject(err);
            // return resolve(at);
            at.forEach(e => {
                if(e.status==1){
                    checklist.push(e.session_id);
                }
            });
            let info = JSON.stringify(at);
            // let info = '';
            result.push(checklist);
            result.push(info);
            return resolve(result);
        });
    });
}
exports.syncAttendance = function(user,id,session,attandance){
    return new Promise((resolve)=>{
        let uid = user.lecturer_id;
        let secret = user.secrect;
        let data = {
            course : id,
            sessions:session,
            attendance:attandance
        }

        ///////////////
        // let form = new FormData({
        //     uid:uid,
        //     secret:secret,
        //     data:data
        // });
        // console.log(JSON.parse(form.data));
        // unirest.post('https://entool.azurewebsites.net/SEP21/SyncAttendance')
        //         .headers({'Accept': 'application/json'})
        //         .send(form)
        //         .end(function (response) {
        //             console.log(response.body);
        //             return resolve(response.body)                
        //         });
        /////////////////////////////

        var request = require("request");
        data = JSON.stringify(data);        
        var options = { method: 'POST',
        url: 'https://entool.azurewebsites.net/SEP21/SyncAttendance',
        headers: 
        { 'Postman-Token': 'b291e1d3-6692-4e13-b116-70ae10e3f47a',
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/x-www-form-urlencoded' },
        form: {   
            uid: uid,
            secret: secret,
            data: data 
        }};
        // console.log(JSON.parse(options.form.data));
        request(options, function (error, response, body) {
        if (error) return reject(error);
        // console.log(body);
        // console.log(response)

        //body đang là string.parse nó về json để hiển thị
        body = JSON.parse(body);
        return resolve(body);
        });
    });
}
exports.getAttendOfOneStudentOneSession =function(id,seid,sid){
    return new Promise((resolve,reject)=>{
        let a;
        Attendance.findOne({$and:[{course_id:id},{session_id:seid},{student_id:sid}]}).lean().exec((err,att)=>{
            if(err) return reject(err);
            if(att){
                let status = att.status;
                let b='';
                if(status==1){
                    b='<span class="qa-center glyphicon glyphicon-ok attend" aria-hidden="true"></span></center>'
                }else if(status==2){
                    b='<span class="qa-center glyphicon glyphicon-remove absent" aria-hidden="true"></span>'
                }
                a = {
                    icon : b,
                    status:status,
                    note :att.note,
                }
            }else{
                a = {
                    status : 'error!!!',
                    note :'error!!!',
                }
            }
            return resolve(a);
        });
    });
}