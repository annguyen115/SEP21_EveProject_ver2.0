'use strict';
var mongoose = require('mongoose');
var https = require('https');
var sortJsonArray = require('sort-json-array');
var Student = mongoose.model('Student');
var Attendance = mongoose.model('Attendance');
var async = require("async");
var find = require('array-find');
var FormData = require('form-data');
var unirest = require('unirest');
var request = require('request');
    exports.getSelectedAttendance = function (id,session_id) {
        return new Promise((resolve,reject)=>{
            let course_id = id;
            Student.find({course_id:course_id}).sort({ firstname: 1 }).populate({
                path: 'oneattendance',
                match: { 'session_id': session_id},
                populate: ({path:'session',model:'Session'})
            }).exec(function (err, student) {
                if (err) return reject(err);
                else {
                    return resolve(student);        
                }
            });
        });
    }
    /* Get Student's Attendance information*/
    exports.getAttendanceInfor = function (id) {
        return new Promise((resolve,reject)=>{
            let course_id = id;
            Student.find({course_id:course_id}).sort({ firstname: 1 }).populate({
                path: 'attendance',
                match: { 'course_id': course_id},
                populate: ({ path: 'session' })
            }).exec(function (err, student) {
                if (err) return reject(err);
                else {
                    // var reformattedArray = student.map((s)=>{
        
                    //     var stuObj = {};
                    //     if(s.attendance.length>0){
                    //         stuObj['attendance']= s.attendance;
                    //     }
                    //     return stuObj;
        
                    // });
                    // // student = student.filter((s) => {
                    //     return s.attendance !== null;
                    // });
                    
                    // student = filter_array(student);
                    
                    // console.log(JSON.stringify(student, null, ' '));
                    
                    return resolve(student);        
                }
            });
        });
    }
    function filter_array(test_array) {
        let index = -1;
        const arr_length = test_array ? test_array.length : 0;
        let resIndex = -1;
        const result = [];

        while (++index < arr_length) {
            const value = test_array[index];

            if (value["attendance"].length>0) {
                result[++resIndex] = value;
            }
        }

        return result;
    }
    /* Insert student into dblocal if they dont exist */
    /* Params: id as Course_id */
    exports.insertStudent = function (id) {
        return new Promise((resolve,reject)=>{
            let course_id = id;
            let url = 'https://entool.azurewebsites.net/SEP21/GetMembers?courseID=' + course_id;
            let newStudentID = [];
            var request = https.get(url, (response) => {
                let json='';
                response.on('data', (d) => {
                    json +=d;
                }).on('error',err=>{
                    console.error(err);
                }).on('end',()=>{
                    try {
                    let data_response = JSON.parse(json);  
                    let stu = data_response.data;
                    async.each(stu,function(s,cb){
                        Student.findOne({ student_id: s.id }).lean().exec((err, student)=>{
                            if (!student) {
                                //create newCourse
                                let course=[course_id];
                                let newStudent = new Student();
                                newStudent.student_id = s.id;
                                newStudent.fullname = s.fullname;
                                newStudent.firstname = s.firstname;
                                newStudent.lastname = s.lastname;
                                newStudent.birthday = s.birthday;
                                newStudent.course_id= course;
                                //save newCourse
                                newStudent.save(err => {
                                    if (err){
                                        cb(err);
                                        return;
                                    }else {
                                        console.log('Insert ' + s.fullname + ' successfully');
                                        newStudentID.push(newStudent.student_id);                                            
                                    }
                                    cb();
                                });
                            }else{
                                let course=student.course_id;
                                let found =find(course, function (c, index, array) {
                                    return c === course_id;
                                });
                                if(typeof found =='undefined'){
                                    // console.log('not found');
                                    course.push(course_id);
                                    Student.update({ _id: student._id }, { $set: { course_id: course }},function(err,raw){
                                        if (err){
                                            cb(err);
                                            return;
                                        }else{
                                            // console.log('raw:'+raw);
                                            newStudentID.push(student.student_id);                                                                                            
                                        }
                                    });
                                }else{
                                    // console.log('found')
                                }
                                cb();
                            }
                        });
                    },function(err){
                        if(err){
                            return reject(err);
                        }else{
                            return resolve(newStudentID);
                        }
                    }); 
                    } catch (err) {
                        console.error(err);
                    }
                });
                
            });
        })
    }


    /* Insert student list from api */
    /* Params: id as Course_id */
    exports.getStudentList = function (id) {
        return new Promise((resolve, reject) => {
            let course_id = id;
            Student.find({course_id:course_id}).sort({firstname:1}).lean().exec((err,student)=>{
                if(err) return reject(err);
                if(student){
                    return resolve(student);
                }else {
                    return resolve('');
                }
            });
        })
    }

    /* Get Student infor and put it intodb */
    /* Params: id as Course_id */
    /* chưa làm vụ check sinh viên học nhiều lớp*/
    exports.createStudent = function (id,student_id) {
        return new Promise((resolve, reject) => {
        let name;
        let result=[];
        Student.findOne({student_id:student_id}).exec((err,student)=>{
                if(err) return reject(err);
                else{
                    if(student!=null){
                        Student.findOne({$and:[{student_id:student_id},{course_id:id}]}).exec((err,st)=>{
                            if(st!=null){
                                //existed
                                name=st.fullname;
                                result.push('existed');
                                result.push(name);
                                return resolve(result);
                            }else{
                                let course=student.course_id;
                                // console.log('course:'+course);
                                course.push(id);
                                // console.log('course after push:'+course);
                                Student.update({ _id: student._id }, { $set: { course_id: course }},function(err,raw) {
                                    if (err) return reject(err);
                                    // console.log('raw:'+raw);
                                }); 
                                name = student.fullname;
                                result.push('create successs');
                                result.push(name);
                                return resolve(result)
                            }
                        })
                    }else{
                        //call api
                        // console.log('call api')
                        let url = 'https://entool.azurewebsites.net/SEP21/GetStudent?code=' + student_id;
                        var request = https.get(url,response=>{
                            var json=''
                            response.on('data',d=>{
                                json+=d;
                            }).on('end',()=>{
                                var data_response = JSON.parse(json);
                                if(data_response.code==1){
                                    name='';
                                    result.push('notfound')
                                    result.push(name);
                                    return resolve(result);
                                }else if(data_response.code==0){
                                    let data = data_response.data;
                                    // console.log('data:'+data);
                                    let newStudent = new Student({
                                        student_id : data.id,
                                        fullname : data.fullname,
                                        firstname : data.firstname,
                                        lastname : data.lastname,
                                        birthday : data.birthday,
                                        course_id:id
                                    });
                                    newStudent.save(err=>{
                                        if(err) return reject(err);
                                        else {
                                            name=data.fullname;
                                            result.push('create successs');
                                            result.push(name);
                                            return resolve(result);
                                        }
                                    })
                                }
                            })
                        });
                    }
                }
        });
        });
    }
    /* Search student infor by Course */
    /* Param: id as Course,data as keyword */
    exports.searchByCourse = function(id,data){
        return new Promise((resolve,reject)=>{
            data = data.trim().toUpperCase();
            let result=[];
            let regexCovert = new RegExp(data, 'i');
            Student.find({$and:[{course_id:id},{$or:[{student_id:regexCovert},{fullname:regexCovert},{firstname:regexCovert},{lastname:regexCovert}]}]}).lean().exec((err,st)=>{
                if(err) return reject (err);
                st.forEach(e => {   
                    let format={
                        id:e.student_id,
                        label:e.fullname,
                        value:e.fullname,
                    };
                    result.push(format); 
                });
                return resolve(result);
            });
        })
    }
    /* Delete Student from course */
    /* Param: id as Course,sid as student_id  */
    exports.removeCourseID = function(id,sid){
        return new Promise((resolve,reject)=>{
            let result=[];
            Student.findOne({$and:[{student_id:sid},{course_id:id}]}).lean().exec((err,st)=>{
                if(err) return reject(st);
                if(st){
                    let course = st.course_id;
                    //remove
                    let position = course.indexOf(id);
                    if(position>-1){
                        course.splice(position,1);
                    }
                    
                    //remove ra khỏi db
                    // console.log(course.length);
                    // if(course.length==0){
                    //     Student.findOne({student_id:sid}).remove().exec((err)=>{
                    //         if(err) return reject(err);
                    //         else{
                    //             console.log('remove successfully');
                    //         }
                    //     })
                    // }else{
                    //     Student.update({_id:st._id},{ $set: { course_id: course }},function(err,raw){
                    //         if (err) return reject(err);
                    //         console.log('raw:'+raw);
                    //     });
                    // }

                    //không remove ra khỏi db
                    Student.update({_id:st._id},{ $set: { course_id: course }},function(err,raw){
                        if (err) return reject(err);
                        // console.log('raw:'+raw);
                    });
                    result.push('succeed');
                    result.push(st.fullname);
                }else{
                    result.push('failed');
                    result.push(sid);
                }
                return resolve(result);
            });
        })
    }

    /* Get student list from all course*/
    exports.getStudentListFromAllCourse = function(){
        return new Promise((resolve,reject)=>{
            Student.find({}).sort({firstname:1}).lean().exec((err,st)=>{
                if(err) return reject(err);
                return resolve(st);
            })
        });
    }
    /*search by StudentID */
    /*Param sid as student_id */
    exports.searchByStudentID = function(sid){
        return new Promise((resolve,reject)=>{
            Student.findOne({student_id:sid}).lean().exec((err,st)=>{
                if(err) return reject(err);
                st.class=[];
                st.sessionlist=[];
                st.attendancelist=[];
                st.statistics=[];
                return resolve(st);
            });
        });
    }

    exports.getStudentIDArray= function(id){
        return new Promise((resolve,reject)=>{
            let course_id = id;
            let sidArray=[]
            Student.find({course_id:id}).lean().exec((err,st)=>{
                if(err) return reject(err);
                if(st){
                    st.forEach(e => {
                        sidArray.push(e.student_id);
                    });
                    return resolve(sidArray);
                }else{
                    return resolve('');
                }
            });
        });
    }
    exports.syncStudent = function(user,id,sidList){
        return new Promise((resolve,reject)=>{
           try {
                //data
                let uid = user.lecturer_id;
                let secret = user.secrect;
                let data = {
                    course:id,
                    members:sidList
                }
                data = JSON.stringify(data);
                let form = new FormData({
                    uid:uid,
                    secret:secret,
                    data:data
                });
                // console.log(form.data)
                var options = {
                    host: 'https://entool.azurewebsites.net',
                    port: 443,
                    path: '/SEP21/SyncMembers',
                    method: 'POST'
                };
                unirest.post('https://entool.azurewebsites.net/SEP21/SyncMembers')
                .headers({'Accept': 'application/json'})
                .send(form)
                .end(function (response) {
                    console.log(response.body);
                    return resolve(response.body)                
                });
                // request.post({url:'https://entool.azurewebsites.net/SEP21/SyncMembers', formData: form}, function(err, httpResponse, body) {
                // if (err) {
                //     return console.error('Sync failed:', err);
                // }
                // console.log('Sync successful!  Server responded with:', body);
                // });
           } catch (err) {
               console.error(err);
           }
        });
    }
    exports.getStudentFullName = function(stid){
        return new Promise((resolve,reject)=>{
            Student.findOne({student_id:stid}).lean().exec((err,st)=>{
                if(err){
                    return reject(err);
                }
                if(st){
                    return resolve(st.fullname);
                }else{
                    return resolve('');
                }
            })
        })
    }
    exports.checkClassIsEmpty = function(course_id){
        return new Promise((resolve,reject)=>{
        })
    }