'use strict';
var mongoose = require('mongoose');
var Course = mongoose.model('Course');
var Attendance = mongoose.model('Attendance');
var Session = mongoose.model('Session');
var Student = mongoose.model('Student');
var https = require('https');

exports.insertStudent = function(id,sid){
    return new Promise((resolve,reject)=>{
        let result = [];
        let message;
        let newstudentID;
        let student_id = sid;
        Student.findOne({student_id:student_id}).exec((err,student)=>{
            if(err) console.error(err);
            else{
                if(student!=null){
                    Student.findOne({$and:[{student_id:student_id},{course_id:id}]}).exec((err,st)=>{
                        if(st!=null){
                        //existed
                        message=student.student_id+' is existed';
                        newstudentID=0;
                        result.push(newstudentID);
                        result.push(message);
                        return resolve(result);
                        }else{
                        let course=student.course_id;
                        console.log('course:'+course);
                        course.push(id);
                        console.log('course after push:'+course);
                        Student.update({ _id: student._id }, { $set: { course_id: course }},function(err,raw) {
                            if (err) console.error(err);
                            else{
                                message='create ' +student.student_id+ ' successs'
                                newstudentID=student.student_id;
                                result.push(newstudentID);
                                result.push(message);
                                return resolve(result);
                            }
                        }); 
                        }
                    })
                }else{
                    //call api
                    console.log('call api')
                    let url = 'https://entool.azurewebsites.net/SEP21/GetStudent?code=' + student_id;
                    let data_response='';
                    https.get(url,response=>{
                        response.on('data',d=>{
                            try {
                                data_response = JSON.parse(d);  
                                
                           } catch (error) {
                            console.error(error);
                           }
                        }).on('end',()=>{
                            if(data_response.code==1){
                                message = student_id+ 'notfound';
                                newstudentID=0;
                                result.push(newstudentID);
                                result.push(message);
                                return resolve(result);
                            }else if(data_response.code==0){
                                let data = data_response.data;
                                // console.log('data:'+data);
                                let newStudent = new Student({
                                    student_id : data.id,
                                    fullname : data.fullname,
                                    firstname : data.firstname,
                                    lastname : data.lastname,
                                    birthday : data.birthday,
                                    course_id:id
                                });
                                newStudent.save(err=>{
                                    if(err) console.error(err);
                                    else {
                                        message="create "+data.id+" successfully";
                                        newstudentID=data.id;
                                        result.push(newstudentID);
                                        result.push(message);
                                        return resolve(result);
                                    }
                                })
                            }
                        })
                    });
                }
            }
        });
    });
}
exports.createAttedance = function(sid,id){

}