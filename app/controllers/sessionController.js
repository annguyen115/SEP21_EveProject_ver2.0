'use strict';
var mongoose = require('mongoose');
var https = require('https');
var sortJsonArray = require('sort-json-array');
var format = require('date-format');
var Session = mongoose.model('Session');


/* Insert session list from table */
/* Params: id as Course_id */
exports.getSessionList = function(id){
    return new Promise((resolve,reject)=>{
        var course_id = id;
        Session.count({course_id:course_id},function(err,count){
            console.log('count:'+count);
            if(count>0){
                Session.find({course_id:course_id}).sort('session_id').exec(function(err,session){
                    return resolve(session);
                })
            }else{
                return resolve(null);
            }
        });
    });
    
}
/* Create session */
/* Param:id:course_id*/
exports.createSession = function(id,sessiondate,lastSeID){
    return new Promise((resolve,reject)=>{
       try {
        let course_id = id;
        let course_session=0;
        // console.log('createSession');
        Session.findOne({course_id:course_id}).sort({session_id:-1}).lean().exec((err,session)=>{            
            if(err) return reject(err);
            if(session){
                course_session=session.temp;
            }
            //create new session
            let sessionNo = course_session+1;
            let sessionName='';
            if(sessionNo==1){
                sessionName="1st Session"
            }else if(sessionNo==2){
                sessionName="2nd Session"
            }else if(sessionNo==3){
                sessionName="3rd Session"
            }else if(sessionNo>=4){
                sessionName=sessionNo+"th Session";
            }
            var newSession = new Session({
                session_id:lastSeID+1,
                session_no:sessionName,
                session_date:sessiondate,
                course_id:course_id,
                temp:sessionNo
            });
            newSession.save(err=>{
                if(err) console.log(err);
                console.log("insert "+newSession.session_no+" "+newSession.session_date+" successfully");
            });
            return resolve(newSession);
        });
       } catch (error) {
           console.log(error);
       }
    })
   
}
//Check how many session in that course,if session > 0 return 1 else return 0
//param id as course_id
exports.checkSessionExist = function(id){
    return new Promise((resolve,reject)=>{
        var course_id = id;
        Session.count({course_id:course_id},function(err,count){
            console.log('count:'+count);
            if(count>0){
                return resolve(1);
            }else{
                return resolve(0);
            }
        });
    });
    
}
//Check that session of that course is existed in db,if session existed  return 1 else return 0
//param id as course_id
exports.checkSessionOfCourseExist = function(id,session_name){
    return new Promise((resolve,reject)=>{
        var course_id = id;
        var name = session_name.replace('\r\r','');
        Session.count({$and:[{course_id:course_id},{session_no:name}]},function(err,count){
            
            if(count==0){
                return resolve(0);
            }else{
                return resolve(1);
            }
        });
    });
    
}
//createSlug
//param id as session_id
exports.createSlug = function(id){
    return new Promise((resolve,reject)=>{
        Session.findOne({session_id:id}).lean().exec((err,se)=>{
            let slug ='';
            if(err) console.error(err);
            else{
                let no = se.session_no.toLowerCase().trim().replace(' ','-');
                slug=no;
            }
            return resolve (slug);
        });
    });
    
}
exports.getFirstSessionInCourse = function(id){
    return new Promise((resolve,reject)=>{
        Session.findOne({course_id:id}).lean().exec((err,se)=>{
            if(err) return reject(err);
            if(se){
                return resolve(se);
            }else{
                return resolve('');
            }
        });
    });
}
exports.getSessionIdList = function(id){
    return new Promise((resolve,reject)=>{
        let sessionIdList=[]
        Session.find({course_id:id}).sort({session_id:1}).lean().exec((err,se)=>{
            if(err) return reject(err);
            if(se){
                se.forEach(e => {
                    sessionIdList.push(e.session_id);
                });
                return resolve(sessionIdList);
            }else{
                return resolve('');
            }
        });
    });
}
exports.getLastSessionId = function(){
    return new Promise((resolve,reject)=>{
        Session.findOne({}).sort({session_id:-1}).lean().exec((err,se)=>{
            if(err) return reject(err);
            if(se){
                return resolve(se.session_id);                
            }else{
                return resolve(0);
            }
        })
    })
}
exports.removeSession = function(seId){
    return new Promise((resolve,reject)=>{
        Session.findOneAndRemove({session_id:seId}).exec((err,se)=>{
            if(err) return reject(err)
            return resolve('done');
        });
    });
}
exports.getSessionListFormatForSync = function(id){
    return new Promise((resolve,reject)=>{
        let sessionlist=[];
        Session.find({course_id:id}).lean().exec((err,sessions)=>{    
            if(err) {
                return reject(err)
            };           
            for(let session of sessions){
                let timeformat = formatDataFromServer(session.session_date);
                let data ={
                    id:session.session_id,
                    date:timeformat.toISOString(),
                    info:JSON.stringify(session)
                    // info:''
                }
                sessionlist.push(data);
            }
            return resolve(sessionlist); 
        }); 
    })
}
exports.updateSession = function(seid,newSessionNo,newSessionDate){
    return new Promise((resolve,reject)=>{
        Session.update({ session_id: seid }, { $set: {session_no:newSessionNo,session_date:newSessionDate}},function(err,raw){
            if(err) return reject(err);
            let message = 'Updated <b>'+newSessionNo+'</b> successfully'
            let status =['succeed',message]
            return resolve(status)
        });
    })
}
function formatDataFromServer(dataNotFormat){
    let a = [];
    let date = new Date(dataNotFormat);
    a[0] = date.getDate();
    a[1] = date.getMonth()+1;
    a[2] = date.getFullYear();
    a[3] = date.getHours();
    a[4] = date.getMinutes();
    a[5] = date.getSeconds();
    a[6] = (a[3] > 11) ? "PM" : "AM";
    if(a[3] > 12) {
      a[3] -= 12;
    } else if(a[3] == 0) {
      a[3] = "12";
    }
    if(a[4] < 10) {
        a[4] = "0" + a[4];
    }
    let formatTime = new Date(a[2]+'/'+a[1]+'/'+a[0]+' '+a[3]+':'+a[4]+':'+a[5]+' '+a[6]);
    return formatTime;
}