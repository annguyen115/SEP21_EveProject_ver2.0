'use strict';
var mongoose = require('mongoose');
var Session = mongoose.model('Session');
var Course = mongoose.model('Course');
var Attendance = mongoose.model('Attendance');
var Student = mongoose.model('Student');
var moment = require('moment');
var https = require('https');

exports.checkSessionExist = function(session_id,course_id){
    return new Promise((resolve,reject)=>{  
        Session.findOne({session_id:session_id,course_id:course_id}).lean().exec((err,se)=>{
            if(err) return reject(err);
            if(se) return resolve('found');
            else return resolve('notfound');
        })
    })
}
exports.getOneSession = function(session_id,course_id){
    return new Promise((resolve,reject)=>{
        Session.findOne({session_id:session_id,course_id:course_id}).lean().exec((err,se)=>{
            if(err) return reject(err);
            if(se) return resolve(se);
            else return resolve('notfound');
        })
    })
}
exports.checkCourseExist = function(course_id){
    return new Promise((resolve,reject)=>{
        Course.findOne({course_id:course_id}).lean().exec((err,co)=>{
            if(err) return reject(err);
            if(co) return resolve('found');
            else return resolve('notfound');
        })
    })
}
exports.sesssionIsEmpty = function(course_id){
    return new Promise((resolve,reject)=>{
        Session.findOne({course_id:course_id}).lean().exec((err,se)=>{
            if(err) return reject(err);
            if(se) return resolve('notempty');
            else return resolve('empty');
        })
    })
}
exports.removeAllCourse = function(){
    return new Promise((resolve,reject)=>{
        Course.remove({}).lean().exec((err,raw)=>{
            if(err) return reject(err);
            else return resolve();
        })     
    })
}
exports.removeAllStudent = function(){
    return new Promise((resolve,reject)=>{
        Student.remove({}).lean().exec((err,raw)=>{
            if(err) return reject(err);
            else return resolve();
        })     
    })
}
exports.courseIsEmpty = function(){
    return new Promise((resolve,reject)=>{
        Course.findOne({}).lean().exec((err,co)=>{
            if(err) return reject(err);
            if(co) return resolve('notempty');
            else return resolve('empty');
        })
    })
}
exports.checkStudentExist = function(student_id,course_id){
    return new Promise((resolve,reject)=>{
        Student.findOne({student_id:student_id,course_id:course_id}).lean().exec((err,co)=>{
            if(err) return reject(err);
            if(co) return resolve('found');
            else return resolve('notfound');
        })
    })
}
exports.checkStudentExistByName = function(student_name){
    return new Promise((resolve,reject)=>{
        student_name = student_name.toUpperCase().trim();
        Student.findOne({$or:[{fullname:student_name},{firstname:student_name},{lastname:student_name}]}).lean().exec((err,st)=>{
            if(err) return reject(err);
            if(st) return resolve('found');
            else return resolve('notfound');
        })
    })
}
exports.checkStudentExistByID = function(student_id){
    return new Promise((resolve,reject)=>{
        student_id = student_id.toUpperCase().trim();
        Student.findOne({student_id:student_id}).lean().exec((err,st)=>{
            if(err) return reject(err);
            if(st) return resolve('found');
            else return resolve('notfound');
        })
    })
}
exports.StudentIsEmpty = function(course_id){
    return new Promise((resolve,reject)=>{
        Student.findOne({course_id:course_id}).lean().exec((err,co)=>{
            if(err) return reject(err);
            if(co) return resolve('notempty');
            else return resolve('empty');
        })
    })
}
exports.checkStudentExistOnServer = function(student_id){
    return new Promise((resolve,reject)=>{
        let url = 'https://entool.azurewebsites.net/SEP21/GetStudent?code=' + student_id;
        https.get(url,response=>{
            var json=''
            response.on('data',d=>{
                json+=d;
            }).on('end',()=>{
                var data_response = JSON.parse(json);
                return resolve(data_response);
            })
        })
    })
}
exports.getOneStudent = function(student_id,course_id){
    return new Promise((resolve,reject)=>{
        Student.findOne({student_id:student_id,course_id:course_id}).lean().exec((err,co)=>{
            if(err) return reject(err);
            if(co) return resolve(co);
            else return resolve('notfound');
        })
    })
}
exports.checkDateFormat = function(date){
    return new Promise((resolve,reject)=>{
        let check = moment(date,'D/M/YYYY h:mm a',true).isValid()
        return resolve(check);
    })
}
exports.getOneAttendance = function(attendance_id){
    return new Promise((resolve,reject)=>{
        Attendance.findOne({attendance_id:attendance_id}).lean().exec((err,at)=>{
            if(err) return reject(err);
            if(at) return resolve(at);
            else return resolve('notfound');
        })
    })
}
exports.formatDateFromServer = function(notformat){
    let a = [];
    let date = new Date(notformat);
    a[0] = date.getDate();
    a[1] = date.getMonth()+1;
    a[2] = date.getFullYear();
    a[3] = date.getHours();
    a[4] = date.getMinutes();
    a[5] = (a[3] > 11) ? "PM" : "AM";
    if(a[3] > 12) {
      a[3] -= 12;
    } else if(a[3] == 0) {
      a[3] = "12";
    }
    if(a[4] < 10) {
        a[4] = "0" + a[4];
    }
    return a;
  }