process.env.NODE_ENV = 'test';
var mongoose = require('mongoose');
let Attendance = require('../models/attendance');
let Student =require ('../models/student');
let Session = require('../models/session');
let Course = require('../models/course');
let request = require('supertest')
var app = require('../../app');
var chai = require('chai');
var chaiHttp = require('chai-http');
// var should = chai.should();
var should = require('should');
var expect = chai.expect;
chai.use(chaiHttp);
var server = request.agent(app);
var Service = require('./hunterService');
// fix Async in Mocha
// more information
//https://labs.chiedo.com/blog/async-mocha-tests/
//https://staxmanade.com/2015/11/testing-asyncronous-code-with-mochajs-and-es7-async-await/
var mochaAsync = (fn) => {
    return done => {
      fn.call().then(done, err => {
        done(err);
      });
    };
};
// var mochaAsync2 = (fn) => {
//     return async (done) => {
//         try {
//             await fn();
//             done();
//         } catch (err) {
//             done(err);
//         }
//     };
// };
//reset all document in db for test
console.log('WARNING!!! Bug Hunter is running...');
before('Reset Db(local)',(done)=>{
    console.log('Reset Db(local)');
    Attendance.remove({},(err)=>{
        if(err) done(err);
    });
    Student.remove({student_id:'T152007'},(err)=>{
        if(err) done(err);
    });     
    Session.remove({},(err)=>{
        if(err) done(err);
    });    
    Course.remove({},(err)=>{
        if(err) done(err);
    });
    done(); 
})
describe('Before Login', () => {
    describe('Authentication Pages [index,Studentlist,Profile,Student\'s Statistic]', () => {
        it('if you dont login and request to Index, it should recdirect to login', (done) => {
            request(app)
            .get('/')
            .end((err, res) => {
                res.status.should.be.equal(302);
                expect(res).to.redirectTo('/login.vlu');
                done();
            })
        })
        it('if you dont login and request to Studentlist, it should recdirect to login', (done) => {
            request(app)
            .get('/studentlist/HC3-software-project.vlu')
            .end((err, res) => {
                res.status.should.be.equal(302);
                expect(res).to.redirectTo('/login.vlu');
                done();
            })
        })
        it('if you dont login and request to Profile, it should recdirect to login', (done) => {
            request(app)
            .get('/profile.vlu')
            .end((err, res) => {
                res.status.should.be.equal(302);
                expect(res).to.redirectTo('/login.vlu');
                done();
            })
        })
        it('if you dont login and request to Student\'s Statistic, it should recdirect to login', (done) => {
            request(app)
            .get('/student-statistic.vlu')
            .end((err, res) => {
                res.status.should.be.equal(302);
                expect(res).to.redirectTo('/login.vlu');
                done();
            })
        })
    })
});

describe('After Login', () => {
    describe('Login GET', () => {
        it('if you request to Login, it should render login', (done) => {
            request(app)
            .get('/login.vlu')
            .end((err, res) => {
                // res.should.have.status(200);
                res.status.should.be.equal(200);
                done();
            });
        });
    })
    describe('Login POST', () => {
        describe('Login POST - Failed(Wrong User or Password)', () => {
            it('if you input invalid username or password, it should redirect to login', (done) => {
                request(app)
                .post('/login.vlu')
                .send({username:'dsada',password:'asad'})
                // .expect(302)
                .end((err,res)=>{
                    setTimeout(done, 500);
                    res.status.should.be.equal(302);
                    expect(res).to.redirectTo('/login.vlu');
                })
            });
        })
        describe('Login POST - Success(Valid User amd Password)', () => {
            it('if you input valid username and password, it should render index', (done) => {
                server.post('/login.vlu')
                .send({username:'lythihuyenchau',password:'pistarebra'})
                // .expect(200)
                // .expect('Location', '/', done)
                .end((err,res)=>{
                    // setTimeout(done, 500);
                    res.status.should.be.equal(302);
                    expect(res).to.redirectTo('/');
                    done();
                })
                // .end((err, res) => {
                //     // should.not.exist(err);
                //     setTimeout(done, 500);
                //     // res.should.have.status(200);
                //     res.status.should.be.equal(200);
                //     res.header['location'].should.include('/')
                //     // expect(res.statusCode).to.equal(200);
                //     // expect(res.headers['location']).to.match(/^https:\/\/github.com\/login\/oauth\/authorize/);
                //     done();
                //     //res.header['location'].should.include('/')
                // });
            });
            // after((done)=>{
            //     Course.remove({});
            //     done();
            // })
        })
        
    })
    describe('Index GET', () => {
        describe('Index GET - Success', () => {
            it('remove all Course in db for test below',mochaAsync(async () => {
                await Service.removeAllCourse();
                let check = await Service.courseIsEmpty();
                expect(check).to.equal('empty');     
            }));  
            it('if you login and request to Index, it should render index',(done) => {
                server.get('/').end(async(err, res) => {
                    try {
                        let check = await Service.checkCourseExist('HC3');
                        expect(check).to.equal('found');
                        // res.should.have.status(200);
                        expect(res.body).to.be.an('object');
                        expect(res.body).should.be.not.empty;
                        // expect(res).to.have.param('user');
                        res.status.should.be.equal(200);
                        done();
                    } catch (err) {
                        done(err);
                    }
                });
            });
        })
    })
    describe('StudentList GET', () => {
        describe('StudentList GET - Success', () => {
            it('if you login and request to Studentlist whose course_id existed, it should render Studentlist', (done) => {
                server.get('/studentlist/HC3-software-project.vlu').end((err, res) => {
                    // setTimeout(done, 500);
                    // res.should.have.status(200);
                    res.status.should.be.equal(200);
                    done();
                });
            });
        })
        describe('StudentList GET - Failed', () => {
            it('if you login and request to Studentlist whose course_id not existed, it should render Studentlist', (done) => {
                server.get('/studentlist/HC4-software-project.vlu').end((err, res) => {
                    res.status.should.be.equal(404);
                    done();
                });
            });
        })
    })
    describe('Profile GET', () => {
        describe('Profile GET - Success', () => {      
            it('if you login and request to Profile, it should render Profile', (done) => {
                server.get('/profile.vlu').end((err, res) => {
                        // setTimeout(done, 500);
                        // res.should.have.status(200);
                        res.status.should.be.equal(200);
                        done();
                });
            });
        })
    })  
    describe('Student\'s Statistic GET', () => {
        describe('Student\'s Statistic GET - Success', () => {
            it('if you login and request to Student\'s Statistic, it should render Student\'s Statistic', (done) => {
                server.get('/student-statistic.vlu').end((err, res) => {
                        // setTimeout(done, 500);
                        // res.should.have.status(200);
                        res.status.should.be.equal(200);
                        done();
                });
            });    
        })
        
    })
    
});
describe('Ajax Call',()=>{
    describe('Add Session POST', () => {
        describe('Add Session POST - Success(Session Valid,Course_ID existed)', () => {
            it('check Session is valid,Course_ID existed',mochaAsync(async(done)=>{
                let checkCourse = await Service.checkCourseExist('HC3');
                let checkDate = await Service.checkDateFormat('28/6/2018 11:34 PM')
                expect(checkCourse).to.equal('found')
                expect(checkDate).to.equal(true)
            }))
            it('if Session is valid and Course_ID existed, It should return status success,new Session is created success,new session will be added to Db(local)',(done) => {
                server.post('/action/add-session')
                .send({newSessionDay:{name:'28/6/2018 11:34 PM',format:'6/28/2018 11:34 PM',course_id:'HC3'}})
                .end(async(err,res)=>{
                    try {
                        let check = await Service.checkSessionExist(1,'HC3');
                        expect(check).to.equal('found');
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('succeed');
                        expect(res.body.name).to.equal('1st Session');
                        expect(res.body.date).to.equal('28/6/2018 11:34 PM');
                        expect(res.body.id).to.equal(1);
                        done();
                    } catch (err) {
                        done(err);
                    }
                })
            })
        })        
    })
    describe('Edit Session POST', () => {
        describe('Edit Session POST - Success(Session Valid,Course_id existed,Session_id existed)', () => {
            it('Check Session Valid,Course_id existed,Session_id existed',mochaAsync(async(done) => {
                let checkSession = await Service.checkSessionExist(1,'HC3');
                let checkCourse = await Service.checkCourseExist('HC3');
                expect('Buoi 1').to.have.length.lessThan(16);
                expect(checkSession).to.equal('found');
                expect(checkCourse).to.equal('found');
                //ko kiểm tra dc new_date = moment
                // Version3.0 - request fix
            }))
            it('if New Session Name is valid(<16) or null and New Session Date is valid, It should return status success,session will be updated successfully', (done) => {
                server.post('/action/edit-session')
                .send({newSession:{id:1,course_id:'HC3',new_name:'Buoi 1',new_date:'Fri Jun 29 2018 15:36:00 GMT+0700 (Giờ Đông Dương)'}})
                .end(async(err,res)=>{
                    try {
                        let session = await Service.getOneSession(1,'HC3');
                        expect(session.session_no).to.equal('Buoi 1');
                        let newSessionDateFormat = await Service.formatDateFromServer('Fri Jun 29 2018 15:36:00 GMT+0700 (Giờ Đông Dương)')
                        let ServerDateFormat = await Service.formatDateFromServer(session.session_date);
                        expect(ServerDateFormat[0]+'/'+ServerDateFormat[1]+'/'+ServerDateFormat[2]+' '+ServerDateFormat[3]+':'+ServerDateFormat[4]+' '+ServerDateFormat[5]).to.equal(newSessionDateFormat[0]+'/'+newSessionDateFormat[1]+'/'+newSessionDateFormat[2]+' '+newSessionDateFormat[3]+':'+newSessionDateFormat[4]+' '+newSessionDateFormat[5])
                        
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('succeed');
                        expect(res.body.message).to.equal('Updated <b>Buoi 1</b> successfully');
                        done();
                    } catch (err) {
                        done(err);
                    }
                })
            })
        })
        
    })  
    describe('Remove Session POST', () => {
        describe('Remove Session POST - Success(session_id existed)', () => {
            it('check Session_id exist in db(local)',mochaAsync(async(done)=>{
                let check = await Service.checkSessionExist(1,'HC3');
                let session = await Service.getOneSession(1,'HC3');
                expect(check).to.equal('found');
                expect(session.session_no).to.equal('Buoi 1');
                let dateFormat = await Service.formatDateFromServer(session.session_date);
                expect(dateFormat[0]+'/'+dateFormat[1]+'/'+dateFormat[2]+' '+dateFormat[3]+':'+dateFormat[4]+' '+dateFormat[5]).to.equal('29/6/2018 3:36 PM');
            }))
            it('if Session is valid, It should return status success', (done) => {
                server.post('/action/remove-session')
                .send({session:{id:1,no:'Buoi 1',date:'29/6/2018 3:36 PM'}})
                .end(async(err,res)=>{
                    // setTimeout(done, 500);
                    try {
                        let check = await Service.checkSessionExist(1,'HC3');
                        expect(check).to.equal('notfound');
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('succeed');
                        expect(res.body.message).to.equal('Removed <b>Buoi 1 (29/6/2018 3:36 PM)</b>  successfully');
                        done();
                    } catch (err) {
                        done(err);
                    }
                })
            })
        })
    })
    describe('Add Student POST', () => {
        describe('Add Student POST - Success(New Student_ID is valid and dont exist in db', () => {
            it('check new Student dont exist in db(local)',mochaAsync(async()=>{
                let check = await Service.checkStudentExist('T152007','HC3');
                expect(check).to.equal('notfound');
            }))
            it('if New Student ID is valid and they dont exist in db, It should return createStatus success', (done) => {
                server.post('/action/add-student')
                .send({newStudent:{id:'T152007',course_id:'HC3'}})
                .end(async(err,res)=>{
                    try {    
                        let check = await Service.checkStudentExist('T152007','HC3');                
                        expect(check).to.equal('found');
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('succeed');
                        expect(res.body.message).to.equal('Added <b>NGUYỄN ĐÌNH QUỐC AN</b> successfully');
                        expect(res.body.status).to.equal('create successs');
                        done();
                    } catch (err) {
                        done(err);
                    }
                })
            })
        })
        describe('Add Student POST - Failed(New Student_ID is valid and exist in db', () => {
            it('check new Student exist in db(local)',mochaAsync(async()=>{
                let check = await Service.checkStudentExist('T152007','HC3');
                expect(check).to.equal('found');
            }))
            it('if New Student ID is valid and they exist in db, It should return createStatus falied', (done) => {
                server.post('/action/add-student')
                .send({newStudent:{id:'T152007',course_id:'HC3'}})
                .end(async(err,res)=>{
                    try {
                        let student = await Service.getOneStudent('T152007','HC3')
                        expect(student.student_id).to.equal('T152007');
                        expect(student.fullname).to.equal('NGUYỄN ĐÌNH QUỐC AN');
                        expect(student.firstname).to.equal('AN');
                        expect(student.lastname).to.equal('NGUYỄN ĐÌNH QUỐC');
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('failed');
                        expect(res.body.message).to.equal('<b>NGUYỄN ĐÌNH QUỐC AN</b> has already existed in studentlist of HC3');
                        expect(res.body.status).to.equal('existed');
                        done();
                    } catch (err) {
                        done(err);
                    }
                })
            })
        })
        describe('Add Student POST - Failed(New Student_ID is valid and dont exist eitheer db or server(API)', () => {
            it('check New Student ID not existed in db and Server(API)',mochaAsync(async()=>{
                let checklocal = await Service.checkStudentExist('T123456','HC3');
                let checkserver = await Service.checkStudentExistOnServer('T123456');
                expect(checklocal).to.equal('notfound');
                expect(checkserver.code).to.equal(1);
                expect(checkserver.message).to.equal('Student not found.')
            }))
            it('if New Student ID is valid and they dont exist either db or server(API), It should return createStatus failed', (done) => {
                server.post('/action/add-student')
                .send({newStudent:{id:'T123456',course_id:'HC3'}})
                .end((err,res)=>{
                    res.status.should.be.equal(200);
                    expect(res.body.createStatus).to.equal('failed');
                    expect(res.body.message).to.equal('<b>T123456</b> not found in mr Duy\'s db');
                    expect(res.body.status).to.equal('notfound');
                    done();
                })
            })
        })
        
    })
    describe('Remove Student POST', () => {
        describe('Remove Student POST - Failed(Student_id exist and Course_id not exist)', () => {
            it('check New Student ID existed and Course_id not exist',mochaAsync(async()=>{
                let checkStudent = await Service.checkStudentExist('T152007','HC3');
                let checkCourse = await Service.checkCourseExist('HC4');
                expect(checkStudent).to.equal('found');
                expect(checkCourse).to.equal('notfound')
            }))
            it('if Student_ID exist in db and Course_ID not exist in db, It should not remove student from course return createStatus failed', (done) => {
                server.post('/action/delete-student')
                .send({data:{student_id:'T152007',course_id:'HC4'}})
                .end(async(err,res)=>{
                    try {
                        let checkStudent = await Service.checkStudentExist('T152007','HC3');
                        let checkCourse = await Service.checkCourseExist('HC4');
                        expect(checkStudent).to.equal('found');
                        expect(checkCourse).to.equal('notfound');
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('failed');
                        expect(res.body.message).to.equal('There are no students <b>HC4</b>');
                        done();
                    } catch (err) {
                        done(err)
                    }
                })
            })
        })
        describe('Remove Student POST - Success(Student_id exist and Course_id exist)', () => {
            it('check both Student_id and Course_id exist',mochaAsync(async()=>{
                let checkStudent = await Service.checkStudentExist('T152007','HC3');
                let checkCourse = await Service.checkCourseExist('HC3');
                expect(checkStudent).to.equal('found');
                expect(checkCourse).to.equal('found');
            }))
            it('if both Student_ID and Course_ID exist in db, It should remove student from course and should return createStatus success', (done) => {
                server.post('/action/delete-student')
                .send({data:{student_id:'T152007',course_id:'HC3'}})
                .end(async(err,res)=>{
                    try {
                        let checkStudent = await Service.checkStudentExist('T152007','HC3');
                        expect(checkStudent).be.equal('notfound');
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('succeed');
                        expect(res.body.message).to.equal('Removed <b>NGUYỄN ĐÌNH QUỐC AN</b> from <b>HC3</b> successfully');
                        done();
                    } catch (err) {
                        done(err);
                    }
                })
            })
        })
        describe('Remove Student POST - Failed(Student_id not exist and Course_id not exist)', () => {
            it('check both Student_id and Course_id not exist',mochaAsync(async()=>{
                let checkStudent = await Service.checkStudentExist('T123456','HC3');
                let checkCourse = await Service.checkCourseExist('HC4');
                expect(checkStudent).to.equal('notfound');
                expect(checkCourse).to.equal('notfound');
            }))
            it('if both Student_ID and Course_ID not exist in db, It should not remove student from course and should return createStatus failed', (done) => {
                server.post('/action/delete-student')
                .send({data:{student_id:'T123456',course_id:'HC4'}})
                .end(async(err,res)=>{
                    try {
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('failed');
                        expect(res.body.message).to.equal('There are no students <b>HC4</b>');
                        done();
                    } catch (err) {
                        done(err);
                    }
                })
            })
        })
        describe('Remove Student POST - Failed(Student_id not exist and Course_id exist)', () => {
            it('check both Student_id and Course_id not exist',mochaAsync(async()=>{
                let checkStudent = await Service.checkStudentExist('T123456','HC3');
                let checkCourse = await Service.checkCourseExist('HC3');
                expect(checkStudent).to.equal('notfound');
                expect(checkCourse).to.equal('found');
            }))
            it('if Student_ID not exist in db and Course_ID exist in db, It should not remove student from course and should return createStatus failed', (done) => {
                server.post('/action/delete-student')
                .send({data:{student_id:'T123456',course_id:'HC3'}})
                .end((err,res)=>{
                    res.status.should.be.equal(200);
                    expect(res.body.createStatus).to.equal('failed');
                    expect(res.body.message).to.equal('Cant find <b>T123456</b> in <b>HC3</b>');
                    done();
                })
            })
        })
    })
    describe('Search Student Infor POST',()=>{
        before((done)=>{
            server.post('/action/api/sync-student-from-server')
                .send({course:{id:'HC3',name:'Software Project'}})
                .end((err,res)=>{
                    done();
                })
        })
        after((done)=>{
            Student.remove({});
            done();
        })
        describe('Search Student Infor POST- Success(search Student by name && Student exist in Db)',()=>{
            
            it('check Student Name exist in db',mochaAsync(async()=>{
                let checkStudentUppercase = await Service.checkStudentExistByName('ANH');
                let checkStudentLowerCase = await Service.checkStudentExistByName('anh');
                let checkStudentTrim = await Service.checkStudentExistByName(' Anh ');
                expect(checkStudentUppercase).to.equal('found');
                expect(checkStudentLowerCase).to.equal('found');
                expect(checkStudentTrim).to.equal('found');
            }))
            it('if search "ANH",it should return object which length = 3', (done) => {
                server.post('/action/search-by-course')
                .send({term:{data:'ANH',course_id:'HC3'}})
                .end((err,res)=>{
                    res.status.should.be.equal(200);
                    expect(res.body).to.have.length(3);
                    expect(res.body).to.be.an('array');
                    done();
                })
            })
            
        })
        describe('Search Student Infor POST- Success(search Student by Student_id && Student exist in Db)',()=>{
            it('check Student ID exist in db',mochaAsync(async()=>{
                let checkStudentUppercase = await Service.checkStudentExistByID('T150510');
                let checkStudentLowerCase = await Service.checkStudentExistByID('t150510');
                let checkStudentTrim = await Service.checkStudentExistByID(' t150510 ');
                expect(checkStudentUppercase).to.equal('found');
                expect(checkStudentLowerCase).to.equal('found');
                expect(checkStudentTrim).to.equal('found');
            }))
            it('if search "ANH",it should return object which length = 3', (done) => {
                server.post('/action/search-by-course')
                .send({term:{data:'T150510',course_id:'HC3'}})
                .end((err,res)=>{
                    res.status.should.be.equal(200);
                    expect(res.body).to.have.length(1);
                    expect(res.body).to.be.an('array');
                    done();
                })
            })
        })
        describe('Search Student Infor POST- Failed(search Student by name && Student not exist in Db)',()=>{
            it('check Student Name exist in db',mochaAsync(async()=>{
                let checkStudentUppercase = await Service.checkStudentExistByName('Bug');
                let checkStudentLowerCase = await Service.checkStudentExistByName('Bug');
                let checkStudentTrim = await Service.checkStudentExistByName(' Bug ');
                expect(checkStudentUppercase).to.equal('notfound');
                expect(checkStudentLowerCase).to.equal('notfound');
                expect(checkStudentTrim).to.equal('notfound');
            }))
            it('if search "Bug",it should return object which length = 3', (done) => {
                server.post('/action/search-by-course')
                .send({term:{data:'Bug',course_id:'HC3'}})
                .end((err,res)=>{
                    res.status.should.be.equal(200);
                    expect(res.body).to.have.length(0);
                    expect(res.body).to.be.an('array');
                    done();
                })
            })
            
        })
        describe('Search Student Infor POST- Success(search Student by Student_id && Student not exist in Db)',()=>{
            it('check Student ID exist in db',mochaAsync(async()=>{
                let checkStudentUppercase = await Service.checkStudentExistByID('T123456');
                let checkStudentLowerCase = await Service.checkStudentExistByID('t123456');
                let checkStudentTrim = await Service.checkStudentExistByID(' t123456 ');
                expect(checkStudentUppercase).to.equal('notfound');
                expect(checkStudentLowerCase).to.equal('notfound');
                expect(checkStudentTrim).to.equal('notfound');
            }))
            it('if search "ANH",it should return object which length = 3', (done) => {
                server.post('/action/search-by-course')
                .send({term:{data:'T123456',course_id:'HC3'}})
                .end((err,res)=>{
                    res.status.should.be.equal(200);
                    expect(res.body).to.have.length(0);
                    expect(res.body).to.be.an('array');
                    done();
                })
            })
        })
    })
     //đáng lẽ phải truyền id,mã lớp học,session_id
     // Test Error
    describe.skip('Update Student Attendance',()=>{
        before((done)=>{
            server.post('/action/add-session')
            .send({newSessionDay:{name:'28/6/2018 11:34 PM',format:'6/28/2018 11:34 PM',course_id:'HC3'}})
            .end((err,res)=>{
               done();
            })
        })
        after((done)=>{
            Session.remove({});
            Attendance.remove({});
            Student.remove({});
            done();
        })

        it('it should return createStatus Suceed and Attendance Status udpate successfully', (done) => {
            server.post('/action/update-attendance')
            .send({myattendance:{attendance_id:1,status:1,student_id:'T150510'}})
            .end(async(err,res)=>{
                try {
                    res.status.should.be.equal(200);
                    let attendance = await Service.getOneAttendance(1);
                    let student = await Service.getOneStudent(attendance.student_id,'HC3');
                    expect(attendance.status).to.equal(1);
                    expect(attendance.student_id).to.equal('T150510');
                    expect(res.body.createStatus).to.equal('succeed');
                    expect(res.body.message).to.equal('succeed');
                    done();
                } catch (err) {
                    done(err);
                }
            })
        })
    })
    describe('Synchronize Function',()=>{
        describe('Synchronize Studentlist From Server(API) To Db(Local) POST',()=>{
            describe('Synchronize Studentlist From Server(API) To Db(Local) POST - Success(Course_id existed)',()=>{
                it('check Course_ID existed',mochaAsync(async()=>{
                    let checkCourse = await Service.checkCourseExist('HC3');
                    expect(checkCourse).to.equal('found')
                }))
                it('if input Course_ID existed in db, It should return createStatus success', (done) => {
                    server.post('/action/api/sync-student-from-server')
                    .send({course:{id:'HC3',name:'Software Project'}})
                    .end((err,res)=>{
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('succeed');
                        expect(res.body.message).to.equal('Update studentlist of <b>SOFTWARE PROJECT(HC3)</b> successfully');
                        done();
                    })
                })
            })
            describe('Synchronize Studentlist From Server(API) To Db(Local) POST - Failed(Course_id not existed)',()=>{
                it('Course_id not exist',mochaAsync(async()=>{
                    let checkCourse = await Service.checkCourseExist('HC4');
                    expect(checkCourse).to.equal('notfound');
                }))
                it('if input Course_ID not exist in db, It should return createStatus failed', (done) => {
                    server.post('/action/api/sync-student-from-server').send({course:{id:'HC4',name:'Software Project'}})
                    .end((err,res)=>{
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('failed');
                        expect(res.body.message).to.equal('There are no students <b>HC4</b>');
                        done();
                    })
                })
            }) 
        })
        describe('Synchronize Student From Db(Local) To Server(API) POST',()=>{
            describe('Synchronize Student From Db(Local) To Server(API) POST - Success(Course_ID existed)',()=>{
                it('check Course_ID existed',mochaAsync(async()=>{
                    let checkCourse = await Service.checkCourseExist('HC3');
                    expect(checkCourse).to.equal('found')
                }))
                it('if input Course_ID exist in db, It should return createStatus success', (done) => {
                    server.post('/action/api/sync-student-to-server')
                    .send({course_id:'HC3'})
                    .end((err,res)=>{
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('succeed');
                        expect(res.body.message).to.equal('Sync successfully.');
                        expect(res.body.code).to.equal(0);
                        done();
                    })
                })
            })
            describe('Synchronize Student From Db(Local) To Server(API) POST - Success(Course_ID not existed)',()=>{
                it('check Course_ID not existed',mochaAsync(async()=>{
                    let checkCourse = await Service.checkCourseExist('HC4');
                    expect(checkCourse).to.equal('notfound')
                }))
                it('if input Course_ID not exist in db, It should return createStatus failed POST', (done) => {
                    server.post('/action/api/sync-student-to-server')
                    .send({course_id:'HC4'})
                    .end((err,res)=>{
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('failed');
                        expect(res.body.message).to.equal('There are no students HC4');
                        done();
                    })
                })
            }) 
        })
        describe('Synchronize Attendance From Db(Local) To Server(API) POST',()=>{
            describe('Synchronize Attendance From Db(Local) To Server(API) POST - Failed(No session in db)',()=>{
                it('check there are no session in db',mochaAsync(async()=>{
                    let checkSessionIsEmpty = await Service.sesssionIsEmpty('HC3');
                    expect(checkSessionIsEmpty).to.equal('empty')
                }))
                it('if there are no session in db, It should return createStatus failed POST',(done)=>{
                    server.post('/action/api/sync-attendance-to-server')
                    .send({course_id:'HC3'})
                    .end((err,res)=>{
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('failed');
                        expect(res.body.message).to.equal('There are no sessions in <b>HC3</b>');
                        done();
                    })
                })
            })
            describe('Synchronize Attendance From Db(Local) To Server(API) POST - Failed(Course_ID not existed)',()=>{
                it('check Course_ID not exist in db',mochaAsync(async()=>{
                    let checkCourseExist = await Service.checkCourseExist('HC4');
                    expect(checkCourseExist).to.equal('notfound')
                }))
                it('if Course_ID not exist in db, It should return createStatus failed POST',(done)=>{
                    server.post('/action/api/sync-attendance-to-server')
                    .send({course_id:'HC4'})
                    .end((err,res)=>{
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('failed');
                        expect(res.body.message).to.equal('There are no sessions in <b>HC4</b>');
                        done();
                    })
                })
            })
            
            describe('Synchronize Attendance From Db(Local) To Server(API) POST - Success(Course_ID existed,Session Exist,Student Exist)',function() {
                it('insert new Session for synchronize',(done)=>{
                    console.log('insert new session')
                    server.post('/action/add-session').send({newSessionDay:{name:'20/9/2018 11:34 PM',format:'9/20/2018 11:34 PM',course_id:'HC3'}}).end(async(err,res)=>{
                        try {
                            let checkSessionIsEmpty = await Service.sesssionIsEmpty('HC3');
                            expect(checkSessionIsEmpty).to.equal('notempty')
                            done();
                        } catch (err) {
                            done(err);
                        }
                    })
                })
                it('if input course_id exist in db, It should return createStatus success POST',(done)=>{
                    console.log('let sync');
                    server.post('/action/api/sync-attendance-to-server')
                    .send({course_id:'HC3'})
                    .end((err,res)=>{
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('succeed');
                        expect(res.body.message).to.equal('Sync successfully.');
                        done();
                    })
                })
            })
            describe('Synchronize Attendance From Db(Local) To Server(API) POST - Failed(There are no Student in db(Course:HC3))',function() {
                it('remove all student from HC3 for test below',mochaAsync(async()=>{
                    await Service.removeAllStudent();
                    let check = await Service.StudentIsEmpty('HC3');
                    expect(check).to.equal('empty');
                }))
                     
                it('if input course_id exist in db, It should return createStatus success POST',(done)=>{
                    server.post('/action/api/sync-attendance-to-server')
                    .send({course_id:'HC3'})
                    .end((err,res)=>{
                        res.status.should.be.equal(200);
                        expect(res.body.createStatus).to.equal('failed');
                        expect(res.body.message).to.equal('There are no students in <b>HC3</b>');
                        done();
                    })
                })
            }) 
        })
    })
});
describe('Logout',()=>{
    describe('Logout GET - Success', () => {
        it('if logout success, it should redirect to login',(done)=>{
            server.get('/logout.vlu')
            .end((err,res)=>{
                res.status.should.be.equal(302);
                expect(res).to.redirectTo('/login.vlu');
                done();
            })
        })
    })
})