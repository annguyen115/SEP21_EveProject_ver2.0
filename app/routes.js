var courseController = require('./controllers/courseController');
var studentController = require('./controllers/studentController');
var sessionController = require('./controllers/sessionController');
var attendanceControler = require('./controllers/attendanceController');
var commonController = require('./controllers/commonController');
var exportxlsxController = require('./controllers/exportxlsxController');
var importxlsxController = require('./controllers/importxlsxController');
let statisticController = require('./controllers/statisticController')

var express = require('express');
var router = express.Router();
var passport = require('passport');
// require('../config/passport')(passport); 


let nodeXlsx = require('node-xlsx-style');
let fs = require('fs');
var path = require("path");
var mypath = virtual_path;

     /**************************** My Router *************************/
    /* ~~> View HQ. */
    /* Login. */
    router.route('/login.vlu').get(renderLogin)
       .post(passport.authenticate('local-login',{
          successRedirect : mypath+'/',
          failureRedirect : mypath+'/login.vlu', 
          failureFlash : true
    }))
    /* Logout. */
    router.get('/logout.vlu',logout);

    /* Index. */
    router.get('/',isLoggedIn,renderIndex);

    /* Student List. */
    router.get('\/studentlist\/:id(\\w+)-:name([\\s\\w-]+).vlu', isLoggedIn,isAuthorize,renderStudentList);

    /* Profile. */
    router.get('/profile.vlu', isLoggedIn, renderProfile);

    /* Attendance online. */
    router.route('/attendance').post(isLoggedIn,getAttendanceSelect)  
    router.get('\/attendance\/:id(\\w+)-attendance-checking-:name([\\s\\w-]+)-:seid(\\w+).vlu',isLoggedIn,renderAttendanceOnline); 
    
    /* Student Statistic. */
    router.get('/student-statistic.vlu',isLoggedIn,renderStudentStatistic);
    

    
    /* ~~> Action HQ. */
    /* Sync Student From API. */  
    router.post('/action/api/sync-student-from-server', isLoggedIn,syncStudentFromServer);
    /* Sync Student To API. */
    router.post('/action/api/sync-student-to-server',isLoggedIn,syncStudentToServer);
    /* Sync Attendance To API. */
    router.post('/action/api/sync-attendance-to-server',isLoggedIn,syncAttendanceToServer);

    /* Add Session. */
    router.post('/action/add-session', isLoggedIn,addSession);
    /* Remove Session. */
    router.post('/action/remove-session', isLoggedIn,removeSession);
    /* Edit Session. */
    router.post('/action/edit-session',isLoggedIn,editSession);

    /* Add Student. */
    router.post('/action/add-student', isLoggedIn,addStudent);
    /* Update Student Attendance. */
    router.post('/action/update-attendance',isLoggedIn,updateAttendance);
    /* Search Student information. */ 
    router.post('/action/search-by-course',isLoggedIn,searchByCourse);
    /* Delete Student Form Course. */
    router.post('/action/delete-student',isLoggedIn,removeStudent);

    /* Get Student Information. */
    router.post('/action/get-student-infor',isLoggedIn,getStudentInfor);

    /* Get Attendance Statistic. */
    router.post('/action/attendance-statistics',isLoggedIn,getAttendanceStatistic);

    /* Export xlsx File. */
    router.get('/action/export-excel/:id',isLoggedIn,exportXLSX);
    /* Import Xlsx File. */
    router.post('/action/import-excel',isLoggedIn,importXLSX);

    router.get('/action/studentlist-datatable-getdata',isLoggedIn,studentlistDataTable);
    router.get('/action/attendancechecking-datatable-getdata',isLoggedIn,attendanceCheckingDataTable);
    router.get('/action/update-session-list',isLoggedIn,updateSessionList);









    /* ~~> Test Area. */
    router.get('/test', async function(req,res){
        let course_id = "HC1";
        let session_id=1;
        let data="T152007"
        try {
            let testData = await studentController.getSelectedAttendance(course_id,session_id);
            let se = testData[0].oneattendance.session;
            console.log(se);
            testData  = JSON.stringify(testData,null,' ');
            fs.writeFile(path.join(__dirname,'/../',"/test/log"),testData, function(err){
                if(err) {
                    console.log(err);
                }else{
                    console.log("The file was saved!");
                }    
            })
            
        } catch (err) {
            console.error(err);
        }
        res.end();
    }); 
    router.get('/b', function(req,res){
        let currentday = new Date();
        let day = currentday.getDate();
        let month = currentday.getMonth();
        let  year = currentday.getFullYear();
        let hour = currentday.getHours();
        let minutes = currentday.getMinutes();
        let amPM = (hour > 11) ? "PM" : "AM";
        if(hour > 12) {
          hour -= 12;
        } else if(hour == 0) {
          hour = "12";
        }
        if(minutes < 10) {
            minutes = "0" + minutes;
        }
        console.log(month+'/'+day+'/'+year+' '+hour+':'+minutes+' '+amPM);
        var date = new Date(month+'/'+day+'/'+year+' '+hour+':'+minutes+' '+amPM);
        var test = date.getMinutes();
        console.log('date:'+date)
        console.log('test:'+test); 
    });
    router.get('/testview',isLoggedIn, function(req,res){
         let user = req.user;
         res.render('testview',{user:user});
    });
    router.get('/table',isLoggedIn,renderTable);
    router.get('/button',isLoggedIn,renderFloatingButton)
    
    module.exports = router;
    /**************************** My Function *************************/
    /* Get Course List*/
    /* Params: user as object for storing user infor */
    // let getCourselist = (req,res,user)=>{
    //     return new Promise((resolve,reject)=>{
    //         var courseList = userController.getCourselist=(req,res,user);
    //         return resolve(courseList);
    //     });
    // }

    /* ~~> Test Function HQ */
    async function renderTable(req,res){
        let user = req.user;
        let course_id = req.session.course_id;
        course_id='HC1'
        let sessionlist = await sessionController.getSessionList(course_id);
        let mydata = {
            user:user,
            path:mypath,
            sessionlist:sessionlist 
        }
        res.render('tabledata',mydata);
    }
    function renderFloatingButton(req,res){
        let user = req.user;
        let mydata = {
            user:user,
            path:mypath,
        }
        res.render('floatingbutton',mydata);
    }
    /* ~~> Common Function HQ */
    /* Check login*/
    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect(mypath+'/login.vlu');
    }
    function logout(req,res){
        req.logout();
        req.session.destroy;
        res.redirect(mypath+'/login.vlu');
        // let fullurl = req.protocol + '://' + req.get('host');
        // console.log(fullurl);
        // res.redirect(fullurl +mypath+'/login');
    }
    async function isAuthorize(req,res,next){
        let user_id = req.user.lecturer_id;
        let course_id = req.params.id;
        let coursecheck = await courseController.checkExistAndAuthorize(user_id,course_id);
        console.log(coursecheck);
        if(coursecheck=='notexist'){
            return res.status(404).render('404');
        }else if (coursecheck=='notmatch'){
            return res.status(403).render('403');
        }else{
            return next();
        }
    }
    function formatDateFromServer(notformat){
        let a = [];
        let date = new Date(notformat);
        a[0] = date.getDate();
        a[1] = date.getMonth()+1;
        a[2] = date.getFullYear();
        a[3] = date.getHours();
        a[4] = date.getMinutes();
        a[5] = (a[3] > 11) ? "PM" : "AM";
        if(a[3] > 12) {
        a[3] -= 12;
        } else if(a[3] == 0) {
        a[3] = "12";
        }
        if(a[4] < 10) {
            a[4] = "0" + a[4];
        }
        return a;
    }

    /* ~~> Render view HQ */
    function renderLogin(req,res){
        var mydata = {
            message: req.flash('loginMessage'),
            path: mypath
        }
        req.logout();
        res.render('newlogin', mydata)
    }
    async function renderIndex(req,res){
        console.log('render index');
        let user = req.user;
        let listCourse = await courseController.getCourselist(user);
        await courseController.insertCourse(listCourse,user);
        var mydata = {
            user:user,
            listCourse:listCourse,
            path:mypath
        }
        res.status(200).render('index',mydata);
    }
    async function renderStudentList(req,res){
        try {
            console.log('studentist render');
            let user = req.user;
            let course_id =req.params.id;
            req.session.course_id=course_id;
            //get studentlist
            let studentList = await studentController.getStudentList(course_id);
            req.session.studentlist=studentList;
            //get sessionlist
            let sessionlist = await sessionController.getSessionList(course_id);
            req.session.sessionlist =sessionlist;
            // console.log(sessionlist);
            let mydata = {
                user:user,
                path:mypath,
                course_id:course_id,
                sessionlist:sessionlist 
            }
            res.render('studentlist',mydata);
        } catch (err) {
            console.log(err);
        }
    }
    function renderProfile(req,res){
        let user = req.user;
        var mydata = {
            user:user,
            path:mypath
        }
        res.render('profile',mydata);
    }
    async function getAttendanceSelect(req,res){
        let sessionID = req.body.session_id;
        let course_id = req.body.course_id;
        let slug = await sessionController.createSlug(sessionID);
        let fullurl = req.protocol + '://' + req.get('host');
        // console.log(fullurl)
        res.redirect(fullurl +mypath+'/attendance/'+course_id+'-attendance-checking-'+slug+'-'+sessionID+'.vlu');
    }
    async function renderAttendanceOnline(req,res){
        let user = req.user;
        let course_id =req.params.id;
        let sessionID =  req.params.seid;
        let sessionList =  req.session.sessionlist;
        let slug= await courseController.getSlug(course_id);
        let attendanceinfor = await studentController.getSelectedAttendance(course_id,sessionID);
        let mydata = {
            user:user,
            attendlist:attendanceinfor,
            course_id:course_id,
            sessionlist:sessionList,
            slug:slug,
            path:mypath,
        }
        res.render('attendance',mydata);
    }
    async function renderStudentStatistic(req,res){
        let user = req.user;
        // get studentlist from all courses
        let studentlist = await studentController.getStudentListFromAllCourse();
        let mydata = {
            user:user,
            studentlist:studentlist,
            path:mypath
        }
        res.render('filter',mydata);
    }


    /* ~~> Action Function HQ*/
    async function syncStudentFromServer(req,res){
        let data = req.body.course;
        let courseID = data.id;
        let message;
        let createStatus = 'failed';
        let check = await courseController.checkCourseExist(courseID);
        if(check=='found'){
            //  insert student into localdb if it doesnt exist////
            let newStudentID = await studentController.insertStudent(courseID);
            let checkSessionExist = await sessionController.getFirstSessionInCourse(courseID);
            if(newStudentID.length>0){
                if(checkSessionExist!=''){
                    let sessionlist = await sessionController.getSessionIdList(courseID);
                    console.log('selist:'+sessionlist);
                    let lastAttendanceID = await attendanceControler.getLastAttendanceId();
                    console.log('last:'+lastAttendanceID);
                    for(let i in newStudentID){
                        for(let j in sessionlist){
                            let a = await attendanceControler.findAndCreateAttendanceOfOneStudent(courseID,newStudentID[i],sessionlist[j],lastAttendanceID);
                            console.log(a);
                            if(a!=''){
                                console.log('plus');
                                lastAttendanceID++;
                            }
                        }
                    }
                    
                }
            }
            createStatus = 'succeed';
            let name = data.name+'('+data.id+')';
            message =  'Update studentlist of <b>'+name.toUpperCase()+'</b> successfully' 
        }
        else{
            message='There are no students <b>'+courseID+'</b>';
        }
        let response = {
            createStatus:createStatus,
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    async function syncStudentToServer(req,res){
        let message,sync,code;
        let status='failed';
        try {
            let courseID = req.body.course_id;
            let user = req.user;
            let stlist = await studentController.getStudentIDArray(courseID);
            if(stlist!=''){
                sync = await studentController.syncStudent(user,courseID,stlist);
                code = sync.code;
                if(sync.code!=0){
                    message=sync.message;
                }else{
                    status='succeed';
                    message=sync.message;
                }
            }else{
                message='There are no students '+courseID;
            }
        } catch (err) {
            message=err;
        }
        let response = {
            createStatus:status,
            message : message,
            code : code,
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    async function syncAttendanceToServer(req,res){
        let message;
        let status='failed';
        try {
            let courseID = req.body.course_id;
            let user = req.user;
            let attendance=[];
            let firstSession = await sessionController.getFirstSessionInCourse(courseID);
            if(firstSession!=''){
                let stList = await studentController.getStudentList(courseID);
                if(stList!=''){
                    //chuẩn bị dữ liệu
                    //sessionlist
                    let se = await sessionController.getSessionIdList(courseID);
                    let sessions = await sessionController.getSessionListFormatForSync(courseID);
                    for(let st of stList){
                        let result = await attendanceControler.getAttendanceListFormatForSync(courseID,st.student_id,se)                        
                        let a = {
                            student : st.student_id,
                            checklist : result[0],
                            info : result[1]
                        }
                        attendance.push(a);
                    }
                    //chuẩn bị data xong,tiến hành sync
                    let a = await attendanceControler.syncAttendance(user,courseID,sessions,attendance);
                    status = 'succeed';
                    message = a.message;
                }else{
                    message='There are no students in <b>'+courseID+'</b>';    
                }
            }else{
                message='There are no sessions in <b>'+courseID+'</b>';
            }
        } catch (err) {
            message = err;
        }
        let response = {
            createStatus:status,
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    async function addStudent(req,res){
        let data = req.body;
        // let course_id = req.session.course_id;
        let course_id = data.newStudent.course_id;
        let message = '';
        let createStatus='';
        try {
            let result = await studentController.createStudent(course_id,data.newStudent.id);
            let status = result[0];
            let studentname=result[1];
            if(status=='existed'){
                createStatus='failed';
                message = '<b>'+studentname+'</b> has already existed in studentlist of ' +course_id;
            }else if(status=='notfound'){
                createStatus='failed'
                message ='<b>'+data.newStudent.id+'</b> not found in mr Duy\'s db';
            }else if(status=='create successs'){
                createStatus='succeed';
                message = 'Added <b>'+studentname+'</b> successfully';
                let count = await attendanceControler.countStudentAttendance(course_id,data.newStudent.id);
                let lastAttendanceID = await attendanceControler.getLastAttendanceId();
                let newAttendance = await attendanceControler.createStudentAttendance(course_id,data.newStudent.id,count,lastAttendanceID);
            }      
            let response = {
                createStatus:createStatus,
                message : message,
                status:status
            };
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.json(response);
        } catch (error) {
            console.error(error);
        }
    }
    async function removeStudent(req,res){
        let studentID = req.body.data.student_id;
        let course_id = req.body.data.course_id;
        let message;
        let status = 'failed';
        try {
            let check = await courseController.checkCourseExist(course_id);
            if(check=='found'){
                let result = await studentController.removeCourseID(course_id,studentID);
                status = result[0];
                if(result[0]=='succeed'){
                    message = 'Removed <b>'+result[1]+'</b> from <b>'+course_id+'</b> successfully';
                }else if(result[0]=='failed'){
                    message='Cant find <b>'+result[1]+'</b> in <b>'+course_id+'</b>';       
                }
            }else{
                status = 'failed';
                message = 'There are no students <b>'+course_id+'</b>';
            }
        } catch (err) {
            message =err;
            console.log(err);
        }
        let response = {
            createStatus:status,
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    async function addSession(req,res){
        let data = req.body;
        // let course_id = req.session.course_id;
        let course_id = data.newSessionDay.course_id;
        let studentlist = req.session.studentlist;
        let message = '';
        let createStatus='';
        let sessionNew = data.newSessionDay.format;
        try {
            let lastSessionID = await sessionController.getLastSessionId();
            let newSession = await sessionController.createSession(course_id,sessionNew,lastSessionID);
            let lastAttendanceID = await attendanceControler.getLastAttendanceId();
            let done = await attendanceControler.createAttendance(course_id,studentlist,newSession.session_id,lastAttendanceID);
            createStatus ='succeed';
            message='Added ' +newSession.session_no+' ('+data.newSessionDay.name+') '+' successfully';
            let response = {
                createStatus:createStatus,
                message : message,
                name:newSession.session_no,
                date:data.newSessionDay.name,
                id : newSession.session_id
            };
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.json(response);
        } catch (error) {
            console.error(error);
        }
    }
    async function editSession(req,res){
        let createStatus = 'failed';
        let message;
        try {
            let data = req.body.newSession;
            let status = await sessionController.updateSession(data.id,data.new_name,data.new_date);
            createStatus=status[0];
            message = status[1];
        } catch (err) {
            message=err;
            console.log(err)
        }
        let response = {
            createStatus:createStatus,
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    async function removeSession(req,res){
        let createStatus = 'succeed';
        let message;
        try {
            let data = req.body.session;
            let sessionID = data.id;
            let sessionName = data.no;
            let sessionDate = data.date;
            await sessionController.removeSession(sessionID);
            await attendanceControler.removeAttendance(sessionID);
            message = 'Removed <b>'+ sessionName +' ('+sessionDate+')</b>  successfully'
        } catch (err) {
            createStatus = 'failed'
            message = 'Some thing wrong!!';
            console.log(err);
        }
        let response = {
            createStatus:createStatus,
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    async function updateAttendance(req,res){
        try {
            let data = req.body.myattendance;
            let status = await attendanceControler.updateAttendance(data.attendance_id,data.student_id,data.status);
            // console.log('status:'+status);
            let student_fullname = await studentController.getStudentFullName(data.student_id);
            // console.log('fullname:'+student_fullname)
            let response = {
                createStatus:'succeed',
                message : 'updated '+student_fullname+ '\'s attendance status successfully'
            };
            // console.log(response);
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.json(response);
        } catch (err) {
            console.log(err);
        }
    }
    async function searchByCourse(req,res){
        let data = req.body.term.data;
        let course_id = req.body.term.course_id;
        let result = await studentController.searchByCourse(course_id,data);
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(result);
    }
    async function getStudentInfor(req,res){
        let studentinfor;
        try {
            let studentID = req.body.student_id;
            studentinfor = await studentController.searchByStudentID(studentID);
            let courselistID = studentinfor.course_id;
            if(courselistID.length>0){
                for(let i in courselistID){
                    let course_id = courselistID[i];
                    //get course
                    let course = await courseController.getCourseInfor(course_id);
                    if(course!=''){
                        studentinfor.class.push(course);
                    }
                    //get session
                    let sessionlist = await sessionController.getSessionList(course_id);
                    if(sessionlist!=null){
                        let a = [];
                        // a.push(sessionlist);
                        studentinfor.sessionlist.push(sessionlist);
                    }
                    //get attendance
                    let attendlist = await attendanceControler.getAttendOfOneStudentInOneCourse(course_id,studentID)
                    if(attendlist!=''){
                        let a = [];
                        // a.push(attendlist);
                        studentinfor.attendancelist.push(attendlist);
                    }
                    //get statistic student per class
                    let status = await attendanceControler.attendanceStatisticsOneStudentInOneCourse(course_id,studentID);
                    if(status!=''){
                        studentinfor.statistics.push(status);
                    }
                }
            }
        } catch (err) {
            console.error(err);
        }
        let response = {
            createStatus:'succeed',
            data:studentinfor
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    async function getAttendanceStatistic(req,res){
        let sessionid = req.body.session_id;
        console.log(sessionid);
        let studentlist = req.session.studentlist;

        let total = await attendanceControler.attendanceStatistics(sessionid,studentlist);

        let response = {
            createStatus:'succeed',
            data:total
        };
        console.log(response);
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    async function studentlistDataTable(req,res){
        let course_id=req.query.course_id;
        try {
            let response;
            let col = [{ "data": "no","visible":true,"title":'No.',"orderable": false},{ "data": "id","visible":true,"title":'ID',"orderable": false},{"data":{_:"name",sort:"sortname"},"title":'FullName','className': 'noVis'},{ "data": "birthday","visible":true,"title":'Birthday',"orderable": false}];
            let studentlist = await studentController.getStudentList(course_id);
            let sessionlist = await sessionController.getSessionList(course_id);
            if(studentlist!=''){
                let attendance = [];
                if(sessionlist!= null){
                    col.push({"data": "percent","title":"Attendance","className":"attendance-percent"});
                    let i = 0;
                    for(let se of sessionlist){
                        let formatSessionDate = formatDateFromServer(se.session_date);
                        let display ='<span class="session_name" query-data="'+se.session_id+'">'+se.session_no+' </span><br/><span class="session_date">'+formatSessionDate[0]+'/'+formatSessionDate[1]+'/'+formatSessionDate[2]+' '+formatSessionDate[3]+':'+formatSessionDate[4]+' '+formatSessionDate[5]+'</span>'
                        col.push({"className":"qa-attendance-session","orderable": false,"data": "attendance."+i+".icon","title":display});
                        i++;
                    }
                }
                let no=1;
                for(let st of studentlist){
                    let a = [],student_id = st.student_id;
                    let formatdate = formatDateFromServer(st.birthday)
                    let total=0,countAtttend=0;
                    if(sessionlist!= null){
                        for(let se of sessionlist){
                            let session_id = se.session_id
                            let attendanceInfor = await attendanceControler.getAttendOfOneStudentOneSession(course_id,session_id,student_id);
                            if(attendanceInfor.status==1){
                                countAtttend++
                            }if(attendanceInfor.status==1||attendanceInfor.status==2){
                                total++
                            }
                            a.push(attendanceInfor);                        
                        }
                    }
                    if(total==0){
                        total=1;
                    }
                    let percentAttend = Math.round((countAtttend/total)*100);
                    let status ='success';
                    if(percentAttend<70&&percentAttend>=40){
                        status ='warning';
                    }else if(percentAttend<40){
                        status='danger';
                    }
                    let percent ='<span class="'+status+' qa-attendance-percent">'+percentAttend+' %</span>';
                    let attendance_infor_of_one_student = {
                        no:no,
                        id:st.student_id,
                        name:st.fullname,
                        sortname:st.firstname,
                        birthday:formatdate[0]+'/'+formatdate[1]+'/'+formatdate[2],
                        percent:percent,
                        attendance:a
                    }
                    no++;
                    attendance.push(attendance_infor_of_one_student);
                }
                response = {
                    data:attendance,
                    columns:col
                };
            }else{
                response = {
                    data:[],
                    columns:col
                };
            }
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.json(response);
        } catch (err) {
            console.log(err);
        }
    }
    async function attendanceCheckingDataTable(req,res){
        let i = 1;
        let atChecking=[];
        try {
            let course_id = req.query.course_id;
            let session_id = req.query.session_id;
            let data = await studentController.getSelectedAttendance(course_id,session_id);

            //column
            let col = [{ "data": "no","visible":true,"title":'No.',"orderable": false},{ "data": "id","visible":true,"title":'ID',"orderable": false},{ "data":{_:"name",sort:"sortname"},"title":'FullName','className': 'noVis'},{ "data": "birthday","visible":true,"title":'Birthday',"orderable": false},{"data": "status","visible":true,"title":'Status',"orderable": true,'className': 'noVis'}];
            let se = data[0].oneattendance.session;
            let formatSessionDate = formatDateFromServer(se.session_date);
            let display ='<span class="session_name" query-data="'+se.session_id+'">'+se.session_no+' </span><br /><span class="session_date">'+formatSessionDate[0]+'/'+formatSessionDate[1]+'/'+formatSessionDate[2]+' '+formatSessionDate[3]+':'+formatSessionDate[4]+' '+formatSessionDate[5]+'</span>'
            col.push({"className":"qa-attendance-session noVis","orderable": false,"data": "attendance","title":display});
            // data
            for(let student of data){
                let formatdate = formatDateFromServer(student.birthday);
                let status_attend = '<span class="attend qa-status"></span>'
                let status_absent = '<span class="absent qa-status"></span>'
                let status_notattend = '<span class="notattend qa-status"></span>'
                let st = student.oneattendance.status;
                let status = status_notattend;
                let button = '<span query-data="'+student.student_id+' '+student.oneattendance.attendance_id+'"><button type="button" class="btn btn-success btn-attend"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-danger btn-absent"><i class="fa fa-times" aria-hidden="true"></i></button></span>';
                if(st==1){
                    status = status_attend;
                    button = '<span query-data="'+student.student_id+' '+student.oneattendance.attendance_id+'"><button type="button" class="btn btn-success btn-attend" disabled><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-danger btn-absent"><i class="fa fa-times" aria-hidden="true"></i></button></span>';
                }else if(st==2){
                    status = status_absent;
                    button = '<span query-data="'+student.student_id+' '+student.oneattendance.attendance_id+'"><button type="button" class="btn btn-success btn-attend"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-danger btn-absent" disabled><i class="fa fa-times" aria-hidden="true"></i></button></span>';
                }
                let a = {
                    no:i,
                    id:student.student_id,
                    name:student.fullname,
                    sortname:student.firstname,
                    birthday:formatdate[0]+'/'+formatdate[1]+'/'+formatdate[2],
                    status : status,
                    attendance: button
                }
                atChecking.push(a);
                i++;
            }
            let response = {
                data:atChecking,
                columns:col
            };
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.json(response);
        } catch (err) {
            console.log(err);
        }
       
    }
    async function exportXLSX(req,res){
        try {
            let course_id = req.params.id;
            let dataExcel = [];
            let check = await sessionController.checkSessionExist(course_id);
            let sessionList='';
            let attendanceinfor='';
            if(check==1){
                sessionList = await sessionController.getSessionList(course_id);
                attendanceinfor = await studentController.getAttendanceInfor(course_id);
            }


            /**************Create Header Excel *************************/
            
            let responseArray = await exportxlsxController.createHeader(check,sessionList);
            dataExcel.push(responseArray[0]);
            //  Set column witdh
            const width = {'!cols': responseArray[1]};
            const height = {'!rows':[{wch:100}]}
            
            /*********************finish create header excel************************/

            


            /*********************Create Body Excel*********************************/
                // merge
                // range.push({s: {c: 2, r:i+1 }, e: {c:4, r:i+1}})
            let studentList = await studentController.getStudentList(course_id);
            dataExcel = await exportxlsxController.createBody(check,studentList,attendanceinfor,dataExcel);
            /**********************Finish create body excel*******************************/
            
            
            /**********************Create file excel**************************************/
            let buffer = nodeXlsx.build([{name: "student's attendance tracking", data: dataExcel}],width,height); // Returns a buffer           
            let filename = course_id+'.xlsx'
            res.attachment(filename);
             
            
            /********************Send to client********************************************/ 
            res.send(buffer);
        } catch (err) {
            // res.status(400).json(err);
            console.error(err);
        }
    }
    async function importXLSX(req,res){
        let status;
        try {
        let data = req.body.val.data;
        let course_id=req.body.val.course_id;
        let newStudentIDArray=[];
        let newSessionIDArray=[];
        let resultMessage=[];

        //create student if they dont exist in db
        for(let items in data){
            let student_id = data[items].Id.toUpperCase();
            let result = await importxlsxController.insertStudent(course_id,student_id);
            console.log('test:'+result[0]);
            if(result[0]!=0){
                newStudentIDArray.push(result[0]);
            }
            resultMessage.push(result[1]);
        };
        // console.log('test:'+newStudentIDArray);
        //create attendance student(default status = 0 ) if they dont exist in db
        if(newStudentIDArray.length>0){
            for(let items in newStudentIDArray){
                let count = await attendanceControler.countStudentAttendance(course_id,newStudentIDArray[items]);
                let lastAtID = await attendanceControler.getLastAttendanceId();
                let newAttendance = await attendanceControler.createStudentAttendance(course_id,newStudentIDArray[items],count,lastAtID);
            }
        }
        let studentList = await studentController.getStudentList(course_id);
        //create new session if they dont exist
        let sessionList = data[0].attendance;
        let newSessionData;
        let lastSessionID = await sessionController.getLastSessionId();
        let lastAttendanceID = await attendanceControler.getLastAttendanceId();
        for(let items in sessionList){
            newSessionData = sessionList[items];
            let check = await sessionController.checkSessionOfCourseExist(course_id,newSessionData.session_name.trim())
            if(check==0){//dont existed
                //then create session
                // console.log(newSessionData.session_date);
                let newSessionDay = newSessionData.session_date
                if(newSessionDay.indexOf('\r\r')!=-1){
                    newSessionDay = newSessionDay.replace('\r\r','').trim();
                }
                let a = newSessionDay.split('/')
                let time = newSessionData.session_time.toUpperCase().trim();
                let formatTime = new Date(a[1]+'/'+a[0]+'/'+a[2]+' '+time);
                //create session
                
                let newSession = await sessionController.createSession(course_id,formatTime,lastSessionID);
                //create attendance of session
                let done = await attendanceControler.createAttendance(course_id,studentList,newSession.session_id,lastAttendanceID);
                lastSessionID++;
                lastAttendanceID++;
            }
        };
        for(let items in data){
            let check = await attendanceControler.countStudentAttendance('HC2','T152007');
            console.log('checckkkk:'+check)
            // console.log(data[items]);
            //update session
            let result = await attendanceControler.updateManyAttendance(course_id,data[items]);
            // console.log('re:'+result);
            resultMessage.push(result)
        };
        status='succeed';
        } catch (err) {
           console.error(err);
           status='failed';
        }
        let response = {
        createStatus:status,
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    }
    
    /* '/action/get-session-list' */
    async function updateSessionList(req,res){
        let course_id=req.query.course_id;
        try {
            let sessionlist = await sessionController.getSessionList(course_id);
            let response = {
                data:sessionlist
            };
            console.log(response);
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.json(response);
        } catch (err) {
            console.log(err);
        }
    }
    

























    //backup
    // \/studentlist\/:name(\\w+-\\w+)-:id(\\w+).vlu
    // \/studentlist\/(?<name>\w+.+[^\-])-(?<id>\w+)\.vlu
    // \/studentlist\/(\w.+\S[^\-])+-(\w+).vlu
    // duy \/studentlist\/(\w+-)+HC1.vlu
    // \/studentlist\/:name(\\w.+\\S[^\\-])+-:id(\\w+).vlu
    // router.get('\/studentlist\/:id(\\w+)-:name([\\s\\w-]+).vlu', isLoggedIn,isAuthorize, async function(req,res){
    //     try {        
    //         let user = req.user;
    //         // let course_id = req.query.course_id;
    //         // var [a,b] = req.params;
    //         let course_id =req.params.id;
    //         req.session.course_id=course_id;
    //         //get studentlist
    //         let studentList = await studentController.getStudentList(course_id);
    //         req.session.studentlist=studentList;
    //         //get sessionlist
    //         let sessionList = await sessionController.getSessionList(course_id);
    //         req.session.sessionlist =sessionList;
    //         //get attendance;ost
    //         let attendancelist = await studentController.getAttendanceInfor(course_id);
    //         var mydata = {
    //             user:user,
    //             studentList:studentList,
    //             sessionList:sessionList,
    //             attendancelist:attendancelist,
    //             course_id:course_id,
    //             path:mypath
    //         }
    //         res.render('studentlist',mydata);
        
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    // router.route('/attendance').post(isLoggedIn,getAttendanceSelect)
    //   //remove this function
    //   .get(isLoggedIn, async function(req,res){
    //       let user = req.user;
    //       let course_id = req.session.course_id;
    //       let sessionID =  req.session.attendancesessionid;
    //       let sessionList =  req.session.sessionlist;
    //       let slug= await courseController.getSlug(course_id);
    //       let attendanceinfor = await studentController.getSelectedAttendance(course_id,sessionID);
    //       res.render('attendance',{user:user,attendlist:attendanceinfor,course_id:course_id,sessionlist:sessionList,slug:slug});
    //   });
    // app.post('/action/change-session',async function(req,res){
    //     let data = req.body.session;
    //     let sessionID= data;
    //     let slug = await sessionController.createSlug(sessionID);
    //     let fullurl = req.protocol + '://' + req.get('host');
    //     // console.log(fullurl)
    //     res.redirect(fullurl + '/attendance/attendance-checking-'+slug+'-'+sessionID+'.vlu');
    // });
    // router.get('/studentStatistic.vlu', isLoggedIn, async function(req,res){
    //     let user = req.user;
    //     res.render('studentStatistic',{user:user});
    // });
    // router.post('/action/getStat', isLoggedIn, async function(req,res){
    //     let stid = req.body.data;
    //     //console.log(stid);
    //     let courseName =[];
    //     let attendanceStatistic = [];
    //     let response;
    //     try{
    //         let student = await statisticController.getStudent(stid);
    //     for (let course of student.course_id) {
    //         let courseInfo = await statisticController.getCourse(course);
    //         // push course name to array
    //         courseName.push(courseInfo.course_name + " " + courseInfo.course_info);

    //         let numberOfSession = await statisticController.countSession(course);
    //         let numerOfAttendance = await statisticController.countAttendance(stid,course);
    //         let attendancePercentage = ((numerOfAttendance/numberOfSession)*100);
    //         // push attendance percentage to array
    //         attendanceStatistic.push(attendancePercentage);
            
    //         //console.log(courseInfo.course_name + " " + courseInfo.course_info + " numbers of session: " + numberOfSession + " number of attendance: " + numerOfAttendance);
    //         //console.log(attendancePercentage + " %");
    //     };
        
    //         response = {
    //             CN: courseName,
    //             AP: attendanceStatistic
    //         };
    //     } catch(err){
    //         response = {
    //             CN: 0,
    //             AP: 0
    //         };
    //     }
    //     res.header('Content-type','application/json');
    //     res.header('Charset','utf8');
    //     res.json(response);
    // });
    // getStudentsStatistic = async (id) => {
    //     let courseName =[];
    //     let attendanceStatistic = [];
    //     let student = await statisticController.getStudent(id);
    //     for (let course of student.course_id) {
    //         let courseInfo = await statisticController.getCourse(course);
    //         // push course name to array
    //         courseName.push(courseInfo.course_name + " " + courseInfo.course_info);

    //         let numberOfSession = await statisticController.countSession(course);
    //         let numerOfAttendance = await statisticController.countAttendance(id,course);
    //         let attendancePercentage = ((numerOfAttendance/numberOfSession)*100);
    //         // push attendance percentage to array
    //         attendanceStatistic.push(attendancePercentage);
            
    //         //console.log(courseInfo.course_name + " " + courseInfo.course_info + " numbers of session: " + numberOfSession + " number of attendance: " + numerOfAttendance);
    //         //console.log(attendancePercentage + " %");
    //     };
    //     // console.log(student);
    //     // console.log(courseName);
    //     // console.log(attendanceStatistic);
    // }
    
    // //getStudentsStatistic("T153914");
    










