'use strict';
var mongoose = require('mongoose');
var schema=mongoose.Schema;
var attendanceSchema= new schema({
    attendance_id :{
      type:Number,
      unique:true,
      required:true  
    },
    session_id :{
      type:Number,
      ref:'Session',
      unique:false,
      required:true  
    },
    student_id :{
      type:String,
      ref:'Student',
      unique:false,
      required:true  
    },
    course_id :{
      type:String,
      unique:false,
      required:true  
    },
    status :{
      type:Number,
      unique:false,
      enum:[0,1,2],
      default:0,  
    },note :{
      type:String,
      unique:false,
      required:false  
    },

});
attendanceSchema.virtual('session', {
  ref: 'Session',
  localField: 'session_id', 
  foreignField: 'session_id',
  justOne: true
});
attendanceSchema.set('toObject', { virtuals: true });
attendanceSchema.set('toJSON', { virtuals: true });
module.exports=mongoose.model("Attendance",attendanceSchema);