'use strict';
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var schema=mongoose.Schema;

var lecturerSchema= new schema({
    lecturer_id :{
      type:String,
      unique:true,
      required:true  
    },
    secrect:{
      type:String,
      unique:false,
      required:true  
    },
    local:{
      username:String,
      password:String,
    }

});
lecturerSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
// kiểm tra mật khẩu có trùng khớp
lecturerSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};
module.exports=mongoose.model("Lecturer",lecturerSchema);