'use strict';
var mongoose = require('mongoose');
var schema=mongoose.Schema;

var sessionSchema= new schema({
    session_id :{
      type:Number,
      unique:true,
      required:true  
    },
    session_no :{
      type:String,
      unique:false,
      required:true  
    },
    session_date :{
      type:Date,
      required:true  
    },
    course_id :{
      type:String,
      unique:false,
      required:true  
    },
    temp:{
      type:Number,
      unique:false,
      required:true  
    }
    

});
module.exports=mongoose.model("Session",sessionSchema);