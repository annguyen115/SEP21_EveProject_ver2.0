'use strict';
var mongoose = require('mongoose');
var schema=mongoose.Schema;

var courseSchema= new schema({
    course_id :{
      type:String,
      unique:true,
      required:true  
    },
    course_name :{
      type:String,
      unique:false,
      required:true  
    },
    course_info :{
      type:String,
      unique:false,
      required:false  
    },
    lecturer_id :{
      type:String,
      ref:'Lecturer',
      unique:false,
      required:true  
    },
    slug:{
      type:String,
      unique:false,
      required:true
    }		
});
module.exports = mongoose.model("Course",courseSchema);