var courseController = require('./controllers/courseController');
var studentController = require('./controllers/studentController');
var sessionController = require('./controllers/sessionController');
var attendanceControler = require('./controllers/attendanceController');
var commonController = require('./controllers/commonController');
var exportxlsxController = require('./controllers/exportxlsxController');
var importxlsxController = require('./controllers/importxlsxController');
let statisticController = require('./controllers/statisticController')





let nodeXlsx = require('node-xlsx-style');
let fs = require('fs');
var path = require("path");


module.exports = function(app,passport){
    
     /********************************************************************** My Router**************************************************************/
    /* Login. */
    app.route('/login')
       .get(function(req,res){
            req.logout();
            res.render('login.ejs', { message: req.flash('loginMessage') })
        })
       .post(passport.authenticate('local-login',{
          successRedirect : '/',
          failureRedirect : '/login', 
          failureFlash : true
    }))
    /* Logout. */
    app.get('/logout', function(req, res) {
        req.logout();
        req.session.destroy;
        res.redirect('/login');
    });
    /* Index. */
    app.get('/',isLoggedIn,async function(req, res) {
        let user = req.user;
        courseController.insertCourse(user);
        let listCourse = await courseController.getCourselist(user);
        res.render('index',{user:user,listCourse:listCourse});
    });

    /* Student List. */
    // \/studentlist\/:name(\\w+-\\w+)-:id(\\w+).vlu
    // \/studentlist\/(?<name>\w+.+[^\-])-(?<id>\w+)\.vlu
    // \/studentlist\/(\w.+\S[^\-])+-(\w+).vlu
    // duy \/studentlist\/(\w+-)+HC1.vlu
    // \/studentlist\/:name(\\w.+\\S[^\\-])+-:id(\\w+).vlu
    app.get('\/studentlist\/:id(\\w+)-:name([\\s\\w-]+).vlu', isLoggedIn,isAuthorize, async function(req,res){
        try {        
            let user = req.user;
            // let course_id = req.query.course_id;
            // var [a,b] = req.params;
            let course_id =req.params.id;
            req.session.course_id=course_id;
            //get studentlist
            let studentList = await studentController.getStudentList(course_id);
            req.session.studentlist=studentList;
            //get sessionlist
            let sessionList = await sessionController.getSessionList(course_id);
            req.session.sessionlist =sessionList;
            //get attendance;ost
            let attendancelist = await studentController.getAttendanceInfor(course_id);
            res.render('studentlist',{user:user,studentList:studentList,sessionList:sessionList,attendancelist:attendancelist,course_id:course_id});
        
        } catch (err) {
            console.log(err);
        }
    });

    /* Profile. */
    app.get('/profile.vlu', isLoggedIn, function(req,res){
        let user = req.user;
        res.render('profile',{user:user});
    }); 
    /*insert student from API*/  
    app.post('/action/insert-student', isLoggedIn, async function(req,res){
        let data = req.body.course;
        let courseID = data.id;
        // console.log(data);
        //  insert student into localdb if it doesnt exist////
        let newStudentID = await studentController.insertStudent(courseID);
        let checkSessionExist = await sessionController.getFirstSessionInCourse(courseID);
        if(newStudentID.length>0){
            if(checkSessionExist!=''){
                let sessionlist = await sessionController.getSessionIdList(courseID);
                console.log('selist:'+sessionlist);
                let lastAttendanceID = await attendanceControler.getLastAttendanceId();
                console.log('last:'+lastAttendanceID);
                for(let i in newStudentID){
                    for(let j in sessionlist){
                        let a = await attendanceControler.findAndCreateAttendanceOfOneStudent(courseID,newStudentID[i],sessionlist[j],lastAttendanceID);
                        console.log(a);
                        if(a!=''){
                            console.log('plus');
                            lastAttendanceID++;
                        }
                    }
                }
                
            }
        }
        let name = data.name+'('+data.id+')';
        name = name.toUpperCase();
        let response = {
            createStatus:'succeed',
            message : 'Update studentlist of '+name+' successfully' 
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    });
    /* Create Session. */
    app.post('/action/create-session', isLoggedIn, async function(req,res){
        let data = req.body;
        let course_id = req.session.course_id;
        let studentlist = req.session.studentlist;
        let message = '';
        let createStatus='';
        let sessionNew = data.newSessionDay.format;
        try {
            let lastSessionID = await sessionController.getLastSessionId();
            let newSession = await sessionController.createSession(course_id,sessionNew,lastSessionID);
            let lastAttendanceID = await attendanceControler.getLastAttendanceId();
            let done = await attendanceControler.createAttendance(course_id,studentlist,newSession.session_id,lastAttendanceID);
            createStatus ='succeed';
            message='create ' +newSession.session_no+' ('+data.newSessionDay.name+') '+' successfully';
            let response = {
                createStatus:createStatus,
                message : message
            };
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.json(response);
        } catch (error) {
            console.error(error);
        }
        
        
    });
    /*Remove Session */
    app.post('/action/remove-session', isLoggedIn, async function(req,res){
        let createStatus = 'succeed';
        let message;
        try {
            let data = req.body;
            let sessionInfor = data.session;
            let sessionID = sessionInfor.session_id;
            let sessionDate = data.date;
            let sessionName = sessionInfor.session_no;


            await sessionController.removeSession(sessionID);
            await attendanceControler.removeAttendance(sessionID);
            message = 'Remove '+ sessionName +' ('+sessionDate+')  successfully'
        } catch (err) {
            createStatus = 'failed'
            message = err;
        }
        let response = {
            createStatus:createStatus,
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
        
    });
    /* Create Student. */
    app.post('/action/create-student', isLoggedIn, async function(req,res){
        let data = req.body;
        let course_id = req.session.course_id;
        let message = '';
        let createStatus='';
        try {
            let result = await studentController.createStudent(course_id,data.newStudent.id);
            let status = result[0];
            let studentname=result[1];
            if(status=='existed'){
                createStatus='failed';
                message = studentname+' has already existed in studentlist of ' +course_id;
            }else if(status=='notfound'){
                createStatus='failed'
                message ="'"+data.newStudent.id+"' not found in mr Duy's db ";
            }else if(status=='create successs'){
                createStatus='succeed';
                message = "Create '"+studentname+"' successfully";
                let count = await attendanceControler.countStudentAttendance(course_id,data.newStudent.id);
                let lastAttendanceID = await attendanceControler.getLastAttendanceId();
                let newAttendance = await attendanceControler.createStudentAttendance(course_id,data.newStudent.id,count,lastAttendanceID);
            }      
            let response = {
                createStatus:createStatus,
                message : message
            };
            res.header('Content-type','application/json');
            res.header('Charset','utf8');
            res.json(response);
        } catch (error) {
            console.error(error);
        }
        
    });

    app.post('/action/update-attendance',isLoggedIn, async function(req,res){
        let data = req.body.myattendance;
        let status = await attendanceControler.updateAttendance(data.attendance_id,data.status);
        let response = {
            createStatus:'succeed',
            message : 'update '+data.name.trim()+ "'s attendance status successfully"
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
        
    });
    app.get('/student-statistic.vlu',isLoggedIn,isLoggedIn,async function(req,res){
        let user = req.user;
        // get studentlist from all courses
        let studentlist = await studentController.getStudentListFromAllCourse();
        res.render('filter',{user:user,studentlist:studentlist});
    });
    app.post('/action/sync-student',isLoggedIn,isLoggedIn,async function(req,res){
        let message;
        let status='failed';
        try {
            let courseID = req.body.courseID;
            let user = req.user;
            let stlist = await studentController.getStudentIDArray(courseID);
            if(stlist!=''){
                let sync = await studentController.syncStudent(user,courseID,stlist);
                if(sync.code!=0){
                    message=sync.message;
                }else{
                    status='succeed';
                    message=sync.message;
                }
            }else{
                message='There are no students '+courseID;
            }
        } catch (err) {
            message=err;
        }
        let response = {
            createStatus:status,
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    });
    
    app.post('/action/sync-attendance',isLoggedIn,isLoggedIn,async function(req,res){
        let message;
        let status='failed';
        try {
            let courseID = req.body.courseID;
            let user = req.user;
            let attendance=[];
            let firstSession = await sessionController.getFirstSessionInCourse(courseID);
            if(firstSession!=''){
                let stList = await studentController.getStudentList(courseID);
                if(stList!=''){
                    //chuẩn bị dữ liệu
                    //sessionlist
                    let se = await sessionController.getSessionIdList(courseID);
                    let sessions = await sessionController.getSessionListFormatForSync(courseID);
                    for(let st of stList){
                        let result = await attendanceControler.getAttendanceListFormatForSync(courseID,st.student_id,se)                        
                        let a = {
                            student : st.student_id,
                            checklist : result[0],
                            info : result[1]
                        }
                        attendance.push(a);
                    }
                    //chuẩn bị data xong,tiến hành sync
                    let a = await attendanceControler.syncAttendance(user,courseID,sessions,attendance);
                    status = 'succeed';
                    message = a;
                }else{
                    message='There are no students in '+courseID;    
                }
            }else{
                message='There are no sessions in '+courseID;
            }
        } catch (err) {
            message = err;
        }
        let response = {
            createStatus:status,
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    });
    /* Search Student information*/ 
    app.post('/action/search-by-course',isLoggedIn,async function(req,res){
        let data = req.body.term.data;
        let course_id = req.body.term.course_id;
        let result = await studentController.searchByCourse(course_id,data);
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(result);
    });
    /* Delete Student Form Course*/
    app.post('/action/delete-student',isLoggedIn,async function(req,res){
        let studentID = req.body.data.student_id;
        let course_id = req.body.data.course_id;
        let message;
        console.log(studentID,course_id);
        let result = await studentController.removeCourseID(course_id,studentID);
        if(result[0]=='succeed'){
            message = 'Removed '+result[1]+' from '+course_id+' successfully';
        }else if(result[0]=='failed'){
            message='Cant find '+result[1]+' in '+course_id+' in database';       
        }
        let response = {
            createStatus:result[0],
            message : message
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    });
    // app.post('/action/change-session',async function(req,res){
    //     let data = req.body.session;
    //     let sessionID= data;
    //     let slug = await sessionController.createSlug(sessionID);
    //     let fullurl = req.protocol + '://' + req.get('host');
    //     // console.log(fullurl)
    //     res.redirect(fullurl + '/attendance/attendance-checking-'+slug+'-'+sessionID+'.vlu');
        
    // });
    app.post('/action/get-student-infor',isLoggedIn,async function(req,res){
        let studentinfor;
        try {
            let studentID = req.body.student_id;
            studentinfor = await studentController.searchByStudentID(studentID);
            let courselistID = studentinfor.course_id;
            if(courselistID.length>0){
                for(let i in courselistID){
                    let course_id = courselistID[i];
                    //get course
                    let course = await courseController.getCourseInfor(course_id);
                    if(course!=''){
                        studentinfor.class.push(course);
                    }
                    //get session
                    let sessionlist = await sessionController.getSessionList(course_id);
                    if(sessionlist!=null){
                        let a = [];
                        // a.push(sessionlist);
                        studentinfor.sessionlist.push(sessionlist);
                    }
                    //get attendance
                    let attendlist = await attendanceControler.getAttendOfOneStudentInOneCourse(course_id,studentID)
                    if(attendlist!=''){
                        let a = [];
                        // a.push(attendlist);
                        studentinfor.attendancelist.push(attendlist);
                    }
                    //get statistic student per class
                    let status = await attendanceControler.attendanceStatisticsOneStudentInOneCourse(course_id,studentID);
                    if(status!=''){
                        studentinfor.statistics.push(status);
                    }
                }
            }
        } catch (err) {
            console.error(err);
        }
        let response = {
            createStatus:'succeed',
            data:studentinfor
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    });
    app.post('/action/attendance-statistics',async function(req,res){
        let count0=0,count1=0,count2=0;
        let sessionid = req.body.sessionid;
        let studentlist = req.session.studentlist;

        let total = await attendanceControler.attendanceStatistics(sessionid,studentlist);

        let response = {
            createStatus:'succeed',
            data:total
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    });  

    /* Attendance online */
    app.route('/attendance')
       .post(isLoggedIn, async function(req,res){
        let user = req.user;
        let sessionID = req.body.session_id;
        let course_id = req.body.course_id;
        let slug = await sessionController.createSlug(sessionID);
        let fullurl = req.protocol + '://' + req.get('host');
        // console.log(fullurl)
        res.redirect(fullurl +'/attendance/'+course_id+'-attendance-checking-'+slug+'-'+sessionID+'.vlu');
        })
        //remove this function

        .get(isLoggedIn, async function(req,res){
            let user = req.user;
            let course_id = req.session.course_id;
            let sessionID =  req.session.attendancesessionid;
            let sessionList =  req.session.sessionlist;
            let slug= await courseController.getSlug(course_id);
            let attendanceinfor = await studentController.getSelectedAttendance(course_id,sessionID);
            res.render('attendance',{user:user,attendlist:attendanceinfor,course_id:course_id,sessionlist:sessionList,slug:slug});
        });
    app.get('\/attendance\/:id(\\w+)-attendance-checking-:name([\\s\\w-]+)-:seid(\\w+).vlu',isLoggedIn,async function(req,res){
        let user = req.user;
        let course_id =req.params.id;
        // let course_id = req.session.course_id;
        let sessionID =  req.params.seid;
        let sessionList =  req.session.sessionlist;
        let slug= await courseController.getSlug(course_id);
        let attendanceinfor = await studentController.getSelectedAttendance(course_id,sessionID);
        res.render('attendance',{user:user,attendlist:attendanceinfor,course_id:course_id,sessionlist:sessionList,slug:slug});
    });
    /* Export xlsx file. */
    app.get('/action/export-excel/:id',isLoggedIn,async function(req,res){
        
        try {
            let course_id = req.params.id;
            let dataExcel = [];
            let check = await sessionController.checkSessionExist(course_id);
            let sessionList='';
            let attendanceinfor='';
            if(check==1){
                sessionList = await sessionController.getSessionList(course_id);
                attendanceinfor = await studentController.getAttendanceInfor(course_id);
            }


            /**************Create Header Excel *************************/
            
            let responseArray = await exportxlsxController.createHeader(check,sessionList);
            dataExcel.push(responseArray[0]);
            //  Set column witdh
            const width = {'!cols': responseArray[1]};
            const height = {'!rows':[{wch:100}]}
            
            /*********************finish create header excel************************/

            


            /*********************Create Body Excel*********************************/
                // merge
                // range.push({s: {c: 2, r:i+1 }, e: {c:4, r:i+1}})
            let studentList = await studentController.getStudentList(course_id);
            dataExcel = await exportxlsxController.createBody(check,studentList,attendanceinfor,dataExcel);
            /**********************Finish create body excel*******************************/
            
            
            /**********************Create file excel**************************************/
            let buffer = nodeXlsx.build([{name: "student's attendance tracking", data: dataExcel}],width,height); // Returns a buffer           
            let filename = course_id+'.xlsx'
            res.attachment(filename);
             
            
            /********************Send to client********************************************/ 
            res.send(buffer);
        } catch (err) {
            // res.status(400).json(err);
            console.error(err);
        }
    });
    app.post('/action/import-excel',isLoggedIn,async function(req,res){
       let status;
        try {
        let data = req.body.val.data;
        let course_id=req.body.val.course_id;
        let newStudentIDArray=[];
        let newSessionIDArray=[];
        let resultMessage=[];

        //create student if they dont exist in db
        for(let items in data){
            let student_id = data[items].Id.toUpperCase();
            let result = await importxlsxController.insertStudent(course_id,student_id);
            console.log('test:'+result[0]);
            if(result[0]!=0){
                newStudentIDArray.push(result[0]);
            }
            resultMessage.push(result[1]);
        };
        // console.log('test:'+newStudentIDArray);
        //create attendance student(default status = 0 ) if they dont exist in db
        if(newStudentIDArray.length>0){
            for(let items in newStudentIDArray){
                let count = await attendanceControler.countStudentAttendance(course_id,newStudentIDArray[items]);
                let lastAtID = await attendanceControler.getLastAttendanceId();
                let newAttendance = await attendanceControler.createStudentAttendance(course_id,newStudentIDArray[items],count,lastAtID);
            }
        }
        let studentList = await studentController.getStudentList(course_id);
        //create new session if they dont exist
        let sessionList = data[0].attendance;
        let newSessionData;
        let lastSessionID = await sessionController.getLastSessionId();
        let lastAttendanceID = await attendanceControler.getLastAttendanceId();
        for(let items in sessionList){
            newSessionData = sessionList[items];
            let check = await sessionController.checkSessionOfCourseExist(course_id,newSessionData.session_name.trim())
            if(check==0){//dont existed
                //then create session
                // console.log(newSessionData.session_date);
                let newSessionDay = newSessionData.session_date
                if(newSessionDay.indexOf('\r\r')!=-1){
                    newSessionDay = newSessionDay.replace('\r\r','').trim();
                }
                let a = newSessionDay.split('/')
                let time = newSessionData.session_time.toUpperCase().trim();
                let formatTime = new Date(a[1]+'/'+a[0]+'/'+a[2]+' '+time);
                //create session
                
                let newSession = await sessionController.createSession(course_id,formatTime,lastSessionID);
                //create attendance of session
                let done = await attendanceControler.createAttendance(course_id,studentList,newSession.session_id,lastAttendanceID);
                lastSessionID++;
                lastAttendanceID++;
            }
        };
        
        for(let items in data){
            // console.log(data[items]);
            //update session
            let result = await attendanceControler.updateManyAttendance(course_id,data[items]);
            // console.log('re:'+result);
            resultMessage.push(result)
        };
        status='succeed';
        } catch (err) {
           console.error(err);
           status='failed';
        }
        let response = {
        createStatus:status,
        };
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    });
    app.get('/test', async function(req,res){
        let course_id = "HC1";
        let data="T152007"
        try {
            let testData = await commonController.test();
            testData  = JSON.stringify(testData,null,' ');
            fs.writeFile(path.join(__dirname,'/../',"/log/test"),testData, function(err){
                if(err) {
                    console.log(err);
                }else{
                    console.log("The file was saved!");
                }    
            })
            
        } catch (err) {
            console.error(err);
        }
        res.end();
    }); 
    app.get('/b', function(req,res){
        let currentday = new Date();
        let day = currentday.getDate();
        let month = currentday.getMonth();
        let  year = currentday.getFullYear();
        let hour = currentday.getHours();
        let minutes = currentday.getMinutes();
        let amPM = (hour > 11) ? "PM" : "AM";
        if(hour > 12) {
          hour -= 12;
        } else if(hour == 0) {
          hour = "12";
        }
        if(minutes < 10) {
            minutes = "0" + minutes;
        }
        console.log(month+'/'+day+'/'+year+' '+hour+':'+minutes+' '+amPM);
        var date = new Date(month+'/'+day+'/'+year+' '+hour+':'+minutes+' '+amPM);
        var test = date.getMinutes();
        console.log('date:'+date)
        console.log('test:'+test); 
    });
    app.get('/testview',isLoggedIn, function(req,res){
         let user = req.user;
         res.render('testview',{user:user});
    });
    

    /* Student Statistic */
    app.get('/studentStatistic.vlu', isLoggedIn, async function(req,res){
        let user = req.user;
        res.render('studentStatistic',{user:user});
    }); 

    app.post('/action/getStat', isLoggedIn, async function(req,res){
        let stid = req.body.data;
        //console.log(stid);
        let courseName =[];
        let attendanceStatistic = [];
        let response;
        try{
            let student = await statisticController.getStudent(stid);
        for (let course of student.course_id) {
            let courseInfo = await statisticController.getCourse(course);
            // push course name to array
            courseName.push(courseInfo.course_name + " " + courseInfo.course_info);

            let numberOfSession = await statisticController.countSession(course);
            let numerOfAttendance = await statisticController.countAttendance(stid,course);
            let attendancePercentage = ((numerOfAttendance/numberOfSession)*100);
            // push attendance percentage to array
            attendanceStatistic.push(attendancePercentage);
            
            //console.log(courseInfo.course_name + " " + courseInfo.course_info + " numbers of session: " + numberOfSession + " number of attendance: " + numerOfAttendance);
            //console.log(attendancePercentage + " %");
        };
        
            response = {
                CN: courseName,
                AP: attendanceStatistic
            };
        } catch(err){
            response = {
                CN: 0,
                AP: 0
            };
        }
        res.header('Content-type','application/json');
        res.header('Charset','utf8');
        res.json(response);
    });


    /********************************************************************** My Function**************************************************************/
    /* Get Course List*/
    /* Params: user as object for storing user infor */
    // let getCourselist = (req,res,user)=>{
    //     return new Promise((resolve,reject)=>{
    //         var courseList = userController.getCourselist=(req,res,user);
    //         return resolve(courseList);
    //     });
    // }
    /* Check login*/
    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/login');
    }
    async function isAuthorize(req,res,next){
        let user_id = req.user.lecturer_id;
        let course_id = req.params.id;
        let coursecheck = await courseController.checkExistAndAuthorize(user_id,course_id);
        console.log(coursecheck);
        if(coursecheck=='notexist'){
            return res.status(404).render('404');
        }else if (coursecheck=='notmatch'){
            return res.status(403).render('403');
        }else{
            return next();
        }
    }

    // getStudentsStatistic = async (id) => {
    //     let courseName =[];
    //     let attendanceStatistic = [];
    //     let student = await statisticController.getStudent(id);
    //     for (let course of student.course_id) {
    //         let courseInfo = await statisticController.getCourse(course);
    //         // push course name to array
    //         courseName.push(courseInfo.course_name + " " + courseInfo.course_info);

    //         let numberOfSession = await statisticController.countSession(course);
    //         let numerOfAttendance = await statisticController.countAttendance(id,course);
    //         let attendancePercentage = ((numerOfAttendance/numberOfSession)*100);
    //         // push attendance percentage to array
    //         attendanceStatistic.push(attendancePercentage);
            
    //         //console.log(courseInfo.course_name + " " + courseInfo.course_info + " numbers of session: " + numberOfSession + " number of attendance: " + numerOfAttendance);
    //         //console.log(attendancePercentage + " %");
    //     };
    //     // console.log(student);
    //     // console.log(courseName);
    //     // console.log(attendanceStatistic);
    // }
    
    // //getStudentsStatistic("T153914");
};











