'use strict';
//merge
// let range = [];
// range.push({s: {c: 2, r:0 }, e: {c:4, r:0}}); // C1:E1
// const option = {'!merges': range};
exports.createHeader = function(check,sessionList){
    return new Promise((resolve,reject)=>{
            let arrHeaderTitle = [];
            let col = [ {wch:6},{wch:9},{wch:25},{wch:12}];           
            arrHeaderTitle.push({v:'No',s:headerStyle},{v:'Id',s:headerStyle},{v:'FullName',s:headerStyle},{v:'Birthday',s:headerStyle});
            //check session exist,if exist then execute getsessionList
            console.log(check);
            if(check==1){
                sessionList.forEach(e => {
                    let name = e.session_no;
                    let date = new Date(e.session_date);
                    let day = date.getDate();
                    let month = date.getMonth()+1;
                    let year = date.getFullYear();
                    let hour = date.getHours();
                    let minutes = date.getMinutes();
                    let amPM = (hour > 11) ? "PM" : "AM";
                    if(hour > 12) {
                    hour -= 12;
                    } else if(hour == 0) {
                    hour = "12";
                    }
                    if(minutes < 10) {
                        minutes = "0" + minutes;
                    }
                    let time = hour+':'+minutes+' '+amPM;
                    let finaltime = name+'\r\n'+day+'/'+month+'/'+year+'\r\n'+time;
                    col.push({wch:12});
                    arrHeaderTitle.push({v:finaltime,s:headerStyle});
                });
            }
            let a =[];
            a.push(arrHeaderTitle);
            a.push(col);
            return resolve(a);
    });
}
exports.createBody = function(check,studentList,attendanceinfor,dataExcel){
    return new Promise((resolve,reject)=>{
        var i = 0;
        let style;
        studentList.forEach(e => {
            if((i+1)%2==0){
                style=studentEvenStyle;
                
            }else{
                style=studentOddStyle;
            }
            let rowItemValue = [];
            // style2.fill.fgColor.rgb = color;
            rowItemValue.push({v:i+1,s:style});
            rowItemValue.push({v:e.student_id,s:style});
            rowItemValue.push({v:e.fullname,s:style});
            let date = new Date(e.birthday);
            let day = date.getDate();
            let month = date.getMonth()+1;
            let year = date.getFullYear();
            rowItemValue.push({v:day+"/"+month+"/"+year,s:style});
            if(check==1){
                attendanceinfor[i].attendance.forEach(a => {
                    if((i+1)%2==0){    
                        if(a.status==0){
                            rowItemValue.push({v:' ',s:notAttendEvenStyle}); 
                        }else if(a.status==1){
                            rowItemValue.push({v:'v',s:attendEvenStyle});
                        }else if(a.status==2){
                            rowItemValue.push({v:'x',s:absentEvenStyle});
                        }
                    }else{
                        if(a.status==0){
                            rowItemValue.push({v:' ',s:notAttendOddStyle}); 
                        }else if(a.status==1){
                            rowItemValue.push({v:'v',s:attendOddStyle});
                        }else if(a.status==2){
                            rowItemValue.push({v:'x',s:absentOddStyle});
                        }
                    }
                    
                });
            }
            dataExcel.push(rowItemValue);
            i++;
        });
        return resolve(dataExcel);
    });
}











/**************** Excel Style *************************/
const borderStyle = {
    bottom:{style:'thin',color:{ rgb: "9bbdf7" }},
    top:{style:'thin',color:{ rgb: "9bbdf7" }},
    left:{style:'thin',color:{ rgb: "9bbdf7" }},
    right:{style:'thin',color:{ rgb: "9bbdf7" }}
};

const headerStyle = {
    font:{bold:true,sz:14,color:{ rgb: "ffffff" }},
    alignment: {horizontal: 'center',vertical:'center'},
    fill: {patternType: 'solid',fgColor: {rgb: '2674f2'}},
    border:borderStyle
};
const studentOddStyle = {border:borderStyle,fill: {patternType: 'solid',fgColor: {rgb: 'ffffff'}}};
const studentEvenStyle = {border:borderStyle,fill: {patternType: 'solid',fgColor: {rgb: 'ccdfff'}}};
const notAttendOddStyle = {
    font:{bold:true,sz:20,color:{ rgb: "000000" }},
    alignment: {horizontal: 'center'},
    border:borderStyle,
    fill: {patternType: 'solid',fgColor: {rgb: 'ffffff'}}
};
const attendOddStyle = {
    font:{bold:true,sz:20,color:{ rgb: "42f456" }},
    alignment: {horizontal: 'center'},
    border:borderStyle,
    fill: {patternType: 'solid',fgColor: {rgb: 'ffffff'}}
};
const absentOddStyle = {
    font:{bold:true,sz:20,color:{ rgb: "d60202" }},
    alignment: {horizontal: 'center'},
    fill: {patternType: 'solid',fgColor: {rgb: 'ffffff'}},
    border:borderStyle
};
const notAttendEvenStyle = {
    font:{bold:true,sz:20,color:{ rgb: "000000" }},
    alignment: {horizontal: 'center'},
    border:borderStyle,
    fill: {patternType: 'solid',fgColor: {rgb: 'ccdfff'}}
};
const attendEvenStyle = {
    font:{bold:true,sz:20,color:{ rgb: "42f456" }},
    alignment: {horizontal: 'center'},
    border:borderStyle,
    fill: {patternType: 'solid',fgColor: {rgb: 'ccdfff'}}
};
const absentEvenStyle = {
    font:{bold:true,sz:20,color:{ rgb: "d60202" }},
    alignment: {horizontal: 'center'},
    fill: {patternType: 'solid',fgColor: {rgb: 'ccdfff'}},
    border:borderStyle
};

/*****************************************************************************/