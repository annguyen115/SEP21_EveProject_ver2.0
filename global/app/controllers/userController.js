var https = require('https');
exports.getSelectedAttendance = function (username,password) {
    return new Promise((resolve,reject)=>{
        var url = 'https://entool.azurewebsites.net/SEP21/Login?Username=' + username + '&Password=' + password;
        var request = https.get(url,function(response){
                var json='';
                response.on('data',d=>{
                    json+=d;
                });
                response.on('end',()=>{
                    let data_response = JSON.parse(json);
                    if(data_response.code == 1){
                        return resolve('false');
                    }
                    return resolve('true');   
                });
            });  
    })
}