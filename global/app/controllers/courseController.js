    'use strict';


    var mongoose = require('mongoose');
    var https = require('https');

    // var lecturer = mongoose.model('Lecturer');
    var Course = mongoose.model('Course');

    /* Get courselist. */
    exports.getCourselist = function(user){
        return new Promise((resolve,reject)=>{
            var user_id = user.lecturer_id;
            var url = 'https://entool.azurewebsites.net/SEP21/GetCourses?lecturerID='+user_id;
            var request = https.get(url,(response)=>{
                var json='';
                response.on('data',d=>{
                    json+=d;        
                });
                response.on('end',()=>{
                    try {
                        let data_response = JSON.parse(json);
                        let listCourse = data_response.data
                        return resolve(listCourse);   
                   } catch (error) {
                       console.error(error);
                   }
                })

            });
            request.on('error',err=>{
                console.log(err);
            })
          });
    }
    /* Insert course into localdb if it doesnt exist. */
    exports.insertCourse = function(user){
        return new Promise((resolve,reject)=>{
            var user_id = user.lecturer_id;
            var url = 'https://entool.azurewebsites.net/SEP21/GetCourses?lecturerID='+user_id;
            var request = https.get(url,(response)=>{
                var json='';
                response.on('data',d=>{
                    json+=d; 
                })
                response.on('end',()=>{
                    var data_response = JSON.parse(json);
                    data_response.data.forEach(d => {
                        Course.find({course_id:d.id},function(err,course){
                            if(course.length==0){
                                //create newCourse
                                let slug = d.name.toLowerCase().replace(' ','-');
                                var newCourse = new Course({
                                    course_id : d.id,
                                    course_name : d.name,
                                    course_info : d.info,
                                    lecturer_id : user_id,
                                    slug:slug
                                });
                                //save newCourse
                                newCourse.save(err=>{
                                    if(err) return reject(err);
                                    else{
                                        console.log('Insert '+d.name+' successfully');
                                        return resolve('done');
                                    }
                                });
                            }
                            return resolve('done');
                        });
                    });
                });
            });
        });
    };
    /**Check Authorize of user to access this course*/
    /**param: userid as userID,id as course_id */
    exports.checkExistAndAuthorize = function(userid,id){
        return new Promise((resolve,reject)=>{
            let name="";
            let result=[];
            Course.findOne({course_id:id}).lean().exec((err,c)=>{
                if(c){
                    if(c.lecturer_id==userid){
                        return resolve('match');
                    }else{
                        return resolve('notmatch');
                    }
                }else{
                    return resolve('notexist');
                }
            });
        });
    };
    /**get Slug*/
    exports.getSlug = function(id){
        return new Promise((resolve,reject)=>{
            Course.findOne({course_id:id}).lean().exec((err,c)=>{
                if(c){
                    return resolve(c.slug);
                }
            });
        });
    }
    exports.getCourseInfor= function(course_id){
        return new Promise((resolve,reject)=>{
            Course.findOne({course_id:course_id}).lean().exec((err,c)=>{
                if(err) return reject(err);
                if(c){
                    return resolve(c.course_name);
                }else{
                    return resolve('');
                }
            })
        })
    }
