'use strict';
var mongoose = require('mongoose');
var schema=mongoose.Schema;

var studentSchema= new schema({
    student_id :{
      type:String,
      unique:true,
      required:true  
    },
    firstname :{
      type:String,
      unique:false,
      required:true  
    },
    lastname :{
      type:String,
      unique:false,
      required:true  
    },
    fullname :{
      type:String,
      unique:false,
      required:true  
    },
    birthday :{
      type:Date,
      unique:false,
      required:true  
    },
    course_id:[{
      type:String,
      ref:'Course',
      unique:false,
      required:true
    }]

});
studentSchema.virtual('attendance', {
  ref: 'Attendance',
  localField: 'student_id', 
  foreignField: 'student_id',
  justOne: false
});
studentSchema.virtual('oneattendance', {
  ref: 'Attendance',
  localField: 'student_id', 
  foreignField: 'student_id',
  justOne: true
});
studentSchema.set('toObject', { virtuals: true });
studentSchema.set('toJSON', { virtuals: true });
module.exports=mongoose.model("Student",studentSchema);