$(document).ready(function(){
    $('.qa-student-update').on('click',function(){
      let course_id = $(this).attr('course_id');
      let course_name = $(this).attr('course_name');
      console.log(course_name);
      let name = course_name+'('+course_id+')'+'  ............';
      name = name.toUpperCase();
      $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
      let message = 'Updating studentlist of '+name;
      $('#qa-infor-alert span').html(message);
      $('#qa-infor-alert').show('drop');
      let course = {
        id:course_id,
        name:course_name
      }
      $.ajax({ 
        type:'POST',
        data:{course},
        url:'/action/insert-student',
        dataType:'json'
      }).done(function(response){
        console.log('response:'+JSON.stringify(response));
        var status = response.createStatus;
        console.log('status:'+status)
        if(status=="succeed"){
          setTimeout(()=>{
            $('#qa-infor-alert').hide('drop');
          },1000);
          setTimeout(()=>{
            $('#qa-infor-alert .qa-message').html('<i class="fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff"></i>'+'<span style="margin-left:10px;"> '+response.message+'</span>');              
            $('#qa-infor-alert').show('drop');
          },2000);
          setTimeout(()=>{
            $('#qa-infor-alert').hide('drop');
          },5000);  
        }
      })
    })
  });