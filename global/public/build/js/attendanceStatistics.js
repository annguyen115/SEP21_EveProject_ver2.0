$(document).ready(function(){
    $('#qa-statistics').on('click',function(){
        let sessionid = $('#qa-session-id').html();
        $.ajax({ 
          type:'POST',
          data:{sessionid},
          url:'/action/attendance-statistics',
          dataType:'json'
        }).done(function(response){
          let status = response.createStatus;
          console.log(response.data);
          let notattend = response.data.notattend;
          let attend = response.data.attend;
          let absent = response.data.absent
          if(status == "succeed"){
            function getRandomColor() {
              var letters = '0123456789ABCDEF'.split('');
              var color = '#';
              for (var i = 0; i < 6; i++ ) {
                  color += letters[Math.floor(Math.random() * 16)];
              }
              return color;
            }  
            function drawChart() {
              var a  = [{name:'notattend',value:notattend},{name:'attend',value:attend},{name:'absent',value:absent}];
              a.sort(function(a, b){return parseFloat(b.value)-parseFloat(a.value)});
              var data = google.visualization.arrayToDataTable([
              ['Status', 'Quantity'],
              [a[0].name, 0],
              [a[1].name,0],
              [a[2].name, 0]
              ]);
              var chartColor = []
              for (i=0;i<3;i++){
                var colorZ = getRandomColor();
                chartColor.push(colorZ);
              }
              var options = {
                width:500,
                height:500,
                chartArea: {width: 500, height: 500},
                colors: chartColor,
                animation: {
                  duration: 1000,
                  easing: 'in',
                  startup: true
                }
              };
              var chart = new google.visualization.PieChart(document.getElementById('piechart'));
              chart.draw(data, options);
              var percent = 0;
              var total = notattend+attend+absent;
              var b = Math.round((a[0].value/total)*100);
              var c = Math.round((a[2].value/total)*100);
              // start the animation loop
              var handler = setInterval(function(){
                  // values increment
                  percent += 1;
                  // apply new values
                  data.setValue(0, 1, percent);
                  data.setValue(1, 1, 100 - (percent+c));
                  data.setValue(2, 1, c);
                  console.log(c);
                  // update the pie
                  chart.draw(data, options);
                  // check if we have reached the desired value
                  if (percent > b-1){
                      // stop the loop
                      data.setValue(0, 1,a[0].value);
                      data.setValue(1, 1,a[1].value);
                      data.setValue(2, 1,a[2].value);
                      chart.draw(data, options);                      
                      clearInterval(handler);
                    }
                  }, 30);
              }
             
              google.charts.load('current', {
                'packages': ['corechart']
              });
              google.charts.setOnLoadCallback(drawChart);
              $('#statistics-modal').modal('show');
            }
        })
      })
})