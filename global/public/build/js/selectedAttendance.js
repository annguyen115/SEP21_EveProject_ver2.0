$(document).ready(function(){
    $('#addtendance-modal').on('click',function(){
        if($('#error-message').length >0){
            $('#error-message').remove();
        }
        $('#qa-attend-select').val(-1);
        $('#attendance').modal('toggle');
    });
    $('#qa-attend-select').change(function() {
        let value = $('#qa-attend-select').val();
        $('#error-message').remove();
        if(value=="addnew"){
          $('[data-toggle="dropdown"]').parent().removeClass('open');
          let currentday = new Date();
          let day = currentday.getDate();
          let month = currentday.getMonth()+1;
          let  year = currentday.getFullYear();
          let hour = currentday.getHours();
          let minutes = currentday.getMinutes();
          let amPM = (hour > 11) ? "PM" : "AM";
          if(hour > 12) {
            hour -= 12;
          } else if(hour == 0) {
            hour = "12";
          }
          if(minutes < 10) {
              minutes = "0" + minutes;
          }
          let dateformat = day+'/'+month+'/'+year+' '+hour+':'+minutes+' '+amPM; 
          $('#sessionpicker input').val(dateformat);
          $('#attendance').modal('hide');
          setTimeout(()=>{
            $('#addsession').modal('show');
          },500);       
        }
      });
      $('#attend-submit').on('click',function(){
        let value = $('#qa-attend-select').val();
        if(value==null){
            if($('#error-message').length==0){
                $('<span style="color:red" id="error-message">This field is required.<span>' ).insertBefore("#qa-attend");
            }
        }else{
          $('#attendance').modal('hide');
          $('#attend-form').submit();
        }
        
      })
});