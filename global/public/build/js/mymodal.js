function createBasicModal(id){   
    var html = '<div id="'+id+'" class="modal fade" role="dialog" aria-labelledby="..." tabindex="-1" aria-hidden="true">';
        html+='<div class="modal-dialog modal-sm modal-dialog-centered" role="document">'
        html+='<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"></h4></div>'
        html+='<div class="modal-body"></div>'
        html+='<div class="modal-footer"></div>'
        html+= '</div></div></div></div>'
        $('#qa-modal-hq').append(html);
}
function removeAllModal(){
    $('#qa-modal-hq').empty();
}
// EditOrRemoveSessionModal
function createEditRemoveModal(id,selist){
    createBasicModal(id);
    //title
    $('#'+id+' .modal-title').text('Edit or Remove Session')
    //body      
    $('#'+id+' .modal-body').append('<div class="form-group" id="qa-edit-session"></div>');
    $('#qa-edit-session').append('<select class="form-control" id="qa-edit-select"></select>');
    if(selist!=undefined){ 
      $('#qa-edit-select').append('<option selected disabled value=-1>Choose...</option>');        
      for(i=0;i<selist.length;i++){
          let timeFormat = formatDateFromServer(selist[i].session_date);
          $('#qa-edit-select').append('<option value="'+selist[i].session_id+'">'+selist[i].session_no+'  |  '+timeFormat[0]+'/'+timeFormat[1]+'/'+timeFormat[2]+'</option>');
      }
    }else{
      $('#qa-edit-select').append('<option selected disabled value=-1>Cant find any session to be deleted</option>');
    }
    //footer
    $('#'+id+' .modal-footer').append('<button type="button" class="btn btn-success" id="session-edit"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>');
    $('#'+id+' .modal-footer').append('<button type="button" class="btn btn-danger" id="session-remove"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>')
}