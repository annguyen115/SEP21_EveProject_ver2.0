function formatDateFromServer(notformat){
    let a = [];
    let date = new Date(notformat);
    a[0] = date.getDate();
    a[1] = date.getMonth()+1;
    a[2] = date.getFullYear();
    a[3] = date.getHours();
    a[4] = date.getMinutes();
    a[5] = (a[3] > 11) ? "PM" : "AM";
    if(a[3] > 12) {
      a[3] -= 12;
    } else if(a[3] == 0) {
      a[3] = "12";
    }
    if(a[4] < 10) {
        a[4] = "0" + a[4];
    }
    return a;
  }