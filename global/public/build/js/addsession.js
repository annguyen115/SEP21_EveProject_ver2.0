$(function(){
  $('#session-modal').on('click',function(){
    if($('#error-message').length >0){
          $('#error-message').remove();
        }
    let currentday = new Date();
    let day = currentday.getDate();
    let month = currentday.getMonth()+1;
    let  year = currentday.getFullYear();
    let hour = currentday.getHours();
    let minutes = currentday.getMinutes();
    let amPM = (hour > 11) ? "PM" : "AM";
    if(hour > 12) {
      hour -= 12;
    } else if(hour == 0) {
      hour = "12";
    }
    if(minutes < 10) {
        minutes = "0" + minutes;
    }
    let dateformat = day+'/'+month+'/'+year+' '+hour+':'+minutes+' '+amPM; 
    $('#sessionpicker input').val(dateformat);
    $('#addsession').modal('toggle');
  });
  $('#create-session').on('click',function(){
      let session_date = $('#sessionpicker input').val();
      let a = session_date.split(" ");
      let datemonthyear =a[0];
      let time = a[1]+' '+a[2];
      let b = datemonthyear.split("/")
      let day = b[0];
      let month = b[1];
      let  year = b[2];
      let newSession  = new Date(month+'/'+day+'/'+year+' '+time);
      let newSessionDay ={
        name : day+'/'+month+'/'+year+' '+time,
        format: newSession,
      } 
      $('#addsession').modal('hide');
      $.ajax({ 
        type:'POST',
        data:{newSessionDay},
        url:'/action/create-session',
        dataType:'json'
      }).done(function(response){
        console.log('response:'+JSON.stringify(response));
        var status = response.createStatus;
        console.log('status:'+status)
        if(status=="succeed"){
          $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
          $('#qa-success-alert').show('drop',function(){
            $('#qa-success-alert .qa-message').show();
          });
          setTimeout(()=>{
            $('#qa-success-alert .qa-message').hide();
            $('#qa-success-alert').hide('drop'); 
          },2500); 
            setTimeout(()=>{
              location.reload(); 
          },3500);  
        }else if(status=="failed"){
          $('#qa-error-alert .qa-message').html('<strong>Error! </strong>'+response.message);
          $('#qa-error-alert').show('drop',function(){
            $('#qa-error-alert .qa-message').show();
          });
          setTimeout(()=>{
            $('#qa-error-alert .qa-message').hide();
            $('#qa-error-alert').hide('drop'); 
          },2500); 
        }
      })


        
  });
})