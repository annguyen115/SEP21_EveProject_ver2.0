// Prevent XSS attacks?? Not sure
// https://stackoverflow.com/questions/16098397/pass-variables-to-javascript-in-expressjs
function htmlDecode(input){
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
  }