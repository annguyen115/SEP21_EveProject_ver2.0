function importAjax(data,filename){
    console.log(data);
    let val = {
      data:data,
      course_id:mycourseid
    }
    $.ajax({ 
      type:'POST',
      data:{val},
      url:mypath+'/action/import-excel',
      dataType:'json'
    }).done(function(response){
        console.log('response:'+JSON.stringify(response));
        var status = response.createStatus;
        setTimeout(()=>{
            $('#qa-infor-alert').hide('drop');     
        },2000); 
        if(status=="succeed"){
          $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i><span>Your file('+filename+') processed successfully</span>');
          setTimeout(()=>{
            $('#qa-success-alert').show('drop',function(){
              $('#qa-success-alert .qa-message').show();
            });     
          },2500);           
          setTimeout(()=>{
            $('#qa-success-alert .qa-message').hide();
            $('#qa-success-alert').hide('drop'); 
          },4500); 
            setTimeout(()=>{
              // location.reload(); 
          },5500);
        }else if(status=="failed"){
            $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i><span>Your file('+filename+') processed failed</span>');
            setTimeout(()=>{
              $('#qa-error-alert').show('drop',function(){
                $('#qa-error-alert .qa-message').show();
              });     
            },2500);
            setTimeout(()=>{
              $('#qa-error-alert .qa-message').hide();
              $('#qa-error-alert').hide('drop'); 
            },4500); 
        }
    });
  };
  $(document).ready(function(){
      $('#files').on('change',function(event){
        let filename = event.target.files[0].name;
        let a = filename.split('.');
        let extenstion = a[1].toLowerCase();
        console.log(extenstion);
        let message;
        if(extenstion!='xlsx'){
          message='<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;color:#fff"> We only support .xlsx file </span>'
          $('#qa-error-alert .qa-message').html(message);
          $('#qa-error-alert').show('drop',function(){
            $('#qa-error-alert .qa-message').show();
          });
          setTimeout(()=>{
            $('#qa-error-alert .qa-message').hide();
            $('#qa-error-alert').hide('drop'); 
          },3000);  
        }else{
          $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
          message = 'Your file ('+filename+') is in processing, Please wait....';
          $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
          $('#qa-infor-alert span').html(message);
          $('#qa-infor-alert').show('drop',function(){
            $('#qa-infor-alert .qa-message').show();
          });
          /* set up XMLHttpRequest */
          var tmppath = URL.createObjectURL(event.target.files[0]);
          var oReq = new XMLHttpRequest();
          oReq.open("GET", tmppath, true);
          oReq.responseType = "arraybuffer";
          oReq.onload = function(e) {
              var arraybuffer = oReq.response;

              /* convert data to binary string */
              var data = new Uint8Array(arraybuffer);
              var arr = new Array();
              for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
              var bstr = arr.join("");

              /* Call XLSX */
              var workbook = XLSX.read(bstr, {type:"binary"});

              /* DO SOMETHING WITH workbook HERE */
              var first_sheet_name = workbook.SheetNames[0];
              /* Get worksheet */
              var worksheet = workbook.Sheets[first_sheet_name];
              var dataresult = XLSX.utils.sheet_to_json(worksheet,{raw:true,defval:null});
              console.log('notformat:'+dataresult)
              for(var i= 0; i< dataresult.length;i++){
                let d =dataresult[i];
                let attendance = [];
                for(var key in d){                  
                  if(key.toLowerCase()!='no'&&key.toLowerCase()!='id'&&key.toLowerCase()!='fullname'&&key.toLowerCase()!='birthday'){
                    let keySplit = key.split('\n'); 
                    console.log('split:'+keySplit[1]);
                    let a = {
                        session_name:keySplit[0],
                        session_date:keySplit[1],
                        session_time:keySplit[2],
                        status:d[key],
                    }
                    attendance.push(a);
                    delete d[key];
                  }
                }
                // console.log('at:'+attendance);
                d['attendance'] = attendance;
                  
              }
              importAjax(dataresult,filename); 
            }
          oReq.send();
        }
      })
    });