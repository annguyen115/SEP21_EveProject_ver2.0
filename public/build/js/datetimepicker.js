//datepicker
function dateTimePicker(){
  var option = {
    format: 'D/M/YYYY h:mm A',
    ignoreReadonly: true,
    allowInputToggle: true,
    calendarWeeks:true,
    showClose:true,
    showClear:true,
    useCurrent:false,
    showTodayButton:true,
    keepOpen:true,
    toolbarPlacement:'top',
    widgetPositioning: {
      horizontal: 'left',
      vertical: 'bottom'
    },
  }
  $('#sessionpicker input').datetimepicker(option).on('dp.change', function(e){
    if($(this).val()!=''){
      if( !e.oldDate || !e.date.isSame(e.oldDate, 'day')){
        $(this).data('DateTimePicker').hide();
      }
    }
  });
  $('#edit-newsessiondate input').datetimepicker(option).on('dp.change', function(e){
    if($(this).val()!=''){
      if( !e.oldDate || !e.date.isSame(e.oldDate, 'day')){
        $(this).data('DateTimePicker').hide();
      }
    }
  });
  // $('#sessionpicker input').on('dp.hide',function(){
  //   $('#sessionpicker input').datetimepicker('show')
  // })
}  
//modal session
// $('#sessionpicker').on('change paste keyup',function(){
//   let datenotformat = $(this).val();
//   let a = datenotformat.split('/');
//   let date =Date.parse(a[1]+'/'+a[0]+'/'+a[2]);
//   let currentdate = new Date();
//   let b = currentdate.getDate();
//   let c = currentdate.getMonth()+1;
//   let d = currentdate.getFullYear();
//   let currentdateformat = new Date(c+'/'+b+'/'+d);
//   console.log('date-input:'+date);
//   console.log('date-now:'+currentdateformat);
//     $('#error-message').remove();  
//     if($('#sessionpicker').val()==''){
//       $( '<span style="color:red" id="error-message">This field is required.<span>' ).insertBefore("#sessionpicker");
//     }else{
//       if (date < currentdateformat && date!=null){ 
//         $('<span style="color:red" id="error-message">Please select another date<span>' ).insertBefore("#sessionpicker");
//       }  
//     }
//   });