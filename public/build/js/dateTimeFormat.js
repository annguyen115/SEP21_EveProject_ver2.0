function formatDateToServer(notformat){
  let a = notformat.split(" ");
  let datemonthyear =a[0];
  let time = a[1]+' '+a[2];
  let b = datemonthyear.split("/")
  let day = b[0];
  let month = b[1];
  let  year = b[2];
  return month+'/'+day+'/'+year+' '+time;
}
function formatDateFromServer(notformat){
    let a = [];
    let date = new Date(notformat);
    a[0] = date.getDate();
    a[1] = date.getMonth()+1;
    a[2] = date.getFullYear();
    a[3] = date.getHours();
    a[4] = date.getMinutes();
    a[5] = (a[3] > 11) ? "PM" : "AM";
    if(a[3] > 12) {
      a[3] -= 12;
    } else if(a[3] == 0) {
      a[3] = "12";
    }
    if(a[4] < 10) {
        a[4] = "0" + a[4];
    }
    return a;
  }
  function getCurrentDateTime(){
    // let currentday = new Date();
    // let day = currentday.getDate();
    // let month = currentday.getMonth()+1;
    // let  year = currentday.getFullYear();
    // let hour = currentday.getHours();
    // let minutes = currentday.getMinutes();
    // let amPM = (hour > 11) ? "PM" : "AM";
    // if(hour > 12) {
    //   hour -= 12;
    // } else if(hour == 0) {
    //   hour = "12";
    // }
    // if(minutes < 10) {
    //     minutes = "0" + minutes;
    // }
    // let dateformat = day+'/'+month+'/'+year+' '+hour+':'+minutes+' '+amPM; 
    let dateformat = moment().format('D/M/YYYY h:mm A');
    return dateformat;
  }
  function checkDateFormat(date){
    let check;
   //  dateonly
   //  let pattern = new RegExp('^(?:(?:31(\/|-|\\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\\1|(?:(?:29|30)(\/|-|\\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\/|-|\\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$');
   // let pattern = new RegExp('(?n:^(?=\\d)((?<day>31(?!(.0?[2469]|11))|30(?!.0?2)|29(?(.0?2)(?=.{3,4}(1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|0?[1-9]|1\\d|2[0-8])(?<sep>[/.-])(?<month>0?[1-9]|1[012])\\2(?<year>(1[6-9]|[2-9]\\d)\\d{2})(?:(?=\x20\\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\\d){0,2}(?i:\\ [AP]M))|([01]\\d|2[0-3])(:[0-5]\\d){1,2})?$)') 
   // if(pattern.test(date)==true){
   //    check = true;
   //  }

   //thua,code chay
    check = moment(date,'D/M/YYYY h:mm a',true).isValid()
    return check;
 }