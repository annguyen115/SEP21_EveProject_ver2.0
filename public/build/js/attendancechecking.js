$(document).ready(function(){
  var stack_bottomleft = {"dir1": "up", "dir2": "right",push:"top",firstpos1: 25, firstpos2: 25};
    $('.qa-attend-status button').on('click',function(){
      var myID = $(this).parent().prop('id');
      $('#'+myID+" :button").prop('disabled', false);
      $(this).prop('disabled', true);
      let mystatus = $(this).html();
      let statusid =0;
      let selectstatus = $(this).parents('.qa-body').find('.qa-status').attr('id');
      let attendanceid = $(this).parents('.qa-body').find('.qa-attend-id').html();
      let studentname = $(this).parents('.qa-body').find('.qa-fullname').html();
      $('#'+selectstatus+' span').removeClass();
      if(mystatus =='Not Attend'){
        $('#'+selectstatus+' span').addClass('notattend');
        statusid = 0;
      }else if(mystatus=="Attend"){
        $('#'+selectstatus+' span').addClass('attend');
        statusid =1;
      }else if(mystatus=="Absent"){
        $('#'+selectstatus+' span').addClass('absent');
        statusid = 2;
      }
      let myattendance = {
        attendance_id:attendanceid,
        status : statusid,
        name : studentname
      }
      console.log(myattendance);
      $.ajax({ 
        type:'POST',
        data:{myattendance},
        url:mypath+'/action/update-attendance',
        dataType:'json'
      }).done(function(response){
        console.log('response:'+JSON.stringify(response));
        var status = response.createStatus;
        if(status == "succeed"){
          
          PNotify.alert({
            title: 'Success!',
            text: response.message,
            type: 'success',
            styling: 'bootstrap4',
            icons:'fontawesome5',
            delay:1500,
            animate: {
              animate: true,
              in_class: 'bounceIn',
              out_class: 'bounceOut'
            },
            addclass: "stack-custom",
            stack: stack_bottomleft
          });
        }
      });
    });
  });