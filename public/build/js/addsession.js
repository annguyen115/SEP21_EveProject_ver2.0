$(function(){ 
  $('#session-modal').on('click',function(){
      createAddSessionModal();
  });
  
})















//Function HQ


// ~~> Add Session HQ //
/* remove Form add Session Status */
function removeAllFormStatus(){
  let a = $('#addSessionForm .form-group');
  //remove success status
  if(a.hasClass('has-success')){
    a.removeClass('has-success')
  }
  if($('#sessionpicker .glyphicon-ok').length>0){
    $('#sessionpicker .glyphicon-ok').remove();
  }
  //remove error status
  if(a.hasClass('has-error')){
    a.removeClass('has-error')
  }if($('#sessionpicker .glyphicon-remove').length>0){
    $('#sessionpicker .glyphicon-remove').remove();
  }if($('#addSessionForm .errormessage').length>0){
    $('#addSessionForm .errormessage').remove();
  }
}
/* Create Add Session Modal */
function createAddSessionModal(){
  //get current datetime
  let date = getCurrentDateTime();
  $.confirm({
    // icon: 'far fa-calendar-plus',
    title: 'Add Session',
    content: 'url:/build/personal/text/addsessionForm.txt',
    theme: 'material,qa-addsession',
    type: 'black',
    animationBounce: 1.5,
    animation:'top',
    typeAnimated: true,
    closeIcon: true,  
    closeIconClass: 'fa fa-close',
    // autoClose: 'cancel|8000',
    columnClass: 'col-md-3 col-md-offset-4',
    smoothContent:true,
    // backgroundDismiss:true,
    draggable: true,
    dragWindowGap: 50,
    alignMiddle:true,
    onContentReady: function () {   
      $('.jconfirm').css('z-index', 99);   
      dateTimePicker(date);
      // dp.change
      $('#sessionpicker input').on('dp.hide',function() {
        let session_date = $('#sessionpicker input').val().trim().toUpperCase();
        let error_message='';
        let a = $('.form-group');
        if(!session_date){
          // remove success status
          removeAllFormStatus();
          //check null
          error_message='this field is required';
          // $('#sessionpicker .glyphicon-remove').remove();
          if(!a.hasClass('has-error')){
            a.addClass('has-error')
          }
          if($('#sessionpicker .glyphicon-remove').length==0){
            $('#sessionpicker').append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
          }if($('#addSessionForm .errormessage').length==0){
            $('#addSessionForm .form-group').append('<small class="errormessage">'+error_message+'</small>')
          }
        }else{
          //remove error if they existed
          removeAllFormStatus();
          //add success
          if(!a.hasClass('has-success')){
            a.addClass('has-success')
          }
          if($('#sessionpicker .glyphicon-ok').length==0){
            $('#sessionpicker').append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
          }

        }
      })
    },
    buttons: {
        AddSession: {
            text: 'Add Session',
            btnClass: 'btn-success',
            action: function () {
              let a = this.$content.find('.form-group');
              let session_date = this.$content.find('#sessionpicker input').val().trim().toUpperCase()  ;
              let error_message='';
              if(!session_date){
                //remove success status
                removeAllFormStatus();
                //check null
                error_message='this field is required';
                if(!a.hasClass('has-error')){
                  a.addClass('has-error')
                }
                if($('#sessionpicker .glyphicon-remove').length==0){
                  $('#sessionpicker').append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
                }if($('#addSessionForm .errormessage').length==0){
                  $('#addSessionForm .form-group').append('<small class="errormessage">'+error_message+'</small>')
                }
                return false;
              }else{
                let a = session_date.split(" ");
                let datemonthyear =a[0];
                let time = a[1]+' '+a[2];
                let b = datemonthyear.split("/")
                let day = b[0];
                let month = b[1];
                let  year = b[2];
                let newSession  = new Date(month+'/'+day+'/'+year+' '+time);
                let newSessionDay ={
                  name : day+'/'+month+'/'+year+' '+time,
                  format: newSession,
                }
                createSessionAjax(newSessionDay);
              } 
            }
        },
        Cancel: {
          text:'Cancel',
            action: function () {
                
            } 
        }
      },
  });
}
//Ajax HQ


/* ~~> Ajax Add Session */
function createSessionAjax(newSessionDay){
  $.ajax({ 
    type:'POST',
    data:{newSessionDay},
    url:'/action/create-session',
    dataType:'json'
  }).done(function(response){
    console.log('response:'+JSON.stringify(response));
    var status = response.createStatus;
    console.log('status:'+status)
    if(status=="succeed"){
      $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
      $('#qa-success-alert').show('drop',function(){
        $('#qa-success-alert .qa-message').show();
      });
      setTimeout(()=>{
        $('#qa-success-alert .qa-message').hide();
        $('#qa-success-alert').hide('drop'); 
      },2500); 
        setTimeout(()=>{
          location.reload(); 
      },3500);  
    }else if(status=="failed"){
      $('#qa-error-alert .qa-message').html('<strong>Error! </strong>'+response.message);
      $('#qa-error-alert').show('drop',function(){
        $('#qa-error-alert .qa-message').show();
      });
      setTimeout(()=>{
        $('#qa-error-alert .qa-message').hide();
        $('#qa-error-alert').hide('drop'); 
      },2500); 
    }
  });
};
