$(function(){
  //modal add student
  $('#add-student-modal').on('click',function(){
    console.log($('#error-message').length >0);
    if($('#error-message').length >0){
          $('#error-message').remove();
    };
    $('#student-id').val('');
    $('#addstudent').modal('toggle');
    
  });
  $('#student-id').on('change paste keyup',function(){
    $('#error-message').remove();
    let studentid = $('#student-id').val().trim().toUpperCase();
    let studentidPattern= new RegExp('(^[a-zA-Z]\\d{6})$')
    if(studentid==''){
      $( '<span style="color:red;font-weight:bold" id="error-message">This field is required.</span>' ).insertBefore("#student-id");
    }else if(studentidPattern.test(studentid)==false){
      $( '<span style="color:red;font-weight:bold" id="error-message">This input not match studentID pattern.</span>').insertBefore("#student-id");
    }else{
      $('#error-message').remove();
    }
  });
  $('#addstudent-submit').on('click',function(){
      let studentid = $('#student-id').val().trim().toUpperCase();
      let studentidPattern= new RegExp('(^[a-zA-Z]\\d{6})$');
      $('#error-message').remove();
      if(studentid==''){
        $( '<span style="color:red;font-weight:bold" id="error-message">This field is required.</span>' ).insertBefore("#student-id");
      }else if(studentidPattern.test(studentid)==false){
        $( '<span style="color:red;font-weight:bold" id="error-message">This input not match studentID pattern.</span>').insertBefore("#student-id");
      }else{
          $('#addstudent').modal('hide');
          //ajax
          let newStudent ={
            id:studentid
          }
          $.ajax({ 
            type:'POST',
            data:{newStudent},
            url:'/action/create-student',
            dataType:'json'
          }).done(function(response){
            let status = response.createStatus;
            console.log(status);
            if(status=="succeed"){
              $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
              $('#qa-success-alert').show('drop',function(){
                $('#qa-success-alert .qa-message').show();
              });
              setTimeout(()=>{
                $('#qa-success-alert .qa-message').hide();
                $('#qa-success-alert').hide('drop'); 
              },2500); 
                setTimeout(()=>{
                  location.reload(); 
              },3500);
              // $('#studentlist-table').DataTable().row.add([12,'T2131214','Test','20/6/2342','100%','','','','']).draw();  
            }else if(status=="failed"){
              $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
              $('#qa-error-alert').show('drop',function(){
                $('#qa-error-alert .qa-message').show();
              });
              setTimeout(()=>{
                $('#qa-error-alert .qa-message').hide();
                $('#qa-error-alert').hide('drop'); 
              },3500); 
            }
        })
      }
  });
})