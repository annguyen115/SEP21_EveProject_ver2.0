$(document).ready(function(){
    $('#sync-student-btn').on('click',function(){
        let courseID = $('#course-id').text();
        $.ajax({ 
          type:'POST',
          data:{courseID},
          url:mypath+'/action/api/sync-student-to-server',
          dataType:'json'
        }).done(function(response){
          let status = response.createStatus;
          console.log(status);
          if(status=="succeed"){
            $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
            $('#qa-success-alert').show('drop',function(){
                $('#qa-success-alert .qa-message').show();
            });
            setTimeout(()=>{
              $('#qa-success-alert .qa-message').hide();
              $('#qa-success-alert').hide('drop'); 
            },2500);
          }else if(status=="failed"){
            $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
            $('#qa-error-alert').show('drop',function(){
              $('#qa-error-alert .qa-message').show();
            });
            setTimeout(()=>{
              $('#qa-error-alert .qa-message').hide();
              $('#qa-error-alert').hide('drop'); 
            },3500); 
          }
        });
    });
  });