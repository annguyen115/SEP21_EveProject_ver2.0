//ajax delete student
function deleteStudentAjax(studentID){
  let course_id = $('#course-id').text();
  let data ={
    student_id:studentID,
    course_id:course_id
  };
  $.ajax({ 
    type:'POST',
    data:{data},
    url:'/action/delete-student',
    dataType:'json'
  }).done(function(response){
    let status = response.createStatus;
    console.log(status);
    if(status=="succeed"){
      $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
      $('#qa-success-alert').show('drop',function(){
        $('#qa-success-alert .qa-message').show();
      });
      setTimeout(()=>{
        $('#qa-success-alert .qa-message').hide();
        $('#qa-success-alert').hide('drop'); 
      },2500); 
        setTimeout(()=>{
          location.reload(); 
      },3500);
    }else if(status=="failed"){
      $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
      $('#qa-error-alert').show('drop',function(){
        $('#qa-error-alert .qa-message').show();
      });
      setTimeout(()=>{
        $('#qa-error-alert .qa-message').hide();
        $('#qa-error-alert').hide('drop'); 
      },2500); 
    }
  });
};
$(function(){
  let name ;
  $('#remove-student-modal').on('click',function(){
    // console.log($('#error-message').length >0);
    if($('#error-message').length >0){
          $('#error-message').remove();
    };
    $('#remove-studentid').val('');
    $('#removestudent').modal('toggle');
    
  });
  //Autocomplete
  $('#remove-studentid').autocomplete({
    autoFocus: true,
    source: function( request,response) {
      let course_id = $('#course-id').text();
      let term = {
        data:request.term,
        course_id :course_id
      };
      $('#error-message').remove();
      $.ajax( {
        url: "/action/search-by-course/",
        type:'POST',
        dataType: "json",
        data: {term},
        success: function( data ) {
          if(!data.length){
            var result= [{
              label: 'No matches found', 
              value: response.term
              }];
              response(result);
          }else{
            // console.log(data);
            response(data)
          }
        }
      });
    },
    minLength: 1,
    select: function( event, ui ) {
      console.log('check:'+ui.item.id);
      name=ui.item.label;
      $('#remove-studentid').val(ui.item.id); 
      return false;
    }
  });
  $('#remove-submit').on('click',function(){
    let studentID = $('#remove-studentid').val().trim().toUpperCase();
    let studentidPattern= new RegExp('(^[a-zA-Z]\\d{6})$');      
    $('#error-message').remove();
    console.log('match?'+!studentidPattern.test(studentID));
    if(studentID ==''){
      $( '<span style="color:red;font-weight:bold" id="error-message">This field is required.</span>').insertBefore("#remove-studentid");
    }else if(studentidPattern.test(studentID)==false){
      $( '<span style="color:red;font-weight:bold" id="error-message">This input not match studentID pattern.</span>').insertBefore("#remove-studentid");
    }else{
      $('#removestudent').modal('hide');
      if (confirm('Are you sure you want to delete '+name+' ?')) {
          //ajax deleting student
          deleteStudentAjax(studentID);
      } else {
          alert('You just canceled deleting student');
      }
    }
  })
});