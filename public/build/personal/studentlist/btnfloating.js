let  fe1 = 0;
      let  fe2 = 0;
      $('.btn-floating-1').mouseenter(function(event){
          if(event.target==this){
            executeFloatingEffect1();
          }
      })
      $('.btn-floating-2 .btn-floating-main').click(function(event){ 
          executeFloatingEffect2();
      })
      $('#qa-import,#qa-export').on('click',function(){
          executeFloatingEffect1();
      })
      function executeFloatingEffect1(){
        if(fe1==0){
            $('.btn-floating-1>a').removeClass('btn-default').addClass('btn-primary');
            $('.btn-floating-1 .btn-floating-sub1,.btn-floating-1 .btn-floating-sub2').show(300);  
            $('.btn-floating-1 .btn-floating-sub1').animate({left:'60px',});
            $('.btn-floating-1 .btn-floating-sub2').animate({left:'60px',top:'45px'});
            $('.btn-floating-1 .btn-floating-sub1,.btn-floating-1 .btn-floating-sub2').fadeIn(100);
            fe1=1;
        }else{
            $('.btn-floating-1 .btn-floating-sub1>a,.btn-floating-1 .btn-floating-sub2>a').fadeOut(100);
            $('.btn-floating-1 .btn-floating-sub1,.btn-floating-1 .btn-floating-sub2').animate({left:'31%',top:'35%'});
            $('.btn-floating-1 .btn-floating-sub1,.btn-floating-1 .btn-floating-sub2').hide('slow');
            $('.btn-floating-1>a').removeClass('btn-primary').addClass('btn-default');
            fe1=0;
        }
      }
      function executeFloatingEffect2(){
        if(fe2==0){
          $('.btn-floating-2 .btn-floating-main').animate({left:'47px'},400);
          $('.btn-floating-2 .btn-floating-main').removeClass('btn-default').addClass('btn-primary');
          $('.btn-floating-2 .btn-floating-sub1').show('slow');
          $('.btn-floating-2 .btn-floating-sub1').delay(100).animate({top:'-23px'},400);
          $('.btn-floating-2 .btn-floating-sub3').delay(600).show('slow');
          $('.btn-floating-2 .btn-floating-sub3').delay(100).animate({top:'-23px'},400);
          $('.btn-floating-2 .btn-floating-sub2').delay(1200).show('slow');
          $('.btn-floating-2 .btn-floating-sub2').delay(100).animate({top:'-62px'},400);
          fe2=1;
        }else{
          $('.btn-floating-2 .btn-floating-sub2').animate({top:'-23px'},400);
          $('.btn-floating-2 .btn-floating-sub2').fadeOut('slow');
          $('.btn-floating-2 .btn-floating-sub3').delay(500).animate({top:'-120px'},400);
          $('.btn-floating-2 .btn-floating-sub3').fadeOut('slow');
          $('.btn-floating-2 .btn-floating-sub1').delay(1000).animate({top:'-120px'},400);
          $('.btn-floating-2 .btn-floating-sub1').fadeOut('slow');
          $('.btn-floating-2 .btn-floating-main').delay(1400).animate({left:'0px'},400);
          setTimeout(()=>{
            $('.btn-floating-2 .btn-floating-main').delay(1800).addClass('btn-default').removeClass('btn-primary');
          },1900);  
          fe2=0;
        }
      };