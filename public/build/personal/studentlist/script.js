import PNotifyStyleMaterial from '/vendors/pnotify/dist/es/PNotifyStyleMaterial.js';
//param
var attendance_modal,addsession_modal,editorremovesession_modal,removestudent_modal,editsession_modal,
name;
//datatable
var table,table2,data,data2,stdt,acdt;
var tableName='#studentlist-table',
tableName2='#attendance-table';
var addsesion_target = {
  main : '#addSessionForm',
  sub : '#sessionpicker',
  error_message:'this field is required',
}
var attendance_target = {
  main : '#attendanceForm',
  sub : '#attendance-select',
  error_message:'this field is required',
}
var addstudent_target = {
  main : '#addStudentForm',
  sub : '#addstudent',
  error_message:'this field is required',
}
var removestudent_target = {
  main : '#removeStudentForm',
  sub : '#removestudent',
  error_message:'this field is required',
}
var editorremovesession_target = {
  main : '#editorremoveForm',
  sub : '#session-select',
  error_message:'this field is required',
}
//edit session param
var edit_newSessionName_target = {
  main : '#editSessionForm',
  sub : '#edit-newsessionname',
  error_message:'You have reached your maximum limit of 16 characters allowed',
}
var edit_newSessionDate_target = {
  main : '#editSessionForm',
  sub : '#edit-newsessiondate',
  error_message:'this field is required',
}
var studentidPattern= new RegExp('(^[a-zA-Z]\\d{6})$');
var tableNeedUpdate = false;
//dùng chứa session id cho attendance checking
var mySessionID;
//Notify Position
var stack_bottomleft = {"dir1": "up", "dir2": "right",push:"top",firstpos1: 25, firstpos2: 25};
$(function(){
    //DataTable
    studentlistDataTable();
    /*~~> Column Hover */
    // $(tableName+' tbody').on('mouseenter', 'td', function () {
    //     var colIdx = table.cell(this).index().column;

    //     $( table.cells().nodes() ).removeClass( 'highlight' );
    //     $( table.column( colIdx ).nodes() ).addClass( 'highlight' );
    // });

    //Add Session HQ
    $('#session-modal').on('click',function(){
        createAddSessionModal();
    });
    // dp.change
    $('body').on('dp.hide','#sessionpicker input',function() {
      let session_date = $('#sessionpicker input').val().trim().toUpperCase();
      let error_message='';
      let a = $('#addSessionForm .form-group');
      let check = checkDateFormat(session_date);
      // console.log(session_date);
      
      // remove all status
      removeAllStatus(addsesion_target)
      if(!session_date||check==false){
        //check null
        //add error
        setErrorStatus(addsesion_target);
      }else{
        //add success
        setSuccessStatus(addsesion_target);
      }
    })


    
    //Attendance HQ
    $('#addtendance-modal').on('click',function(){
        createAttendanceModal();
    })
    $('body').on('change','#attendance-select>select[name="session_id"]',function(){
      let value = $('#attendance-select>select[name="session_id"]').val();
      
      //remove all status
      removeAllStatus(attendance_target)

      if(value=="addnew"){
        attendance_modal.close();
        createAddSessionModal();
      }else if(value==-1){
          setErrorStatus(attendance_target);  
      }else{
          setSuccessStatus(attendance_target);
      }
    });

    //Add Student HQ
    $('#add-student-modal').on('click',function(){
      createAddStudentModal();
    })
    $('body').on('change paste keyup','#addstudent>input',function(){
        let student_id = $('#addstudent>input').val().trim().toUpperCase();
        removeAllStatus(addstudent_target);
        if(student_id==''){
          //check null
          addstudent_target.error_message = "this field is required";
          setErrorStatus(addstudent_target);
        }else if(studentidPattern.test(student_id)==false){
          addstudent_target.error_message = "this input not match studentID pattern.";
          setErrorStatus(addstudent_target);
        }else{
          setSuccessStatus(addstudent_target);
        }
    })

    //Remove Student HQ
    $('#remove-student-modal').on('click',function(){
      createRemoveStudentModal();
    })
    $('body').on('change paste keyup','#removestudent>input',function(){
      let student_id = $('#removestudent>input').val().trim().toUpperCase();
      removeAllStatus(removestudent_target);
      if(student_id==''){
        //check null
        removestudent_target.error_message = "this field is required";
        setErrorStatus(removestudent_target);
      }else if(studentidPattern.test(student_id)==false){
        removestudent_target.error_message = "this input not match studentID pattern.";
        setErrorStatus(removestudent_target);
      }else{
        setSuccessStatus(removestudent_target);
      }
    });
    //Remove Session HQ
    $('#edit-delete-session-modal').on('click',function(){
      createEditRemoveSessionModal();
    })
    $('body').on('change','#session-select>select[name="session_id"]',function(){
      let value = $('#session-select>select[name="session_id"]').val();
      
      //remove all status
      removeAllStatus(editorremovesession_target)

      if(value=="addnew"){
        attendance_modal.close();
        createAddSessionModal();
      }else if(value==-1){
          setErrorStatus(editorremovesession_target);  
      }else{
          setSuccessStatus(editorremovesession_target);
      }
    });
    //Edit Session HQ
    $('body').on('change paste keyup','#edit-newsessionname>input',function(){
      let newSessionName = $('#edit-newsessionname>input').val().trim();
      
      removeAllStatus(edit_newSessionName_target);
      if(newSessionName.length>16){
        setErrorStatus(edit_newSessionName_target);
      }else{
        setSuccessStatus(edit_newSessionName_target);
      }
    })
    $('body').on('dp.hide','#edit-newsessiondate input',function() {
      let session_date = $('#edit-newsessiondate input').val().trim().toUpperCase();
      let error_message='';
      let check = checkDateFormat(session_date);
      // console.log(session_date);
      
      // remove all status
      removeAllStatus(edit_newSessionDate_target)
      if(!session_date||check==false){
        //check null
        //add error
        setErrorStatus(edit_newSessionDate_target);
      }else{
        //add success
        setSuccessStatus(edit_newSessionDate_target);
      }
    })
    // Sync Student
    $('#sync-student-btn').on('click',function(){
      var myConfig = {
        content:'Are you sure you want to synchronize <b>studentlist\'s '+mycourseid+'</b> to server?',
        function:{
             syncStudent: {
               text: 'Yes',
               btnClass: 'btn-danger',
               action: function () {
                  $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
                  let message = 'Synchronizing studentlist\'s <b>'+mycourseid+'</b> to server, Please wait....';
                  $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
                  $('#qa-infor-alert span').html(message);
                  $('#qa-infor-alert').show('drop',function(){
                    $('#qa-infor-alert .qa-message').show();
                  });
                  syncStudentToServerAjax(mycourseid);
               }
           },
           cancel: {
             text:'No',
               action: function () {
                   
               } 
           }
        }
      }
      createConfirmModal(myConfig);
    });
    $('#sync-attendance-btn').on('click',function(){
      var myConfig = {
        content:'Are you sure you want to synchronize <b>attendance\'s '+mycourseid+'</b> to server?',
        function:{
             syncAttendance: {
               text: 'Yes',
               btnClass: 'btn-danger',
               action: function () {
                  $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
                  let message = 'Synchronizing attendance\'s <b>'+mycourseid+'</b> to server, Please wait....';
                  $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
                  $('#qa-infor-alert span').html(message);
                  $('#qa-infor-alert').show('drop',function(){
                    $('#qa-infor-alert .qa-message').show();
                  });
                  syncAttendanceToServerAjax(mycourseid);
               }
           },
           cancel: {
             text:'No',
               action: function () {
                   
               } 
           }
        }
      }
      createConfirmModal(myConfig);
    });
    //Attendance Checking
    //click show modal 
    $('body').on('click',tableName+'>thead>tr>th.qa-attendance-session',function(){
        let session_id = $(this).find('.session_name').attr('query-data');
        mySessionID = session_id;
        let session = mysessionlist.find(function(element) {
          return element.session_id == session_id;
        });
        attendanceCheckingModal(session);
    });
    //click attend
    $('body').on('click','.qa-attendance-session button',function(){
      var query = $(this).parents('span').attr('query-data');
      $(this).parents('span').find(':button').prop('disabled', false);
      $(this).prop('disabled', true);
      // let mystatus = $(this).html();
      let statusid =0;
      let selectstatus = $(this).parents('tr').find('.qa-status');
      // let attendanceid = $(this).parents('.qa-body').find('.qa-attend-id').html();
      // let studentname = $(this).parents('.qa-body').find('.qa-fullname').html();
      selectstatus.removeClass();
      selectstatus.addClass('qa-status')
      if($(this).hasClass("btn-attend")){
        $(selectstatus).addClass('attend');
        statusid =1;
      }else if($(this).hasClass("btn-absent")){
        $(selectstatus).addClass('absent');
        statusid = 2;
      }
      query = query.split(' ');
      let myattendance = {
        attendance_id:query[1],
        status : statusid,
        student_id : query[0],
      }
      attendanceCheckingAjax(myattendance);
    })
    //Attendance statistics
    $('body').on('click','#qa-statistics',function(){
        console.log(mySessionID)
        createStatisticsModal(mySessionID);
    })
  })
  //Import XLSX
  $('#files').on('change',function(event){
    let filename = event.target.files[0].name;
    let a = filename.split('.');
    let extenstion = a[1].toLowerCase();
    console.log(extenstion);
    let message;
    if(extenstion!='xlsx'){
      message='<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;color:#fff"> We only support .xlsx file </span>'
      $('#qa-error-alert .qa-message').html(message);
      $('#qa-error-alert').show('drop',function(){
        $('#qa-error-alert .qa-message').show();
      });
      setTimeout(()=>{
        $('#qa-error-alert .qa-message').hide();
        $('#qa-error-alert').hide('drop'); 
      },3000);  
    }else{
      $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
      message = 'Your file ('+filename+') is in processing, Please wait....';
      $('#qa-infor-alert .qa-message').html('<i class="fa fa-refresh fa-spin fa-lg" aria-hidden="true" style="color:#fff"></i><span style="margin-left:10px;"></span>')
      $('#qa-infor-alert span').html(message);
      $('#qa-infor-alert').show('drop',function(){
        $('#qa-infor-alert .qa-message').show();
      });
      /* set up XMLHttpRequest */
      var tmppath = URL.createObjectURL(event.target.files[0]);
      var oReq = new XMLHttpRequest();
      oReq.open("GET", tmppath, true);
      oReq.responseType = "arraybuffer";
      oReq.onload = function(e) {
          var arraybuffer = oReq.response;

          /* convert data to binary string */
          var data = new Uint8Array(arraybuffer);
          var arr = new Array();
          for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
          var bstr = arr.join("");

          /* Call XLSX */
          var workbook = XLSX.read(bstr, {type:"binary"});

          /* DO SOMETHING WITH workbook HERE */
          var first_sheet_name = workbook.SheetNames[0];
          /* Get worksheet */
          var worksheet = workbook.Sheets[first_sheet_name];
          var dataresult = XLSX.utils.sheet_to_json(worksheet,{raw:true,defval:null});
          console.log('notformat:'+dataresult)
          for(var i= 0; i< dataresult.length;i++){
            let d =dataresult[i];
            let attendance = [];
            for(var key in d){                  
              if(key.toLowerCase()!='no'&&key.toLowerCase()!='id'&&key.toLowerCase()!='fullname'&&key.toLowerCase()!='birthday'){
                let keySplit = key.split('\n'); 
                console.log('split:'+keySplit[1]);
                let a = {
                    session_name:keySplit[0],
                    session_date:keySplit[1],
                    session_time:keySplit[2],
                    status:d[key],
                }
                attendance.push(a);
                delete d[key];
              }
            }
            // console.log('at:'+attendance);
            d['attendance'] = attendance;
              
          }
          importAjax(dataresult,filename); 
        }
      oReq.send();
    }
  })
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  //Function HQ
  // ~~> Common HQ
  function setSuccessStatus(target){
      var main = target.main,
      sub = target.sub;      
      // let a = $(main+' .form-group');
      let a = $(sub).parents('.form-group');
      if(!a.hasClass('has-success')){
        a.addClass('has-success')
      }
      if($(sub+' .glyphicon-ok').length==0){
        $(sub).append('<span class="glyphicon glyphicon-ok form-control-feedback qa-form-status"></span>');
      }
  }
  function setErrorStatus(target){
    var main = target.main,
    sub = target.sub;      
    let error_message=target.error_message;
    // let a = $(main+' .form-group');
    let a = $(sub).parents('.form-group');
    let b = '#'+a.attr('id');
    if(!a.hasClass('has-error')){
      a.addClass('has-error')
    }
    if($(sub+' .glyphicon-remove').length==0){
      $(sub).append('<span class="glyphicon glyphicon-remove form-control-feedback qa-form-status"></span>');
    }if($(b+' .errormessage').length==0){
      // $(main+' .form-group').append('<small class="errormessage">'+error_message+'</small>')
       $(b).append('<small class="errormessage">'+error_message+'</small>')
    }
  }
  function removeAllStatus(target){
    var main = target.main,
    sub = target.sub;      
    // let a = $(main+' .form-group');
    let a = $(sub).parents('.form-group');
    let b = '#'+a.attr('id');
    if(a.hasClass('has-success')){
      a.removeClass('has-success')
    }
    if($(sub+' .glyphicon-ok').length>0){
      $(sub+' .glyphicon-ok').remove();
    }
    //remove error status
    if(a.hasClass('has-error')){
      a.removeClass('has-error')
    }if($(sub+' .glyphicon-remove').length>0){
      $(sub+' .glyphicon-remove').remove();
    }if($(b+' .errormessage').length>0){
      $(b+' .errormessage').remove();
    }
  }
  function createConfirmModal(myConfig){
      $.confirm({
        icon: 'fa fa-exclamation-triangle',
        title: 'Are you sure?',
        content: myConfig.content,
        theme: 'material',
        type: 'red',
        typeAnimated: true,
        closeIcon: true,
        closeIconClass: 'fa fa-close',
        autoClose: 'cancel|12000',
        columnClass: 'col-md-4 col-md-offset-4',
        buttons: myConfig.function
    });
  }
  function updateSessionList(){
    let newSessionList = $.ajax(mypath+'/action/update-session-list?course_id='+mycourseid).done(function () {
      mysessionlist = JSON.parse(newSessionList.responseText);
      mysessionlist = mysessionlist.data
      console.log(mysessionlist);
    })
  }
  function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  // ~~> Add Session HQ //
  /* DataTable */
  function studentlistDataTable(){
    //get data = ajax
    stdt  = $.ajax(mypath+'/action/studentlist-datatable-getdata?course_id='+mycourseid).done(function () {
        data = JSON.parse(stdt.responseText);
        // đưa table về trạng thái ban đầu của nó
        $(tableName+'>thead>tr').empty();
        $(tableName+'>tbody>tr').empty();
        //add column
        $.each(data.columns, function (k, colObj) {
            let str = '<th>' + colObj.title + '</th>';
            $(str).appendTo(tableName+'>thead>tr');
        });
        var option = {
          "select": true,
          "processing": true,
          "serverSide": false,
          "pagingType": "full_numbers",
          "ordering": true,
          "info": true,
          "searching": true,
          "bFilter": false,
          "bDestroy": true,
          "bLengthChange": true,
          "bPaginate": true,
          "deferRender": true,
          language:{
            'loadingRecords': 'StudentList is loading...',
            'processing': 'Please wait...',
          },
          responsive: true,
          paging: true,
          dom: 'Bfrtip',
          lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
          ],
          buttons: [
              { extend:'colvis',
                columns:':not(:nth-child(3))',
                text:'Show/Hide columns',
                className:'datatable-showhide',
              },
              {extend:'colvisRestore',className:'datatable-showall',text:'Show all columns'},
              // 'pageLength',
              {extend:'pageLength',className:'datatable-length'},
              {text: 'Reload',
              className:'datatable-reload',
              action: function ( e, dt, node, config ) {
                  $('#ajax-loading').show('fade');
                  setTimeout(()=>{
                    datatableReDraw();
                  },2500); 
                  setTimeout(()=>{
                    $('#ajax-loading').hide('fade');
                  },3000);  
              }}
          ],
          "columns": data.columns,
          "data":data.data,  
          order:[]
        }
        $(tableName).on('init.dt', function() {
          $('.datatable-showhide').attr({'data-toggle':'tooltip','data-placement':'top','title':'Show or Hide columns'});
          $('.datatable-length').attr({'data-toggle':'tooltip','data-placement':'top','title':'Show length'});
          $('.datatable-reload').attr({'data-toggle':'tooltip','data-placement':'top','title':'Reload table'});
          $('.datatable-showall').attr({'data-toggle':'tooltip','data-placement':'top','title':'Show all hidden columns'});
          $('[data-toggle="tooltip"]').tooltip();
        });
        table =  $(tableName).DataTable(option);
        new $.fn.dataTable.FixedHeader(table);
        $(tableName+'>tbody>tr>td:nth-child(5)').css({'padding':'0px'});
    })
  }
  function datatableReDraw(){
      table.destroy();
      studentlistDataTable(); 
  }
  function attendanceCheckingDataTable(session_id){
    acdt = $.ajax(mypath+'/action/attendancechecking-datatable-getdata?course_id='+mycourseid+'&session_id='+session_id).done(function () {
      data2 = JSON.parse(acdt.responseText);
      // đưa table về trạng thái ban đầu của nó
      $(tableName2+'>thead>tr').empty();
      $(tableName2+'>tbody>tr').empty();
      //add column
      $.each(data2.columns, function (k, colObj) {
          let str = '<th>' + colObj.title + '</th>';
          $(str).appendTo(tableName2+'>thead>tr');
      });
      var option = {
        "select": true,
        "processing": true,
        "serverSide": false,
        "pagingType": "full_numbers",
        "ordering": true,
        "info": true,
        "searching": true,
        "bFilter": false,
        "bDestroy": true,
        "bLengthChange": true,
        "bPaginate": true,
        "deferRender": true,
        language:{
          'loadingRecords': 'StudentList is loading...',
          'processing': 'Please wait...',
        },
        responsive: true,
        paging: true,
        dom: 'Bfrtip',
        lengthMenu: [
          [ 10, 25, 50, -1 ],
          [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
          { extend:'colvis',
            columns:':not(:nth-child(3)):not(:nth-child(5)):not(:nth-child(6))',
            text:'Show/Hide columns',
            className:'datatable-showhide',
          },
          {extend:'colvisRestore',className:'datatable-showall',text:'Show all columns'},
        // 'pageLength',
          {extend:'pageLength',
          className:'datatable-length'
          }],
        "columns": data2.columns,
        "data":data2.data,  
        order:[]
      }
      $(tableName2).on('init.dt', function() {
        $('.datatable-showhide').attr({'data-toggle':'tooltip','data-placement':'top','title':'Show or Hide columns'});
        $('.datatable-length').attr({'data-toggle':'tooltip','data-placement':'top','title':'Show length'});
        $('.datatable-showall').attr({'data-toggle':'tooltip','data-placement':'top','title':'Show all hidden columns'});
        $('[data-toggle="tooltip"]').tooltip();
      });
      table2 =  $(tableName2).DataTable(option);
      $(tableName2+'>tbody>tr>td:nth-child(5)').css({'padding':'0px'});
    })
  }
  function addNewSessionColumn(newSession){
      let header,body;
      if($('.attendance-percent').length==0){
        header ='<th class="attendance-percent sorting" tabindex="0" aria-controls="studentlist-table" rowspan="1" colspan="1" style="width: 72px;" aria-label="Attendance: activate to sort column ascending">Attendance</th>'
        body = '<td class="attendance-percent" style="padding: 0px;"><span class="danger qa-attendance-percent">0 %</span></td>'
        $('table>thead>tr').append(header)
        $('table>tbody>tr').append(body)
      }
      header ='<th class="qa-attendance-session sorting_disabled" rowspan="1" colspan="1" style="width: 177px;"><span class="session_name" query-data="'+newSession.id+'">'+newSession.name+'</span><br /><span class="session_date">'+newSession.date+'</span></th>'
      $('table>thead>tr').append(header);
      $('table>tbody>tr').append('<td></td>')
  }
  /* Create Add Session Modal */
  function createAddSessionModal(){
    //get current datetime
    let current_datetime = getCurrentDateTime();
    $.confirm({
      icon: 'far fa-calendar-plus',
      title: 'Add Session',
      content: 'url:/build/personal/text/addsessionForm.txt',
      theme: 'material,qa-addsession',
      type: 'black',
      animationBounce: 1.5,
      animation:'top',
      typeAnimated: true,
      closeIcon: true,  
      closeIconClass: 'fa fa-close',
      // autoClose: 'cancel|8000',
      columnClass: 'col-md-3 col-md-offset-4',
      smoothContent:true,
      backgroundDismiss:true,
      draggable: true,
      dragWindowGap: 50,
      alignMiddle:true,
      onContentReady: function () {   
        $('.jconfirm').css('z-index', 99);   
        dateTimePicker();
        // $('#sessionpicker input').val(current_datetime);
      },
      buttons: {
          AddSession: {
              text: 'Add Session',
              btnClass: 'btn-blue',
              action: function () {
                let a = this.$content.find('.form-group');
                let session_date = this.$content.find('#sessionpicker input').val().trim().toUpperCase()  ;
                let error_message='';
                if(!session_date){
                  //remove success status
                  removeAllStatus(addsesion_target);
                  setErrorStatus(addsesion_target)
                  return false;
                }else{
                  // let a = session_date.split(" ");
                  // let datemonthyear =a[0];
                  // let time = a[1]+' '+a[2];
                  // let b = datemonthyear.split("/")
                  // let day = b[0];
                  // let month = b[1];
                  // let  year = b[2];
                  let format = formatDateToServer(session_date)
                  let newSession  = new Date(format);
                  let newSessionDay ={
                    name : session_date,
                    format: newSession,
                    course_id:mycourseid
                  }
                  addSessionAjax(newSessionDay);
                } 
              }
          },
          Cancel: {
            text:'Cancel',
              action: function () {
                  
              } 
          }
        },
    });
  }
  
  // ~~> Attendance HQ //
  /* Create Attendance Modal*/
  function createAttendanceModal(){
        attendance_modal = $.confirm({
        icon: 'glyphicon glyphicon-list-alt',
        title: 'Attendance Online',
        content: 'url:/build/personal/text/attendanceForm.txt',
        theme: 'material,qa-attendance',
        type: 'black',
        animationBounce: 1.5,
        animation:'top',
        typeAnimated: true,
        closeIcon: true,  
        closeIconClass: 'fa fa-close',
        // autoClose: 'cancel|8000',
        columnClass: 'col-md-3 col-md-offset-4',
        smoothContent:true,
        backgroundDismiss:true,
        draggable: true,
        dragWindowGap: 50,
        alignMiddle:true,
        onContentReady: function () { 
          $('#attendance-select input[name="course_id"]').val(mycourseid);
          $('#attend-form').attr('action',mypath+'/attendance');
          if(mysessionlist!=null){
            let a =$('#attendance-select>select[name="session_id"]');
            for(let i of mysessionlist){
                var dateFormat = formatDateFromServer(i.session_date);
                let option = '<option value='+i.session_id+'>'+i.session_no+'  |  '+dateFormat[0]+'/'+dateFormat[1]+'/'+dateFormat[2]+'</option>'
                a.append(option)
            }
          }
        },
        buttons: {
            Attendance: {
                text: 'Attend',
                btnClass: 'btn-blue',
                action: function () {
                    let course_id = $('#attendance-select input[name="course_id"]').val();
                    let attendance_id= $('#attendance-select>select[name="session_id"]').val();
                    let a = this.$content.find('.form-group');
                    if(attendance_id!=-1 && attendance_id!='addnew'){
                      $('#attend-form').submit();
                    }else if(attendance_id==-1){
                      removeAllStatus(attendance_target);
                      setErrorStatus(attendance_target);
                      return false;
                    }   
                }
            },
            Cancel: {
              text:'Cancel',
                action: function () {
                    
                } 
            }
          },
      });
  }

  // ~~> Add Student HQ
  function createAddStudentModal(){
    attendance_modal = $.confirm({
      icon: 'fas fa-user-plus',
      title: 'Add Student',
      content: 'url:/build/personal/text/addstudentForm.txt',
      theme: 'material,qa-addstudent',
      type: 'black',
      animationBounce: 1.5,
      animation:'top',
      typeAnimated: true,
      closeIcon: true,  
      closeIconClass: 'fa fa-close',
      // autoClose: 'cancel|8000',
      columnClass: 'col-md-3 col-md-offset-4',
      smoothContent:true,
      backgroundDismiss:true,
      draggable: true,
      dragWindowGap: 50,
      alignMiddle:true,
      onContentReady: function(){
        $('#addstudent input').tooltip({
          'trigger':'focus',
          template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>' 
        });
      },
      buttons: {
          AddStudent: {
              text: 'Add',
              btnClass: 'btn-blue',
              action: function () {
                let student_id = this.$content.find('#addstudent>input').val().trim().toUpperCase();
                // let a = this.$content.find('.form-group');
                removeAllStatus(addstudent_target);
                if(student_id==''){
                  //check null
                  addstudent_target.error_message = "this field is required";
                  setErrorStatus(addstudent_target);
                  return false;
                }else if(studentidPattern.test(student_id)==false){
                  addstudent_target.error_message = "this input not match studentID pattern";
                  setErrorStatus(addstudent_target);
                  return false;
                }else{
                  AddStudentAjax(student_id,mycourseid);
                }
              }
          },
          Cancel: {
            text:'Cancel',
              action: function () {
                  
              } 
          }
        },
    });
  }

  // ~~> Remove Student HQ
  function createRemoveStudentModal(){
    removestudent_modal = $.confirm({
      icon: 'fas fa-user-minus',
      title: 'Remove Student',
      content: 'url:/build/personal/text/removestudentForm.txt',
      theme: 'material,qa-removeStudent',
      type: 'black',
      animationBounce: 1.5,
      animation:'top',
      typeAnimated: true,
      closeIcon: true,  
      closeIconClass: 'fa fa-close',
      // autoClose: 'cancel|8000',
      columnClass: 'col-md-3 col-md-offset-4',
      smoothContent:true,
      backgroundDismiss:true,
      draggable: true,
      dragWindowGap: 50,
      alignMiddle:true,
      onContentReady: function () {
        $('#removestudent input').tooltip({
          'trigger':'focus',
          template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>' 
        });   
        removeStudentAutoComplete();
      },
      buttons: {
          RemoveStudent: {
              text: 'Remove',
              btnClass: 'btn-blue',
              action: function () {
                let student_id = this.$content.find('#removestudent>input').val().trim().toUpperCase();
                removeAllStatus(removestudent_target)
                if(student_id ==''){
                  removestudent_target.error_message="this field is required",
                  setErrorStatus(removestudent_target);
                  return false;
                }else if(studentidPattern.test(student_id)==false){
                  removestudent_target.error_message="this input not match studentID pattern.",
                  setErrorStatus(removestudent_target);
                  return false;
                }else{
                  removestudent_modal.close();
                  var myConfig = {
                    content:'Are you sure you want to delete <b>'+name+'</b> From <b>' +mycourseid+'</b>?',
                    function:{
                         deleteSession: {
                           text: 'Yes',
                           btnClass: 'btn-danger',
                           action: function () {
                              deleteStudentAjax(student_id);
                           }
                       },
                       cancel: {
                         text:'No',
                           action: function () {
                               
                           } 
                       }
                    }
                  }
                  createConfirmModal(myConfig);
                }
              }
          },
          Cancel: {
            text:'Cancel',
              action: function () {
                  
              } 
          }
        },
    });
  }

  function removeStudentAutoComplete(){
    $('#removestudent>input').autocomplete({
      autoFocus: true,
      source: function( request,response) {
        let term = {
          data:request.term,
          course_id :mycourseid,
        };
        $.ajax( {
          url: mypath+"/action/search-by-course/",
          type:'POST',
          dataType: "json",
          data: {term},
          success: function( data ) {
            if(!data.length){
              var result= [{
                label: 'No matches found', 
                value: response.term
                }];
                response(result);
            }else{
              // console.log(data);
              response(data)
            }
          }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        console.log('check:'+ui.item.id);
        name=ui.item.label;
        $('#removestudent>input').val(ui.item.id); 
        return false;
      }
    });
  }


  // ~~> Edit-Remove Session Modal
  function createEditRemoveSessionModal(){
    editorremovesession_modal = $.confirm({
      icon: 'far fa-calendar-alt',
      title: 'Edit or Remove Session',
      content: 'url:/build/personal/text/editorremovesessionForm.txt',
      theme: 'material,qa-editorremove',
      type: 'black',
      animationBounce: 1.5,
      animation:'top',
      typeAnimated: true,
      closeIcon: true,  
      closeIconClass: 'fa fa-close',
      // autoClose: 'cancel|8000',
      columnClass: 'col-md-3 col-md-offset-4',
      smoothContent:true,
      backgroundDismiss:true,
      draggable: true,
      dragWindowGap: 50,
      alignMiddle:true,
      onContentReady: function () {   
        $('#session-select>input[name="course_id"]').val(mycourseid);
          if(mysessionlist!=null){
            let a =$('#session-select>select[name="session_id"]');
            for(let i of mysessionlist){
                var dateFormat = formatDateFromServer(i.session_date);
                let option = '<option value='+i.session_id+'>'+i.session_no+'  |  '+dateFormat[0]+'/'+dateFormat[1]+'/'+dateFormat[2]+'</option>'
                a.append(option)
            }
          }
      },
      buttons: {
          EditSession: {
              text: 'Edit',
              btnClass: 'btn-blue',
              action: function () {
                let session_id = this.$content.find('#session-select>select').val();
                removeAllStatus(editorremovesession_target);
                if(session_id==-1){
                  setErrorStatus(editorremovesession_target);
                  return false;
                }else{
                  let found = mysessionlist.find(function(element) {
                    return element.session_id == session_id;
                  });
                  let a = formatDateFromServer(found.session_date);
                  let seDate=a[0]+'/'+a[1]+'/'+a[2]+' '+a[3]+':'+a[4]+' '+a[5];
                  editorremovesession_modal.close();
                  createEditSessionModal(found,seDate)
                }
              }
          },
          RemoveSession: {
              text:'Remove',
              btnClass: 'btn-danger',
              action: function () {
                let session_id = this.$content.find('#session-select>select').val();
                removeAllStatus(editorremovesession_target);
                if(session_id==-1){
                  setErrorStatus(editorremovesession_target);
                  return false;
                }else{
                    let found = mysessionlist.find(function(element) {
                      return element.session_id == session_id;
                    });
                    let a = formatDateFromServer(found.session_date);
                    let seDate=a[0]+'/'+a[1]+'/'+a[2]+' '+a[3]+':'+a[4]+' '+a[5];
                    editorremovesession_modal.close();
                    var myConfig = {
                       content:'Are you sure you want to delete <b>'+found.session_no+' '+seDate+'</b> From <b>'+found.course_id+'</b>?',
                       function:{
                            deleteSession: {
                              text: 'Yes',
                              btnClass: 'btn-danger',
                              action: function () {
                                  removeSessionAjax(found,seDate);
                              }
                          },
                          cancel: {
                            text:'Cancel',
                              action: function () {
                                  
                              } 
                          }
                       }
                    }
                    createConfirmModal(myConfig);
                }
              } 
          }
        },
    });
  }

  // ~~> Edit Session Modal
  function createEditSessionModal(se,date){
    let oldname,olddate;
    editsession_modal = $.confirm({
      icon: 'far fa-calendar-alt',
      title: 'Edit Session',
      content: 'url:/build/personal/text/editsessionForm.txt',
      theme: 'material,qa-editsession',
      type: 'black',
      animationBounce: 1.5,
      animation:'top',
      typeAnimated: true,
      closeIcon: true,  
      closeIconClass: 'fa fa-close',
      // autoClose: 'cancel|8000',
      columnClass: 'col-md-4 col-md-offset-4',
      smoothContent:true,
      backgroundDismiss:true,
      draggable: true,
      dragWindowGap: 50,
      alignMiddle:true,
      onContentReady: function () {
        oldname = se.session_no;
        olddate = se.session_date;
        olddate = formatDateFromServer(olddate);
        let oldsession = oldname+' | '+olddate[0]+'/'+olddate[1]+'/'+olddate[2]+' '+olddate[3]+':'+olddate[4]+' '+olddate[5]+' <-- (Old Session)';
        $('#editSessionForm input[name="old-session"]').val(oldsession); 
        $('.jconfirm').css('z-index', 99);   
        dateTimePicker();

        $('#edit-newsessionname input').tooltip({
          'trigger':'focus',
          template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>' 
        });
      },
      buttons: {
          EditSesison: {
            text:'Save',
            btnClass: 'btn-blue',
            action:function(){
                let edit_name_pass=false;
                let edit_date_pass=false;
                let newSessionName = $('#edit-newsessionname input').val().trim();
                newSessionName=='' ? newSessionName=oldname:newSessionName=newSessionName;
                let newSessionDate = $('#edit-newsessiondate input').val().trim().toUpperCase();
                removeAllStatus(edit_newSessionName_target);
                removeAllStatus(edit_newSessionDate_target);
                if(newSessionName.length>16){
                  setErrorStatus(edit_newSessionName_target);
                  
                }else{
                  setSuccessStatus(edit_newSessionName_target);
                  edit_name_pass=true;
                }
                if(!newSessionDate){
                  //remove success status
                  setErrorStatus(edit_newSessionDate_target);
                }else{
                  setSuccessStatus(edit_newSessionDate_target);
                  edit_date_pass=true
                }
                if(!edit_name_pass||!edit_date_pass){
                  return false;
                }else{
                  editsession_modal.close();
                  let format = formatDateToServer(newSessionDate)
                  let newSessionDateFormated  = new Date(format);
                  let newSession = {
                    id: se.session_id,
                    course_id:se.course_id,
                    new_name : newSessionName,
                    new_date : newSessionDateFormated
                  }
                  let myConfig = {
                    content:'Are you sure you want to change <b>'+se.session_no+' '+date+'</b> to <b>'+newSessionName+' '+newSessionDate+'</b> of <b>'+se.course_id+'</b>?',
                    function:{
                         EditSession: {
                           text: 'Yes',
                           btnClass: 'btn-danger',
                           action: function () {
                               editSessionAjax(newSession);
                           }
                       },
                       cancel: {
                         text:'Cancel',
                           action: function () {
                               
                           } 
                       }
                    }
                  }
                 createConfirmModal(myConfig);
                }
            }
          },
          Cancel: {
            text:'Cancel',
              action: function () {
                  
              } 
          }
        },
    });
  }
  // ~~> Attendance Checking Modal
  function attendanceCheckingModal(session){
    let a = formatDateFromServer(session.session_date);
    $.confirm({
      icon: 'fa fa-list',
      title: 'Attendance Checking ['+session.session_no+' - '+a[0]+'/'+a[1]+'/'+a[2]+']',
      content: 'url:/build/personal/text/attendancecheckingTable.txt',
      theme: 'material,qa-attendance-checking',
      type: 'black',
      animationBounce: 1.5,
      animation:'left',
      closeAnimation:'right',
      animationSpeed: 1000,
      typeAnimated: true,
      closeIcon: true,  
      closeIconClass: 'fa fa-close',
      // autoClose: 'cancel|8000',
      columnClass: 'col-md-12',
      smoothContent:true,
      backgroundDismiss:true,
      draggable: false,
      // dragWindowGap: 50,
      // alignMiddle:true,
      onContentReady: function () { 
        attendanceCheckingDataTable(session.session_id)
      },
      onClose:function(){
        if(tableNeedUpdate){
          tableNeedUpdate=false;
          $('#ajax-loading').show('fade');
          setTimeout(()=>{         
            datatableReDraw();
          },2500);
          $(tableName).on('init.dt', function() {
            $('#ajax-loading').hide('fade');
          })
        }
      },
    });
  }
  // ~~> StatisticsModal
  function createStatisticsModal(session_id){
    $.confirm({
      icon: 'fas fa-chart-bar',
      title: 'Attendance Checking Statistics',
      content: 'url:/build/personal/text/attendanceStatistic.txt',
      theme: 'material,qa-attendance',
      type: 'black',
      animationBounce: 1.5,
      animation:'top',
      typeAnimated: true,
      closeIcon: true,  
      closeIconClass: 'fa fa-close',
      // autoClose: 'cancel|8000',
      columnClass: 'col-md-6 col-md-offset-3',
      smoothContent:true,
      backgroundDismiss:true,
      draggable: true,
      dragWindowGap: 50,
      alignMiddle:true,
      onContentReady: function () { 
        attendanceStatisticAjax(session_id);
      },
      buttons: {
          Close: {
            text:'Close',
              action: function () {
                  
              } 
          }
        },
    });
  }
  //Ajax HQ
  
  
  // ~~> Ajax Add Session HQ //
  function addSessionAjax(newSessionDay){
    $.ajax({ 
      type:'POST',
      data:{newSessionDay},
      url:mypath+'/action/add-session',
      dataType:'json'
    }).done(function(response){
      console.log('response:'+JSON.stringify(response));
      var status = response.createStatus;
      console.log('status:'+status)
      let newSession = {
        name:response.name,
        date:response.date,
        id:response.id
      }
      if(status=="succeed"){
        updateSessionList();
        $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
        $('#qa-success-alert').show('drop',function(){
          $('#qa-success-alert .qa-message').show();
        });
        $('#ajax-loading').show('fade');
        setTimeout(()=>{
          $('#qa-success-alert .qa-message').hide();
          $('#qa-success-alert').hide('drop');
          addNewSessionColumn(newSession);
        },2500);
        setTimeout(()=>{
          $('#ajax-loading').hide('fade');
        },3000);  
      }else if(status=="failed"){
        $('#qa-error-alert .qa-message').html('<strong>Error! </strong>'+response.message);
        $('#qa-error-alert').show('drop',function(){
          $('#qa-error-alert .qa-message').show();
        });
        setTimeout(()=>{
          $('#qa-error-alert .qa-message').hide();
          $('#qa-error-alert').hide('drop'); 
        },2500); 
      }
    });
  };
  // ~~> Ajax Add Student //
  function AddStudentAjax(student_id,course_id){
    let newStudent ={
      id:student_id,
      course_id:course_id
    }
    $.ajax({ 
      type:'POST',
      data:{newStudent},
      url:mypath+'/action/add-student',
      dataType:'json'
    }).done(function(response){
      let status = response.createStatus;
      console.log(status);
      if(status=="succeed"){
        $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
        $('#qa-success-alert').show('drop',function(){
          $('#qa-success-alert .qa-message').show();
        });
        $('#ajax-loading').show('fade');
        setTimeout(()=>{
          $('#qa-success-alert .qa-message').hide();
          $('#qa-success-alert').hide('drop'); 
          datatableReDraw();
        },2500); 
        // setTimeout(()=>{
        //   $('#ajax-loading').hide('fade');
        // },3000); 
        $(tableName).on('init.dt', function() {
          $('#ajax-loading').hide('fade');
        }) 
      }else if(status=="failed"){
        $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
        $('#qa-error-alert').show('drop',function(){
          $('#qa-error-alert .qa-message').show();
        });
        setTimeout(()=>{
          $('#qa-error-alert .qa-message').hide();
          $('#qa-error-alert').hide('drop'); 
        },3500); 
      }
    })
  }
  // ~~> Ajax Delete Student //
  function deleteStudentAjax(studentID){
    let course_id = $('#course-id').text();
    let data ={
      student_id:studentID,
      course_id:course_id
    };
    $.ajax({ 
      type:'POST',
      data:{data},
      url:mypath+'/action/delete-student',
      dataType:'json'
    }).done(function(response){
      let status = response.createStatus;
      console.log(status);
      if(status=="succeed"){
        $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
        $('#qa-success-alert').show('drop',function(){
          $('#qa-success-alert .qa-message').show();
        });
        $('#ajax-loading').show('fade');
        setTimeout(()=>{
          $('#qa-success-alert .qa-message').hide();
          $('#qa-success-alert').hide('drop');
          datatableReDraw(); 
        },2500);
        // setTimeout(()=>{
        //   $('#ajax-loading').hide('fade');
        // },3000);   
        $(tableName).on('init.dt', function() {
          $('#ajax-loading').hide('fade');
        })
      }else if(status=="failed"){
        $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
        $('#qa-error-alert').show('drop',function(){
          $('#qa-error-alert .qa-message').show();
        });
        setTimeout(()=>{
          $('#qa-error-alert .qa-message').hide();
          $('#qa-error-alert').hide('drop'); 
        },2500); 
      }
    });
  };
  // ~~> Ajax Remove Session //
  function removeSessionAjax(found,date){
    let session = {
      id : found.session_id,
      no : found.session_no,
      date : date
    }
    $.ajax({ 
    type:'POST',
    data:{session},
    url:mypath+'/action/remove-session',
    dataType:'json'
  }).done(function(response){
      var status = response.createStatus;
      if(status=="succeed"){
        updateSessionList();
        $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
        $('#qa-success-alert').show('drop',function(){
          $('#qa-success-alert .qa-message').show();
        });
        $('#ajax-loading').show('fade');
        setTimeout(()=>{
          $('#qa-success-alert .qa-message').hide();
          $('#qa-success-alert').hide('drop');
          datatableReDraw();
        },2500);
        // setTimeout(()=>{
        //   $('#ajax-loading').hide('fade');
        // },3000); 
        $(tableName).on('init.dt', function() {
          $('#ajax-loading').hide('fade');
        }) 
      }else if(status=="failed"){
        $('#qa-error-alert .qa-message').html('<strong>Error! </strong>'+response.message);
        $('#qa-error-alert').show('drop',function(){
          $('#qa-error-alert .qa-message').show();
        });
        setTimeout(()=>{
          $('#qa-error-alert .qa-message').hide();
          $('#qa-error-alert').hide('drop'); 
        },2500); 
      }
  });
  }
  // ~~> Ajax Edit Session //
  function editSessionAjax(newSession){
    $.ajax({ 
      type:'POST',
      data:{newSession},
      url:mypath+'/action/edit-session',
      dataType:'json'
    }).done(function(response){
        console.log(response);
        var status = response.createStatus;
        if(status=="succeed"){
          updateSessionList();
          $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
          $('#qa-success-alert').show('drop',function(){
            $('#qa-success-alert .qa-message').show();
          });
          $('#ajax-loading').show('fade');
          setTimeout(()=>{
            $('#qa-success-alert .qa-message').hide();
            $('#qa-success-alert').hide('drop');
            datatableReDraw();
          },2500);
          // setTimeout(()=>{
          //   $('#ajax-loading').hide('fade');
          // },3000);
          $(tableName).on('init.dt', function() {
            $('#ajax-loading').hide('fade');
          })  
        }else if(status=="failed"){
          $('#qa-error-alert .qa-message').html('<strong>Error! </strong>'+response.message);
          $('#qa-error-alert').show('drop',function(){
            $('#qa-error-alert .qa-message').show();
          });
          setTimeout(()=>{
            $('#qa-error-alert .qa-message').hide();
            $('#qa-error-alert').hide('drop'); 
          },2500); 
        }
    });
  };
  // ~~> Ajax Attendance Checking //
  function attendanceCheckingAjax(myattendance){
    $.ajax({ 
      type:'POST',
      data:{myattendance},
      url:mypath+'/action/update-attendance',
      dataType:'json'
    }).done(function(response){
      console.log('response:'+JSON.stringify(response));
      let status = response.createStatus;
      if(status == "succeed"){
          tableNeedUpdate = true;
          PNotify.alert({
            title: 'Success!',
            text: response.message,
            type: 'success',
            styling: 'bootstrap4',
            icons:'fontawesome5',
            delay:3500,
            animate: {
              animate: true,
              in_class: 'bounceIn',
              out_class: 'bounceOut'
            },
            addclass: "stack-custom",
            stack: stack_bottomleft
          });
      }
    });
  }
  // ~~> Ajax Attendance Statistic //
  function attendanceStatisticAjax(session_id){
    $.ajax({ 
      type:'POST',
      data:{session_id},
      url:mypath+'/action/attendance-statistics',
      dataType:'json'
    }).done(function(response){
      let status = response.createStatus;
      console.log(response.data);
      let notattend = response.data.notattend;
      let attend = response.data.attend;
      let absent = response.data.absent
      if(status == "succeed"){
        function drawChart() {
          var a  = [{name:'notattend',value:notattend},{name:'attend',value:attend},{name:'absent',value:absent}];
          a.sort(function(a, b){return parseFloat(b.value)-parseFloat(a.value)});
          var data = google.visualization.arrayToDataTable([
          ['Status', 'Quantity'],
          [a[0].name, 0],
          [a[1].name,0],
          [a[2].name, 0]
          ]);
          var chartColor = []
          for (let i=0;i<3;i++){
            var colorZ = getRandomColor();
            chartColor.push(colorZ);
          }
          var options = {
            width:500,
            height:500,
            chartArea: {width: 500, height: 500},
            colors: chartColor,
            animation: {
              duration: 1000,
              easing: 'in',
              startup: true
            }
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart'));
          chart.draw(data, options);
          var percent = 0;
          var total = notattend+attend+absent;
          var b = Math.round((a[0].value/total)*100);
          var c = Math.round((a[2].value/total)*100);
          // start the animation loop
          var handler = setInterval(function(){
              // values increment
              percent += 1;
              // apply new values
              data.setValue(0, 1, percent);
              data.setValue(1, 1, 100 - (percent+c));
              data.setValue(2, 1, c);
              console.log(c);
              // update the pie
              chart.draw(data, options);
              // check if we have reached the desired value
              if (percent > b-1){
                  // stop the loop
                  data.setValue(0, 1,a[0].value);
                  data.setValue(1, 1,a[1].value);
                  data.setValue(2, 1,a[2].value);
                  chart.draw(data, options);                      
                  clearInterval(handler);
                }
              }, 30);
          }
         
          google.charts.load('current', {
            'packages': ['corechart']
          });
          google.charts.setOnLoadCallback(drawChart);
        }
    })
  }
  // ~~> Ajax Sync Student //
  function syncStudentToServerAjax(course_id){
      $.ajax({ 
        type:'POST',
        data:{course_id},
        url:mypath+'/action/api/sync-student-to-server',
        dataType:'json'
      }).done(function(response){
        let status = response.createStatus;
        console.log(status);
        setTimeout(()=>{
          $('#qa-infor-alert').hide('drop');     
        },2000);
        if(status=="succeed"){
          $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
          setTimeout(()=>{
            $('#qa-success-alert').show('drop',function(){
              $('#qa-success-alert .qa-message').show();
            });
          },2500)
          setTimeout(()=>{
            $('#qa-success-alert .qa-message').hide();
            $('#qa-success-alert').hide('drop'); 
          },4500);
        }else if(status=="failed"){
          $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
          setTimeout(()=>{
            $('#qa-error-alert').show('drop',function(){
              $('#qa-error-alert .qa-message').show();
            }); 
          },2500)
          setTimeout(()=>{
            $('#qa-error-alert .qa-message').hide();
            $('#qa-error-alert').hide('drop'); 
          },4500); 
        }
      });
  }
  // ~~> Ajax Sync Attendance To Server //
  function syncAttendanceToServerAjax(course_id){
    $.ajax({ 
      type:'POST',
      data:{course_id},
      url:mypath+'/action/api/sync-attendance-to-server',
      dataType:'json'
    }).done(function(response){
      let status = response.createStatus;
      console.log(response);
      setTimeout(()=>{
        $('#qa-infor-alert').hide('drop');     
      },2000);
      if(status=="succeed"){
        $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
        setTimeout(()=>{
          $('#qa-success-alert').show('drop',function(){
            $('#qa-success-alert .qa-message').show();
          });
        },2500)
        setTimeout(()=>{
          $('#qa-success-alert .qa-message').hide();
          $('#qa-success-alert').hide('drop'); 
        },4500);
      }else if(status=="failed"){
        $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i>'+response.message);
        setTimeout(()=>{
          $('#qa-error-alert').show('drop',function(){
            $('#qa-error-alert .qa-message').show();
          }); 
        },2500)
        setTimeout(()=>{
          $('#qa-error-alert .qa-message').hide();
          $('#qa-error-alert').hide('drop'); 
        },4500); 
      }
    });
  }
  function importAjax(data,filename){
    console.log(data);
    let val = {
      data:data,
      course_id:$('#course-id').text()
    }
    $.ajax({ 
      type:'POST',
      data:{val},
      url:mypath+'/action/import-excel',
      dataType:'json'
    }).done(function(response){
        console.log('response:'+JSON.stringify(response));
        var status = response.createStatus;
        setTimeout(()=>{
            $('#qa-infor-alert').hide('drop');     
        },2000); 
        if(status=="succeed"){
          updateSessionList();
          $('#qa-success-alert .qa-message').html('<i class="fa fa fa-check-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i><span>Your file('+filename+') processed successfully</span>');
          setTimeout(()=>{
            $('#qa-success-alert').show('drop',function(){
              $('#qa-success-alert .qa-message').show();
            });     
          },2500);
          $('#ajax-loading').show('fade');           
          setTimeout(()=>{
            $('#qa-success-alert .qa-message').hide();
            $('#qa-success-alert').hide('drop');
            datatableReDraw(); 
          },4500); 
          $(tableName).on('init.dt', function() {
            $('#ajax-loading').hide('fade');
          });
        }else if(status=="failed"){
            $('#qa-error-alert .qa-message').html('<i class="fa fa-exclamation-circle fa-lg" aria-hidden="true" style="color:#fff;margin-right:10px"></i><span>Your file('+filename+') processed failed</span>');
            setTimeout(()=>{
              $('#qa-error-alert').show('drop',function(){
                $('#qa-error-alert .qa-message').show();
              });     
            },2500);
            setTimeout(()=>{
              $('#qa-error-alert .qa-message').hide();
              $('#qa-error-alert').hide('drop'); 
            },4500); 
        }
    });
  };